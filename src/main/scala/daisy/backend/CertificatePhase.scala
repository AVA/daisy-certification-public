package daisy
package backend

import daisy.lang.{ScalaPrinter, PrettyPrinter}
import lang.Trees._
import lang.TreeOps
import lang.Identifiers._
import lang.NumAnnotation
import utils.Interval
import utils.Rational
import analysis.SpecsProcessingPhase

object CertificatePhase extends DaisyPhase {

  override val name = "Certificate Creation"
  override val description = "Generates certificate for chosen theorem prover to check"

  //Map tracking which number has been assigned to which identifier
  var identifierNums :Map [Identifier, Int] = Map[Identifier, Int] ()
  //Map tracking which theorem prover name is associated with a subexpression
  var expressionNames :Map [Expr, String] = Map[Expr, String] ()

  def run(ctx: Context, prg: Program): (Context, Program) = {

    val reporter = ctx.reporter
    //must be set when entering this phase --> should not crash
    val prover = ctx.findOption(Main.optionValidators).get

    //The full certificate as a string
    var fullCertificate = new StringBuilder();

    def writeToFile (fileContent:String, prover:String){
      import java.io.FileWriter
      import java.io.BufferedWriter

      val fileLocation =
        if (prover == "coq")
          s"./coq/output/certificate_${prg.id}.v"
        else
          if (prover == "hol4")
          s"./hol4/output/certificate_${prg.id}Script.sml"
        else
          s"./output/certificate_${prg.id}.txt"

      val fstream = new FileWriter(fileLocation)
      val out = new BufferedWriter(fstream)
      out.write(fileContent)
      out.close
    }

    //First write the imports
    fullCertificate ++= getPrelude(prover, "certificate_" + prg.id)

    //Add some spacing for readability
    //fullCertificate ++= "\n";
    var num_of_functions = 0

    for (fnc <- prg.defs) if (!fnc.precondition.isEmpty && !fnc.body.isEmpty){
      reporter.info(s"Generating certificate for ${fnc.id}")

      //first retrieve the values
      //these should be defined, otherwise the generation will fail
      val thePrecondition = fnc.precondition.get
      val theBody = fnc.body.get
      val errorMap = ctx.intermediateAbsoluteErrors.get(fnc.id).get
      val rangeMap = ctx.intermediateRanges.get(fnc.id).get

      //the definitions for the whole expression
      val (theDefinitions, lastGenName) = getCmd(theBody,reporter,prover)
      //generate the precondition
      val (thePreconditionFunction, functionName) =
        getPrecondFunction(thePrecondition, fnc.id.toString, reporter, prover)
      //the analysis result function
      val (analysisResultText, analysisResultName) =
        getAbsEnvDef(theBody, errorMap, rangeMap, fnc.id.toString, reporter, prover)
      //generate the final evaluation statement
      val functionCall =
         getComputeExpr(lastGenName,
           analysisResultName,
           functionName,
           fnc.id.toString,
           prover)
      //compose the strings and append to certificate
      fullCertificate ++= theDefinitions + "\n\n" +  thePreconditionFunction + "\n\n" + analysisResultText
      //spacing
      fullCertificate ++= "\n\n"
      fullCertificate ++= functionCall

      num_of_functions += 1
    }
    if (prover == "hol4")
      fullCertificate ++= "\n\n val _ = export_theory();"

    if (prover == "binary")
      fullCertificate.insert(0,s"100 $num_of_functions ")

    //end iteration, write certificate
    writeToFile(fullCertificate.mkString,prover)
    (ctx, prg)
  }

  private val nextConstantId = { var i = 0; () => { i += 1; i} }
  private val nextFreshVariable = { var i = 0; () => { i += 1; i} }

  private def getPrelude(prover:String, fname:String) :String =
    {
      if (prover == "coq")
        "Require Import Daisy.CertificateChecker.\n\n"
      else if (prover == "hol4") {
        "open preamble DaisyCompLib AbbrevsTheory CertificateCheckerTheory;\nopen simpLib realTheory realLib RealArith;\n" +
        "val _ = new_theory \"" + fname +"\";\n\n"
      } else ""
    }

  private def coqVariable(vname:Identifier, id:Int, reporter:Reporter) :(String, String) =
  {
    //FIXME: Ugly Hack to get disjoint names for multiple function encodings with same variable names:
    val freshId = nextFreshVariable()
    val theExpr = s"Definition ExpVar$vname$freshId :exp Q := Var Q $id.\n"
    (theExpr,s"ExpVar$vname$freshId")
  }

  private def ocamlVariable(vname:Identifier, id:Int, reporter:Reporter) :(String, String) =
  {
    //FIXME: Ugly Hack to get disjoint names for multiple function encodings with same variable names:
    val freshId = nextFreshVariable()
    val theExpr = s"let ExpVar$vname$freshId = Var $id.\n"
    (theExpr,s"ExpVar$vname$freshId")
  }

  private def hol4Variable(vname:Identifier, id:Int, reporter:Reporter) :(String, String) =
  {
    //FIXME: Ugly Hack to get disjoint names for multiple function encodings with same variable names:
    val freshId = nextFreshVariable()
    val theExpr = s"val ExpVar$vname${freshId}_def = Define `ExpVar$vname$freshId:(real exp) = Var $id`;\n"
    (theExpr, s"ExpVar$vname$freshId")
  }

  private def coqConstant(r:RealLiteral, id:Int, reporter:Reporter) :(String, String) =
    r match {
      case RealLiteral(v) =>
        //FIXME: Ugly Hack to get disjoint names for multiple function encodings with same variable names:
        val freshId = nextConstantId()
        val rationalStr = v.toFractionString
        val coqRational = rationalStr.replace('/','#')
        val theExpr = s"Definition ExpCst$id$freshId :exp Q := Const ($coqRational).\n"
        (theExpr, s"ExpCst$id$freshId")
    }

  private def hol4Constant(r:RealLiteral, id:Int, reporter:Reporter) :(String, String) =
    r match {
      case RealLiteral(v) =>
        //FIXME: Ugly Hack to get disjoint names for multiple function encodings with same variable names:
        val freshId = nextConstantId()
        val rationalStr = v.toFractionString
        val theExpr = s"val ExpCst$id${freshId}_def = Define `ExpCst$id$freshId:(real exp) = Const ($rationalStr)`;\n"
        (theExpr, s"ExpCst$id$freshId")
    }

  private def coqBinOp (e: Expr, nameLHS:String, nameRHS:String, reporter:Reporter) :(String, String) =
    e match {
      case x @ Plus(lhs, rhs) =>
        ("Definition Plus"+ nameLHS + nameRHS + s" :exp Q := Binop Plus $nameLHS $nameRHS.\n",
          "Plus"+ nameLHS + nameRHS)
       case x @ Minus(lhs, rhs) =>
        ("Definition Sub"+ nameLHS + nameRHS + s" :exp Q := Binop Sub $nameLHS $nameRHS.\n",
          "Sub"+ nameLHS + nameRHS)
       case x @ Times(lhs, rhs) =>
        ("Definition Mult"+ nameLHS + nameRHS + s" :exp Q := Binop Mult $nameLHS $nameRHS.\n",
          "Mult"+ nameLHS + nameRHS)
      case x @ Division(lhs, rhs) =>
        ("Definition Div"+ nameLHS + nameRHS + s" :exp Q := Binop Div $nameLHS $nameRHS.\n",
          "Div"+ nameLHS + nameRHS)
       case x @ _ =>
       reporter.fatalError("Unsupported value")
    }

    private def hol4BinOp (e: Expr, nameLHS:String, nameRHS:String, reporter:Reporter) :(String, String) =
    e match {
      case x @ Plus(lhs, rhs) =>
        ("val Plus"+ nameLHS + nameRHS + " = Define `Plus"+ nameLHS + nameRHS + s":(real exp) = Binop Plus $nameLHS $nameRHS`;;\n",
          "Plus"+ nameLHS + nameRHS)
      case x @ Minus(lhs, rhs) =>
        ("val Sub"+ nameLHS + nameRHS + " = Define `Sub"+ nameLHS + nameRHS + s":(real exp) = Binop Sub $nameLHS $nameRHS`;;\n",
          "Sub"+ nameLHS + nameRHS)
      case x @ Times(lhs, rhs) =>
        ("val Mult"+ nameLHS + nameRHS + " = Define `Mult"+ nameLHS + nameRHS + s":(real exp) = Binop Mult $nameLHS $nameRHS`;;\n",
          "Mult"+ nameLHS + nameRHS)
      case x @ Division(lhs, rhs) =>
        ("val Div"+ nameLHS + nameRHS + " = Define `Div"+ nameLHS + nameRHS + s":(real exp) = Binop Div $nameLHS $nameRHS`;;\n",
          "Div"+ nameLHS + nameRHS)
       case x @ _ =>
       reporter.fatalError("Unsupported value")
    }

  private def coqUMin (e:Expr, nameOp:String, reporter:Reporter) :(String, String) =
    (s"Definition UMin${nameOp} :exp Q := Unop Neg $nameOp.\n", s"UMin${nameOp}")

  private def hol4UMin (e:Expr, nameOp:String, reporter:Reporter) :(String, String) =
    (s"val UMin${nameOp} = Define `UMin${nameOp}:(real exp) = Unop Neg ${nameOp}`;\n",
      s"UMin${nameOp}")

  private def getValues(e: Expr,reporter:Reporter,prv:String): (String, String) = {

    //if the expression has already been defined before
    if (expressionNames.contains(e)){
      ("",expressionNames(e))
    } else {
      e match {
        case x @ Variable(id) =>
          if (identifierNums.contains(id)){
            ("",expressionNames(e))
          }else{
            val varId = nextFreshVariable()
            identifierNums += (id -> varId)
            val (definition, name) =
              if (prv == "coq"){
                coqVariable (id,varId,reporter)
              }else if (prv == "hol4"){
                hol4Variable (id, varId, reporter)
              } else {
                (s"Var $varId", s"Var $varId")
              }
            expressionNames += (e -> name)
            (definition,name)
          }

        case x @ RealLiteral(r) =>
          val (definition, name) =
            if (prv == "coq")
              coqConstant (x,nextConstantId(),reporter)
            else if (prv == "hol4")
              hol4Constant (x, nextConstantId(), reporter)
            else {
              val text = r.toFractionString.replace("/","#").replace("(","").replace(")","").replace("-","~")
              (text, text)
            }
          expressionNames += (e -> name)
          (definition,name)

        case x @ Plus(lhs, rhs) =>
          val (lhsText, lhsName) = getValues(lhs,reporter,prv)
          val (rhsText, rhsName) = getValues(rhs,reporter,prv)
          val (definition, name) =
            if (prv == "coq"){
              val (binOpDef, binOpName) = coqBinOp (e, lhsName, rhsName,reporter)
              (lhsText + rhsText + binOpDef, binOpName)
            } else if (prv == "hol4") {
              val (binOpDef, binOpName) = hol4BinOp (e, lhsName, rhsName,reporter)
              (lhsText + rhsText + binOpDef, binOpName)
            } else {
              val text = s"+ $lhsName $rhsName"
              (text, text)
            }
          expressionNames += (e -> name)
          (definition,name)

        case x @ Minus(lhs, rhs) =>
          val (lhsText, lhsName) = getValues(lhs,reporter,prv)
          val (rhsText, rhsName) = getValues(rhs,reporter,prv)
          val (definition, name) =
            if (prv == "coq"){
              val (binOpDef, binOpName) = coqBinOp (e, lhsName, rhsName,reporter)
              (lhsText + rhsText + binOpDef, binOpName)
            } else if (prv == "hol4") {
              val (binOpDef, binOpName) = hol4BinOp (e, lhsName, rhsName, reporter)
              (lhsText + rhsText + binOpDef, binOpName)
            } else {
              val text = s"- $lhsName $rhsName"
              (text, text)
            }
          expressionNames += (e -> name)
          (definition,name)

        case x @ Times(lhs, rhs) =>
          val (lhsText, lhsName) = getValues(lhs,reporter,prv)
          val (rhsText, rhsName) = getValues(rhs,reporter,prv)
          val (definition, name) =
            if (prv == "coq"){
              val (binOpDef, binOpName) = coqBinOp (e, lhsName, rhsName,reporter)
              (lhsText + rhsText + binOpDef, binOpName)
            } else if (prv == "hol4") {
              val (binOpDef, binOpName) = hol4BinOp (e, lhsName, rhsName,reporter)
              (lhsText + rhsText + binOpDef, binOpName)
            } else {
              val text = s"* $lhsName $rhsName"
              (text, text)
            }
          expressionNames += (e -> name)
          (definition,name)

        case x @ Division(lhs, rhs) =>
          val (lhsText, lhsName) = getValues(lhs,reporter,prv)
          val (rhsText, rhsName) = getValues(rhs,reporter,prv)
          val (definition, name) =
            if (prv == "coq"){
              val (binOpDef, binOpName) = coqBinOp (e, lhsName, rhsName,reporter)
              (lhsText + rhsText + binOpDef, binOpName)
            } else if (prv == "hol4") {
              val (binOpDef, binOpName) = hol4BinOp (e, lhsName, rhsName,reporter)
              (lhsText + rhsText + binOpDef, binOpName)
            } else {
              val text = s"/ $lhsName $rhsName"
              (text, text)
            }
          expressionNames += (e -> name)
          (definition,name)

        case x @ UMinus(exp) =>
          val (opDef, opName) = getValues (exp, reporter, prv)
          val (defintion, name) =
            if (prv == "coq") {
              val (unopDef, unopName) = coqUMin (e, opName, reporter)
              (opDef + unopDef, unopName)
            } else if (prv == "hol4"){
              val (unopDef, unopName) = hol4UMin (e, opName, reporter)
              (opDef + unopDef, unopName)
            } else {
              val text = s"~ $opName"
              (text,text)
            }
          expressionNames += (e -> name)
          (defintion, name)

        case x @ _ =>
          reporter.fatalError(s"Unsupported operation $e while generating expression")
      }
    }
  }

  private def getCmd (e: Expr,reporter:Reporter,prv:String): (String, String) = {
    e match {
      case e @ Let(x,exp,g) =>
        //first compute expression AST
        val (expDef, expName) = getValues (exp,reporter,prv)
        //now allocate a new variable
        val varId = nextFreshVariable()
        identifierNums += (x -> varId)
        val (varDef, varName) =
          if (prv == "coq"){
            coqVariable (x,varId,reporter)
          } else if (prv == "hol4") {
            hol4Variable (x, varId, reporter)
          } else {
            (s"Var ${varId.toString}", s"Var ${varId.toString}")
          }
        expressionNames += (Variable(x) -> varName)
        //now recursively compute the command
        val (cmdDef, cmdName) = getCmd (g, reporter,prv)
        val letName = "Let"+varName+expName+cmdName
        val letDef =
          if (prv == "coq"){
            s"Definition $letName := Let $varId $expName $cmdName.\n"
          } else if (prv == "hol4") {
            s"val ${letName}_def = Define `$letName = Let $varId $expName $cmdName`;\n"
          } else {
            s"Let $varDef $expName $cmdDef"
          }
        if (prv == "binary")
          (letDef, letDef)
        else
          (expDef + varDef + cmdDef + letDef, letName)

      case e @ _ =>
        //return statement necessary
        val (expDef, expName) = getValues (e, reporter, prv)
        val retName = s"Ret$expName"
        if (prv == "coq"){
          (expDef + s"Definition $retName := Ret $expName.\n", retName)
        } else if (prv == "hol4") {
          (expDef + s"val ${retName}_def = Define `$retName = Ret $expName`;\n", retName)
        } else
            (s"Ret $expName",s"Ret $expName")
    }
  }

  private def coqInterval(intv:(Rational,Rational)) :String =
    intv match {
      case (lowerBound, upperBound) =>
        val lowerBoundCoq = lowerBound.toFractionString.replace('/','#')
        val upperBoundCoq = upperBound.toFractionString.replace('/','#')
        "( " + lowerBoundCoq + ", " + upperBoundCoq + ")"
    }

  private def hol4Interval(intv:(Rational,Rational)) :String =
    intv match {
      case (lowerBound, upperBound) =>
        "( " + lowerBound.toFractionString + ", " + upperBound.toFractionString + ")"
    }

  private def binaryInterval(intv:(Rational,Rational)) :String =
    intv match {
      case (lowerBound, upperBound) =>
        val loTmp = lowerBound.toFractionString.replace("(","").replace(")","")
        val hiTmp = upperBound.toFractionString.replace("(","").replace(")","")
        val lo = loTmp.replace("/","#").replace("-","~")
        val hi = hiTmp.replace("/", "#").replace("-","~")
        s"$lo $hi"
    }

  private def coqPrecondition (ranges:Map [Identifier, (Rational, Rational)],fName:String) :(String, String) =
  {
    var theFunction = s"Definition thePrecondition_${fName}:precond := fun (n:nat) =>\n"
    for ((id,intv) <- ranges) {
      val ivCoq = coqInterval (intv)
      //variable must already have a binder here!
      //TODO: assert it?
      theFunction += "if n =? " + identifierNums(id) + " then "+ ivCoq +  " else "
    }
    theFunction += "(0#1,0#1)."
    (theFunction, s"thePrecondition_${fName}")
  }

  private def hol4Precondition (ranges:Map [Identifier, (Rational, Rational)],fName:String) :(String, String) =
  {
    var theFunction = s"val thePrecondition_${fName}_def = Define ` \n thePrecondition${fName} (n:num):interval = \n"
    for ((id,intv) <- ranges) {
      val ivHolLight = hol4Interval(intv)
      theFunction += "if n = " + identifierNums(id) + " then " + ivHolLight + " else "
    }
    theFunction += "(0,1)`;"
    (theFunction, s"thePrecondition${fName}")
  }

  private def binaryPrecondition (ranges:Map [Identifier, (Rational, Rational)],fName:String) :(String, String) =
  {
    var theFunction = " PRE "
    for ((id,intv) <- ranges) {
      val ivBin = binaryInterval(intv)
      theFunction += s"? Var ${identifierNums(id)} $ivBin "
    }
    (theFunction, "")
  }

  private def getPrecondFunction(pre:Expr,fName:String,reporter:Reporter,prv:String) :(String,String) =
  {
    val (ranges, errors) = daisy.analysis.SpecsProcessingPhase.extractPreCondition(pre)
    if (! errors.isEmpty){
      reporter.fatalError("Errors on inputs are currently unsupported")
    }
    if (prv == "coq"){
      coqPrecondition(ranges,fName)
    }else if (prv == "hol4"){
      hol4Precondition(ranges,fName)
    } else {
      binaryPrecondition(ranges,fName)
    }
  }

  private def conditional (condition:String, thenC:String, elseC: String) :String =
    "if " + condition + "\n" +
    "then " + thenC + "\n" +
    "else " + elseC

  private def coqAbsEnv (e:Expr, errorMap:Map[Expr, Rational], rangeMap:Map[Expr, Interval], cont:String, reporter:Reporter) :String =
  {
    // Let bindings do not have names, so these are a special case here:
    e match {
      case x @ Let (y,exp, g) =>
        val expFun = coqAbsEnv (exp, errorMap, rangeMap, cont, reporter)
        val gFun = coqAbsEnv (g, errorMap, rangeMap, expFun, reporter)
        val intvY = coqInterval((rangeMap(Variable(y)).xlo, rangeMap(Variable(y)).xhi))
        val errY = errorMap(Variable (y)).toFractionString.replace("/","#")
        val nameY = expressionNames(Variable(y))
        conditional (
          s"( expEqBool e  (Var Q ${identifierNums(y)}) )",
          "(" + intvY + ", " + errY + ")",
          gFun)

      case x @ _ =>

        val nameE = expressionNames(e)

        val intvE = coqInterval((rangeMap(x).xlo,rangeMap(x).xhi))
        val errE = errorMap(x).toFractionString.replace("/","#")

        val continuation =
          e match {

            case x @ Variable(id) => cont

            case x @ RealLiteral(r) => cont

            case x @ Plus(lhs, rhs) =>
              val lFun = coqAbsEnv (lhs, errorMap, rangeMap, cont, reporter)
              coqAbsEnv (rhs, errorMap, rangeMap, lFun, reporter)

            case x @ Minus(lhs, rhs) =>
              val lFun = coqAbsEnv (lhs, errorMap, rangeMap, cont, reporter)
              coqAbsEnv (rhs, errorMap, rangeMap, lFun, reporter)

            case x @ Times(lhs, rhs) =>
              val lFun = coqAbsEnv (lhs, errorMap, rangeMap, cont, reporter)
              coqAbsEnv (rhs, errorMap, rangeMap, lFun, reporter)

            case x @ Division(lhs, rhs) =>
              val lFun = coqAbsEnv (lhs, errorMap, rangeMap, cont, reporter)
              coqAbsEnv (rhs, errorMap, rangeMap, lFun, reporter)

            case x @ UMinus(e) =>
              coqAbsEnv (e, errorMap, rangeMap, cont, reporter)

            case x @ _ =>
              reporter.fatalError(s"Unsupported operation $e while generating absenv")
        }
        conditional (
          "( expEqBool e " + nameE + " )",
          "(" + intvE + ", " + errE + ")",
          continuation)
    }
  }

  private def hol4AbsEnv (e:Expr, errorMap:Map[Expr, Rational], rangeMap:Map[Expr, Interval], cont:String, reporter:Reporter) :String =
  {
    e match {

      case x @ Let (y,exp, g) =>
        val expFun = hol4AbsEnv (exp, errorMap, rangeMap, cont, reporter)
        val gFun = hol4AbsEnv (g, errorMap, rangeMap, expFun, reporter)
        val intvY = hol4Interval((rangeMap(Variable(y)).xlo, rangeMap(Variable(y)).xhi))
        val errY = errorMap(Variable (y)).toFractionString
        val nameY = expressionNames(Variable(y))
        conditional (
          s"( e = Var ${identifierNums(y)} )",
          "(" + intvY + ", " + errY + ")",
          gFun)

      case x @ _ =>

        val nameE = expressionNames(e)

        val intvE = hol4Interval((rangeMap(x).xlo,rangeMap(x).xhi))
        val errE = errorMap(x).toFractionString

        val continuation =
          e match {
            case x @ Variable(id) => cont

            case x @ RealLiteral(r) => cont

            case x @ Plus(lhs, rhs) =>
              val lFun = hol4AbsEnv (lhs, errorMap, rangeMap, cont, reporter)
              hol4AbsEnv (rhs, errorMap, rangeMap, lFun, reporter)

            case x @ Minus(lhs, rhs) =>
              val lFun = hol4AbsEnv (lhs, errorMap, rangeMap, cont, reporter)
              hol4AbsEnv (rhs, errorMap, rangeMap, lFun, reporter)

            case x @ Times(lhs, rhs) =>
              val lFun = hol4AbsEnv (lhs, errorMap, rangeMap, cont, reporter)
              hol4AbsEnv (rhs, errorMap, rangeMap, lFun, reporter)

            case x @ Division(lhs, rhs) =>
              val lFun = hol4AbsEnv (lhs, errorMap, rangeMap, cont, reporter)
              hol4AbsEnv (rhs, errorMap, rangeMap, lFun, reporter)

            case x @ UMinus(e) =>
              hol4AbsEnv (e, errorMap, rangeMap, cont, reporter)

          case x @ _ =>
            reporter.fatalError(s"Unsupported operation $e while generating absenv")
        }
        conditional (
          "( e  = " + nameE + " )",
          "(" + intvE + ", " + errE + ")",
          continuation)
    }
  }

  private def binaryAbsEnv (e:Expr, errorMap:Map[Expr, Rational], rangeMap:Map[Expr, Interval], reporter:Reporter) :String =
  {
    e match {

      case x @ Let (y,exp, g) =>
        val expFun = binaryAbsEnv (exp, errorMap, rangeMap, reporter)
        val gFun = binaryAbsEnv (g, errorMap, rangeMap, reporter)
        val intvY = binaryInterval((rangeMap(Variable(y)).xlo, rangeMap(Variable(y)).xhi))
        // should not be necessary: .replace("-","~")
        val errY = errorMap(Variable (y)).toFractionString.replace("(","").replace(")","").replace("/","#")
        val nameY = expressionNames(Variable(y))
        s"? Var ${identifierNums(y)} $intvY $errY $expFun $gFun"

      case x @ _ =>

        val nameE = expressionNames(e)

        val intvE = binaryInterval((rangeMap(x).xlo,rangeMap(x).xhi))
        val errE = errorMap(x).toFractionString.replace("(","").replace(")","").replace("/","#")

        val continuation =
          e match {
            case x @ Variable(id) => ""

            case x @ RealLiteral(r) => ""

            case x @ Plus(lhs, rhs) =>
              binaryAbsEnv (lhs, errorMap, rangeMap, reporter) +
              binaryAbsEnv (rhs, errorMap, rangeMap, reporter)

            case x @ Minus(lhs, rhs) =>
              binaryAbsEnv (lhs, errorMap, rangeMap, reporter) +
              binaryAbsEnv (rhs, errorMap, rangeMap, reporter)

            case x @ Times(lhs, rhs) =>
              binaryAbsEnv (lhs, errorMap, rangeMap, reporter) +
              binaryAbsEnv (rhs, errorMap, rangeMap, reporter)

            case x @ Division(lhs, rhs) =>
              binaryAbsEnv (lhs, errorMap, rangeMap, reporter) +
              binaryAbsEnv (rhs, errorMap, rangeMap, reporter)

            case x @ UMinus(e) =>
              binaryAbsEnv (e, errorMap, rangeMap, reporter)

          case x @ _ =>
            reporter.fatalError(s"Unsupported operation $e while generating absenv")
          }
        s"? $nameE $intvE $errE $continuation"
    }
  }

  private def getAbsEnvDef (e:Expr, errorMap:Map[Expr, Rational], rangeMap:Map[Expr, Interval], fName:String, reporter:Reporter, prv:String) :(String,String)=
    if (prv == "coq")
      (s"Definition absenv_${fName} :analysisResult := \nfun (e:exp Q) =>\n" +
        coqAbsEnv(e, errorMap, rangeMap, "((0#1,0#1),0#1)", reporter) + ".",
        s"absenv_${fName}")
    else if (prv == "hol4")
      (s"val absenv_${fName}_def = Define `\n  absenv_${fName}:analysisResult = \n\\e. \n" +
        hol4AbsEnv(e, errorMap, rangeMap, "((0,1),1)", reporter) + "`;",
        s"absenv_${fName}")
    else
      ("ABS " + binaryAbsEnv(e, errorMap, rangeMap, reporter), "")

  private def getComputeExpr (lastGenName:String, analysisResultName:String,precondName:String, fName:String, prover:String) :String=
    if (prover == "coq"){
      s"Theorem ErrorBound_${fName}_Sound :\n"+
      s"CertificateCheckerCmd $lastGenName $analysisResultName $precondName = true.\n" +
        "Proof.\n  vm_compute; auto.\nQed.\n"
    } else if (prover == "hol4"){
      "val _ = store_thm (\""+s"ErrorBound_${fName}_Sound"+"\",\n" +
      s"``CertificateCheckerCmd $lastGenName $analysisResultName $precondName``,\n EVAL_TAC);\n"
    } else
      ""

  private def getAllComputeExps (e:Expr, analysisResultName:String,precondName:String, prover:String) :String=
  {
    val nameE = expressionNames(e)
    if (prover == "coq"){
      e match {
        case x @ Variable(id) =>
          "Eval compute in CertificateChecker " + nameE + " " + analysisResultName + " " + precondName + ".\n"

        case x @ RealLiteral(r) =>
          "Eval compute in CertificateChecker " + nameE + " " + analysisResultName + " " + precondName + ".\n"

        case x @ Plus(lhs, rhs) =>
          getAllComputeExps (lhs, analysisResultName, precondName, prover) +
          getAllComputeExps (rhs, analysisResultName, precondName, prover) +
          "Eval compute in CertificateChecker " + nameE + " " + analysisResultName + " " + precondName + ".\n"

        case x @ Minus(lhs, rhs) =>
          getAllComputeExps (lhs, analysisResultName, precondName, prover) +
          getAllComputeExps (rhs, analysisResultName, precondName, prover) +
          "Eval compute in CertificateChecker " + nameE + " " + analysisResultName + " " + precondName + ".\n"

        case x @ Times(lhs, rhs) =>
          getAllComputeExps (lhs, analysisResultName, precondName, prover) +
          getAllComputeExps (rhs, analysisResultName, precondName, prover) +
          "Eval compute in CertificateChecker " + nameE + " " + analysisResultName + " " + precondName + ".\n"

        case x @ _ =>
          "" //TODO Can this happen?
      }
    }
    else ""
  }
}
