(**
   This file contains the HOL4 implementation of the certificate checker as well as its soundness proof.
   The checker is a composition of the range analysis validator and the error bound validator.
   Running this function on the encoded analysis result gives the desired theorem
   as shown in the soundness theorem.
**)
open preamble
open simpLib realTheory realLib RealArith
open AbbrevsTheory ExpressionsTheory RealSimpsTheory DaisyTactics
open ExpressionAbbrevsTheory ErrorBoundsTheory IntervalArithTheory
open IntervalValidationTheory ErrorValidationTheory ssaPrgsTheory

val _ = new_theory "CertificateChecker";

(** Certificate checking function **)
val CertificateChecker_def = Define
  `CertificateChecker (e:real exp) (absenv:analysisResult) (P:precond) =
     if (validIntervalbounds e absenv P LN)
       then (validErrorbound e absenv LN)
       else F`;

val CertificateCheckerCmd_def = Define
  `CertificateCheckerCmd (f:real cmd) (absenv:analysisResult) (P:precond) =
     if (validSSA f (freeVars f))
       then
         if (validIntervalboundsCmd f absenv P LN)
           then (validErrorboundCmd f absenv LN)
           else F
       else F`;

(**
   Soundness proof for the certificate checker.
   Apart from assuming two executions, one in R and one on floats, we assume that
   the real valued execution respects the precondition.
**)
val Certificate_checking_is_sound = store_thm ("Certificate_checking_is_sound",
  ``!(e:real exp) (absenv:analysisResult) (P:precond) (E1 E2:env)
     (vR:real) (vF:real) fVars.
      approxEnv E1 absenv fVars LN E2 /\
      (!v.
          v IN (domain fVars) ==>
          ?vR.
            (E1 v = SOME vR) /\
            FST (P v) <= vR /\ vR <= SND (P v)) /\
      (domain (usedVars e)) SUBSET (domain fVars) /\
      eval_exp 0 E1 e vR /\
      eval_exp machineEpsilon E2 e vF /\
      CertificateChecker e absenv P ==>
      abs (vR - vF) <= (SND (absenv e))``,
(**
   The proofs is a simple composition of the soundness proofs for the range
   validator and the error bound validator.
**)
  simp [CertificateChecker_def]
  \\ rpt strip_tac
  \\ Cases_on `absenv e`
  \\ rename1 `absenv e = (iv, err)`
  \\ Cases_on `iv`
  \\ rename1 `absenv e = ((elo, ehi), err)`
  \\ match_mp_tac validErrorbound_sound
  \\ qexistsl_tac [`e`, `E1`, `E2`, `absenv`, `P`, `elo`, `ehi`, `fVars`, `LN`]
  \\ fs[]);

val CertificateCmd_checking_is_sound = store_thm ("CertificateCmd_checking_is_sound",
  ``!(f:real cmd) (absenv:analysisResult) (P:precond)
     (E1 E2:env) (vR vF:real).
       (* The execution environments are only off by the machine epsilon *)
       approxEnv E1 absenv (freeVars f) LN E2 /\
       (* All free variables are respecting the precondition *)
       (!v.
          v IN (domain (freeVars f)) ==>
          ?vR.
            (E1 v = SOME vR) /\
            FST (P v) <= vR /\ vR <= SND (P v)) /\
       (* Evaluations on the reals and on 1+delta floats *)
       bstep f E1 0 vR /\
       bstep f E2 machineEpsilon vF /\
       (* Certificate checking succeeds *)
       CertificateCheckerCmd f absenv P ==>
       (* Thereby we obtain a valid roundoff error *)
       abs (vR - vF) <= (SND (absenv (getRetExp f)))``,
  simp [CertificateCheckerCmd_def]
  \\ rpt strip_tac
  \\ `?outVars. ssa f (freeVars f) outVars` by (match_mp_tac validSSA_sound \\ fs[] )
  \\ match_mp_tac validErrorboundCmd_sound
  \\ Cases_on `absenv (getRetExp f)`
  \\ rename1 `absenv (getRetExp f) = (iv, err)`
  \\ Cases_on `iv`
  \\ rename1 `absenv (getRetExp f) = ((elo, ehi), err)`
  \\ qexistsl_tac [`f`, `absenv`, `E1`, `E2`, `outVars`, `freeVars f`, `LN`, `elo`, `ehi`, `P`]
  \\ fs[]);

val _ = export_theory();
