(**
  Some abbreviations that require having defined expressions beforehand
  If we would put them in the Abbrevs file, this would create a circular dependency which Coq cannot resolve.
**)
open preamble
open ExpressionsTheory

val _ = new_theory "ExpressionAbbrevs"

(**
We treat a function mapping an expression arguing on fractions as value type
to pairs of intervals on rationals and rational errors as the analysis result
**)
val _ = type_abbrev("analysisResult", ``:real exp->(interval # real)``);

val _ = export_theory()
