(**
  We define a pseudo SSA predicate.
  The formalization is similar to the renamedApart property in the LVC framework by Schneider, Smolka and Hack
  http://dblp.org/rec/conf/itp/SchneiderSH15
  Our predicate is not as fully fledged as theirs, but we especially borrow the idea of annotating
  the program with the predicate with the set of free and defined variables
**)
open preamble
open pred_setSyntax pred_setTheory
open AbbrevsTheory ExpressionsTheory CommandsTheory DaisyTactics

val _ = new_theory "ssaPrgs";

val validVars_add = store_thm("validVars_add",
``!(e:'a exp) Vs n.
     domain (usedVars e) ⊆ domain Vs ==>
     domain (usedVars e) ⊆ domain (insert n () Vs)``,
fs [domain_insert, SUBSET_DEF]);

(**
  Inductive ssa predicate, following the definitions from the LVC framework,
  see top of file for citation
  We maintain as an invariant that if we have
  ssa f
**)
val (ssa_rules, ssa_ind, ssa_cases) = Hol_reln `
  (!x e s inVars (Vterm:num_set).
     (domain (usedVars e)) SUBSET (domain inVars) /\
     (~ (x IN (domain inVars)))  /\
     ssa s (insert x () inVars) Vterm ==>
     ssa (Let x e s) inVars Vterm) /\
  (!e inVars Vterm.
     (domain (usedVars e)) SUBSET (domain inVars) /\
     (domain inVars = domain Vterm) ==>
     ssa (Ret e) inVars Vterm)`;

(*
  As usual
*)
val ssa_cases =
  map (GEN_ALL o SIMP_CONV (srw_ss()) [Once ssa_cases])
    [``ssa (Let x e s) inVars Vterm``,
     ``ssa (Ret e) inVars Vterm``]
  |> LIST_CONJ |> curry save_thm "ssa_cases";

val [ssaLet, ssaRet] = CONJ_LIST 2 ssa_rules;
save_thm ("ssaLet",ssaLet);
save_thm ("ssaRet",ssaRet);

val ssa_subset_freeVars = store_thm ("ssa_subset_freeVars",
  ``!(f:'a cmd) inVars outVars.
      ssa f inVars outVars ==>
      (domain (freeVars f)) SUBSET (domain inVars)``,
  ho_match_mp_tac ssa_ind \\ rw []
  >- (once_rewrite_tac [freeVars_def, domain_insert] \\ simp []
      \\ once_rewrite_tac [domain_union]
      \\ fs[SUBSET_DEF]
      \\ rpt strip_tac
      >- (first_x_assum match_mp_tac
          \\ simp [])
      >- (fs [ domain_insert]
          \\ metis_tac[]))
  >- (once_rewrite_tac[freeVars_def]
      \\ fs []));

val ssa_equal_set_ind = prove(
  ``!(f:'a cmd) inVars outVars.
       ssa f inVars outVars ==>
        (!inVars'.
           (domain inVars' = domain inVars) ==>
           ssa f inVars' outVars)``,
  qspec_then `\f inVars outVars.
    ∀inVars'. (domain inVars' = domain inVars) ==> ssa f inVars' outVars` (fn thm => ASSUME_TAC (SIMP_RULE std_ss [] thm)) ssa_ind
  \\ first_x_assum match_mp_tac
  \\ conj_tac \\ rw[]
  >- (match_mp_tac ssaLet \\ fs[]
      \\ first_x_assum match_mp_tac
      \\ simp[ domain_insert])
  >- (match_mp_tac ssaRet \\ fs[]));

val ssa_equal_set = store_thm ("ssa_equal_set",
  ``!(f:'a cmd) inVars outVars inVars'.
       (domain inVars' = domain inVars) /\
       ssa f inVars outVars ==>
       ssa f inVars' outVars``,
  rpt strip_tac
  \\ qspecl_then [`f`, `inVars`, `outVars`] ASSUME_TAC ssa_equal_set_ind
  \\ specialize `ssa _ _ _ ==> _` `ssa f inVars outVars`
  \\ specialize `!inVars'. A ==> B` `inVars'`
  \\ first_x_assum match_mp_tac \\ fs[]);

val validSSA_def = Define `
  validSSA (f:real cmd) (inVars:num_set) =
    case f of
      |Let x e g =>
        ((lookup x inVars = NONE) /\
         (subspt (usedVars e) inVars) /\
         (validSSA g (insert x () inVars)))
      |Ret e =>
       (subspt (usedVars e) inVars)`;

val validSSA_sound = store_thm ("validSSA_sound",
  ``!(f:real cmd) (inVars:num_set).
       (validSSA f inVars) ==>
       ?outVars.
         ssa f inVars outVars``,
  Induct \\ once_rewrite_tac [validSSA_def] \\ rw[]
  >- (specialize `!inVars. _ ==> _` `insert n () inVars`
      \\ `?outVars. ssa f (insert n () inVars) outVars` by (first_x_assum match_mp_tac \\ simp[])
      \\ qexists_tac `outVars`
      \\ match_mp_tac ssaLet
      \\ fs [subspt_def, SUBSET_DEF, lookup_NONE_domain])
  >- (exists_tac ``inVars:num_set``
      \\ match_mp_tac ssaRet
      \\ fs[subspt_def, SUBSET_DEF]));

val _ = export_theory ();
