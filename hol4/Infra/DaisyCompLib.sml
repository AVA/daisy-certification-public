structure DaisyCompLib =
struct
open computeLib;

val _ = computeLib.add_funs [sptreeTheory.subspt_eq];
val _ = computeLib.del_funs [sptreeTheory.subspt_def];

end
