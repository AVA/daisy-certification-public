(**
    Interval arithmetic checker and its soundness proof.
    The function validIntervalbounds checks wether the given analysis result is
    a valid range arithmetic for each sub term of the given expression e.
    The computation is done using our formalized interval arithmetic.
    The function is used in CertificateChecker.v to build the full checker.
**)
open preamble
open simpLib realTheory realLib RealArith pred_setTheory sptreeTheory
open AbbrevsTheory ExpressionsTheory RealSimpsTheory DaisyTactics
open ExpressionAbbrevsTheory IntervalArithTheory CommandsTheory ssaPrgsTheory
open sptreeLib sptreeTheory

val _ = new_theory "IntervalValidation";

val _ = Parse.hide "delta"; (* so that it can be used as a variable *)

val validIntervalbounds_def = Define `
  validIntervalbounds e (absenv:analysisResult) (P:precond) (validVars:num_set) =
    let (intv, _) = absenv e in
      case e of
        | Var v =>
            if (lookup v validVars = SOME ())
              then T
              else (isSupersetInterval (P v) intv /\ IVlo (P v) <= IVhi (P v))
        | Const n => isSupersetInterval (n,n) intv
        | Unop op f =>
          (if validIntervalbounds f absenv P validVars
            then
              let (iv, _) = absenv f in
                case op of
                  | Neg =>
                    let new_iv = negateInterval iv in
                      isSupersetInterval new_iv intv
                  | Inv =>
                    if IVhi iv < 0 \/ 0 < IVlo iv
                      then
                        let new_iv = invertInterval iv in
                          isSupersetInterval new_iv intv
                       else
                         F
            else F)
        | Binop op f1 f2 =>
          (if (validIntervalbounds f1 absenv P validVars /\ validIntervalbounds f2 absenv P validVars)
            then
              let (iv1, _ ) = absenv f1 in
              let (iv2, _) = absenv f2 in
                case op of
                  | Plus =>
                    let new_iv = addInterval iv1 iv2 in
                      isSupersetInterval new_iv intv
                  | Sub =>
                    let new_iv = subtractInterval iv1 iv2 in
                      isSupersetInterval new_iv intv
                  | Mult =>
                     let new_iv = multInterval iv1 iv2 in
                       isSupersetInterval new_iv intv
                  | Div =>
                     if (IVhi iv2 < 0 \/ 0 < IVlo iv2)
                       then
                         let new_iv = divideInterval iv1 iv2 in
                           isSupersetInterval new_iv intv
                       else F
            else F)`;

val validIntervalboundsCmd_def = Define `
  validIntervalboundsCmd (f:real cmd) (absenv:analysisResult) (P:precond) (validVars:num_set) =
    case f of
     | Let x e g =>
       if (validIntervalbounds e absenv P validVars /\
          (FST (absenv e) = FST (absenv (Var x))))
         then validIntervalboundsCmd g absenv P (insert x () validVars)
         else F
   | Ret e =>
     (validIntervalbounds e absenv P validVars)`;

val ivbounds_approximatesPrecond_sound = store_thm ("ivbounds_approximatesPrecond_sound",
``!(f:real exp) (absenv:analysisResult) (P:precond) (V:num_set).
   validIntervalbounds f absenv P V ==>
   (!(v:num). v IN ((domain (usedVars f)) DIFF (domain V)) ==>
           isSupersetInterval (P v) (FST (absenv (Var v))))``,
  Induct_on `f` \\ once_rewrite_tac [usedVars_def] \\ rpt strip_tac \\ fs[]
  >- (rule_assum_tac (ONCE_REWRITE_RULE [validIntervalbounds_def])
      \\ fs [domain_lookup, usedVars_def, lookup_insert]
      \\ `v = n` by (Cases_on `v = n` \\ fs[lookup_def])
      \\ rveq
      \\ Cases_on `absenv (Var n)`
      \\ Cases_on `lookup n V`
      \\ fs[])
  >- (rpt (first_x_assum (fn thm => qspecl_then [`absenv`, `P`, `V`] ASSUME_TAC thm))
      \\ `validIntervalbounds f absenv P V`
           by (qpat_x_assum `validIntervalbounds _ _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validIntervalbounds_def] thm))
               \\ Cases_on `absenv (Unop u f)` \\ fs[])
      \\ specialize `validIntervalbounds f _ _ _ ==> _` `validIntervalbounds f _ _ _`
      \\ first_x_assum match_mp_tac
      \\ simp[])
  >- (rpt (first_x_assum (fn thm => qspecl_then [`absenv`, `P`, `V`] ASSUME_TAC thm))
      \\ fs [domain_union]
      \\ `validIntervalbounds f absenv P V`
           by (qpat_x_assum `validIntervalbounds _ _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validIntervalbounds_def] thm))
               \\ Cases_on `absenv (Binop b f f')` \\ fs[])
      \\ `validIntervalbounds f' absenv P V`
            by (qpat_x_assum `validIntervalbounds (Binop b f f') _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validIntervalbounds_def] thm))
                \\ Cases_on `absenv (Binop b f f')` \\ fs[])
       \\ fs[domain_union]));

val validIntervalbounds_sound = store_thm ("validIntervalbounds_sound",
  ``!(f:real exp) (absenv:analysisResult) (P:precond) (fVars:num_set) dVars E vR.
       validIntervalbounds f absenv P dVars /\
       (!v. v IN (domain dVars) ==>
         (?vR. (E v = SOME vR) /\
         (FST (FST (absenv (Var v))) <= vR /\ vR <= SND (FST (absenv (Var v)))))) /\
       (((domain (usedVars f)) DIFF (domain dVars)) SUBSET (domain fVars)) /\
       (!v. v IN (domain fVars) ==>
         (?vR. (E v = SOME vR) /\
         (FST (P v) <= vR /\ vR <= SND (P v)))) /\
       eval_exp 0 E f vR ==>
       (FST (FST (absenv f))) <= vR /\ vR <= (SND (FST (absenv f)))``,
  Induct \\ once_rewrite_tac [eval_exp_cases] \\ rpt gen_tac \\ strip_tac
  (* Variable case *)
  >- (rw_thm_asm `validIntervalbounds _ _ _ _` validIntervalbounds_def
      \\ Cases_on `absenv (Var n)` \\ Cases_on `lookup n dVars` \\ fs[]
      >- (specialize `!v. v IN domain fVars ==> _` `n`
          \\ fs [usedVars_def]
          \\ `~ (n IN (domain dVars))` by (fs[domain_lookup])
          \\ `n IN domain fVars` by fs[]
          \\ fs [isSupersetInterval_def, IVlo_def, IVhi_def]
          \\ conj_tac
          >- (match_mp_tac REAL_LE_TRANS \\ asm_exists_tac
              \\ fs [] \\ rveq \\ simp[])
          >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `SND (P n)`\\ fs [] \\ rveq \\ simp []))
      >- (specialize `!v. v IN domain dVars ==> _` `n`
          \\ `n IN domain dVars` by (fs [domain_lookup])
          \\ qpat_x_assum `absenv (Var n) = _` (fn thm => fs[thm])
          \\ qpat_x_assum `E n = Some vR` (fn thm => fs[thm])))
  (* Const case *)
  >- (Cases_on `absenv (Const v)`
      \\ `perturb v delta = v` by (qspecl_then [`v`, `delta`] match_mp_tac delta_0_deterministic \\ fs[])
      \\fs [validIntervalbounds_def, isSupersetInterval_def, IVhi_def, IVlo_def])
  (* Negation case *)
  >- (simp[evalUnop_def]
      \\ rw_thm_asm `validIntervalbounds (unop u f) absenv P dVars` validIntervalbounds_def
      \\ Cases_on `absenv (Unop u f)`
      \\ qmatch_assum_rename_tac `absenv (Unop u f) = (iv_u, err_u)`
      \\ Cases_on `absenv f`
      \\ qmatch_assum_rename_tac `absenv f = (iv_f, err_f)`
      \\ rveq
      \\ fs[evalUnop_def, isSupersetInterval_def, IVlo_def, IVhi_def, negateInterval_def]
      \\ first_x_assum (fn thm => qspecl_then [`absenv`,`P`, `fVars`, `dVars`, `E`,`v1`] ASSUME_TAC thm)
      \\ qpat_x_assum `absenv f = _` (fn thm => fs[thm])
      \\ `FST iv_f <= v1 /\ v1 <= SND iv_f`
           by (first_x_assum match_mp_tac
               \\ conj_tac \\ fs [Once usedVars_def])
      \\ qpat_x_assum `! _. Q ==> R` kall_tac
      \\ fs[] \\ conj_tac \\ match_mp_tac REAL_LE_TRANS
         >- (qexists_tac `- SND iv_f` \\ conj_tac \\ simp [])
         >- (qexists_tac `- FST iv_f` \\ conj_tac \\ simp []))
  (* Inversion case *)
  >- (simp[evalUnop_def]
      \\ `perturb (evalUnop Inv v1) delta = evalUnop Inv v1`
           by (qspecl_then [`evalUnop Inv v1`, `delta`] match_mp_tac delta_0_deterministic \\ fs[])
      \\ `perturb (inv v1) delta = inv v1` by (fs[evalUnop_def])
      \\ simp[]
      \\ rw_thm_asm `validIntervalbounds a b c d` validIntervalbounds_def
      \\ first_x_assum (fn thm => qspecl_then [`absenv`,`P`, `fVars`, `dVars`, `E`, `v1`] ASSUME_TAC thm)
      \\ Cases_on `absenv (Unop u f)`
      \\ qmatch_assum_rename_tac `absenv (Unop u f) = (iv_u, err_u)`
      \\ Cases_on `absenv f`
      \\ qmatch_assum_rename_tac `absenv f = (iv_f, err_f)`
      \\ rveq
      \\ qpat_x_assum `validIntervalbounds _ _ _ _ /\ _ /\ _ /\ _ /\ _ ==> _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [FST, SND] thm))
      \\ `FST iv_f <= v1 /\ v1 <= SND iv_f` by (first_x_assum match_mp_tac \\ fs[Once usedVars_def])
      \\ rpt (qpat_x_assum `!v. _ ==> _` kall_tac)
      \\ conj_tac \\ match_mp_tac REAL_LE_TRANS
         >- (qexists_tac `inv (SND iv_f)` \\ conj_tac
             \\ TRY(fs[evalUnop_def, isSupersetInterval_def, IVlo_def, IVhi_def, invertInterval_def, GSYM REAL_INV_1OVER])
            >- (`0 < - SND iv_f` by REAL_ASM_ARITH_TAC
                \\ `0 < -v1` by REAL_ASM_ARITH_TAC
                \\ `-SND iv_f <= -v1` by REAL_ASM_ARITH_TAC
                \\ `inv (-v1) <= inv (- SND iv_f)` by fs[REAL_INV_LE_ANTIMONO]
                \\ `inv(-v1) = - (inv v1)` by (match_mp_tac (GSYM REAL_NEG_INV) \\ REAL_ASM_ARITH_TAC)
                \\ `inv(-(SND iv_f)) = - (inv (SND iv_f))` by (match_mp_tac (GSYM REAL_NEG_INV) \\ REAL_ASM_ARITH_TAC)
                \\ `-(inv v1) <= - (inv (SND iv_f))` by fs []
                \\ fs[])
            >- (`0 < v1` by REAL_ASM_ARITH_TAC \\
                `0 < SND iv_f` by REAL_ASM_ARITH_TAC \\
                fs[REAL_INV_LE_ANTIMONO]))
         >- (qexists_tac `inv (FST iv_f)` \\ conj_tac
             \\  TRY(fs[evalUnop_def, isSupersetInterval_def, IVlo_def, IVhi_def, invertInterval_def, GSYM REAL_INV_1OVER])
            >- (`0 < - SND iv_f` by REAL_ASM_ARITH_TAC
                \\ `0 < -v1` by REAL_ASM_ARITH_TAC
                \\ `0 < - (FST iv_f)` by REAL_ASM_ARITH_TAC
                \\ `- v1 <= - (FST iv_f)` by REAL_ASM_ARITH_TAC
                \\ `inv (- FST iv_f) <= inv (- v1)` by fs[REAL_INV_LE_ANTIMONO]
                \\ `inv(- FST iv_f) = - (inv (FST iv_f))` by (match_mp_tac (GSYM REAL_NEG_INV) \\ REAL_ASM_ARITH_TAC)
                \\ `inv(- v1) = - (inv v1)` by (match_mp_tac (GSYM REAL_NEG_INV) \\ REAL_ASM_ARITH_TAC)
                \\ `-(inv (FST iv_f)) <= - (inv v1)` by fs []
                \\ fs[])
            >- (`0 < v1` by REAL_ASM_ARITH_TAC \\
                `0 < FST iv_f` by REAL_ASM_ARITH_TAC \\
                fs[REAL_INV_LE_ANTIMONO])))
  (* Binary operator case *)
  >- (`perturb (evalBinop b v1 v2) delta = evalBinop b v1 v2`
        by (qspecl_then [`evalBinop b v1 v2`, `delta`] match_mp_tac delta_0_deterministic \\ fs[])
      \\ `vR = evalBinop b v1 v2` by fs[]
      \\ rw_thm_asm `validIntervalbounds (Binop b f f') absenv P V` validIntervalbounds_def
      \\ rpt (first_x_assum (fn thm => qspecl_then [`absenv`,`P`, `fVars`, `dVars`, `E`] ASSUME_TAC thm))
      \\ Cases_on `absenv (Binop b f f')` \\ Cases_on `absenv f` \\ Cases_on `absenv f'`
      \\ qmatch_assum_rename_tac `absenv (Binop b f f') = (iv_b,err_b)`
      \\ qmatch_assum_rename_tac `absenv f = (iv_f,err_f)`
      \\ qmatch_assum_rename_tac `absenv f' = (iv_f',err_f')`
      \\ `validIntervalbounds f absenv P dVars /\ validIntervalbounds f' absenv P dVars` by fs[]
      \\ Cases_on `b` \\ simp[evalBinop_def]
     (* Plus case *)
     >- (fs[evalBinop_def, isSupersetInterval_def, absIntvUpd_def, IVlo_def, IVhi_def, addInterval_def]
         \\ `FST iv_f <= v1 /\ v1 <= SND iv_f`
              by (first_x_assum match_mp_tac
                  \\ fs[Once usedVars_def, domain_union, UNION_DEF, DIFF_DEF, SUBSET_DEF]
                  \\ rpt strip_tac
                  \\ first_x_assum match_mp_tac \\ fs[]
                  \\ DISJ1_TAC \\ simp[Once usedVars_def])
         \\ `FST iv_f' <= v2 /\ v2 <= SND iv_f'`
              by (first_x_assum match_mp_tac
                  \\ fs[Once usedVars_def, domain_union, UNION_DEF, DIFF_DEF, SUBSET_DEF]
                  \\ rpt strip_tac
                  \\ first_x_assum match_mp_tac \\ fs[]
                  \\ DISJ2_TAC \\ simp[Once usedVars_def])
         \\ qspecl_then [`iv_f`, `iv_f'`, `v1`, `v2`] ASSUME_TAC
           (REWRITE_RULE
                [validIntervalAdd_def, addInterval_def, absIntvUpd_def, contained_def, IVhi_def, IVlo_def] interval_addition_valid)
         \\ conj_tac \\ match_mp_tac REAL_LE_TRANS
        >- (qexists_tac `min4 (FST iv_f + FST iv_f') (FST iv_f + SND iv_f') (SND iv_f + FST iv_f') (SND iv_f + SND iv_f')` \\
            metis_tac [])
        >- (qexists_tac `max4 (FST iv_f + FST iv_f') (FST iv_f + SND iv_f') (SND iv_f + FST iv_f') (SND iv_f + SND iv_f')` \\
            metis_tac []))
     (* Sub case *)
     >- (fs[evalBinop_def, isSupersetInterval_def, absIntvUpd_def, IVlo_def, IVhi_def, subtractInterval_def, addInterval_def, negateInterval_def]
         \\ `FST iv_f <= v1 /\ v1 <= SND iv_f`
              by (first_x_assum match_mp_tac
                  \\ fs[Once usedVars_def, domain_union, UNION_DEF, DIFF_DEF, SUBSET_DEF]
                  \\ rpt strip_tac
                  \\ first_x_assum match_mp_tac \\ fs[]
                  \\ DISJ1_TAC \\ simp[Once usedVars_def])
         \\ `FST iv_f' <= v2 /\ v2 <= SND iv_f'`
              by (first_x_assum match_mp_tac
                  \\ fs[Once usedVars_def, domain_union, UNION_DEF, DIFF_DEF, SUBSET_DEF]
                  \\ rpt strip_tac
                  \\ first_x_assum match_mp_tac \\ fs[]
                  \\ DISJ2_TAC \\ simp[Once usedVars_def])
         \\ qspecl_then [`iv_f`, `iv_f'`, `v1`, `v2`] ASSUME_TAC
           (REWRITE_RULE
              [validIntervalSub_def, subtractInterval_def, addInterval_def, negateInterval_def, absIntvUpd_def, contained_def, IVhi_def, IVlo_def]interval_subtraction_valid)
         \\ conj_tac \\ match_mp_tac REAL_LE_TRANS
        >- (qexists_tac `min4 (FST iv_f + -SND iv_f') (FST iv_f + -FST iv_f') (SND iv_f + -SND iv_f') (SND iv_f + -FST iv_f')` \\
            metis_tac [])
        >- (qexists_tac `max4 (FST iv_f + -SND iv_f') (FST iv_f + -FST iv_f') (SND iv_f + -SND iv_f') (SND iv_f + -FST iv_f')` \\
            metis_tac []))
     (* Mult case *)
     >- (fs[evalBinop_def, isSupersetInterval_def, absIntvUpd_def, IVlo_def, IVhi_def, multInterval_def]
         \\ `FST iv_f <= v1 /\ v1 <= SND iv_f`
              by (first_x_assum match_mp_tac
                  \\ fs[Once usedVars_def, domain_union, UNION_DEF, DIFF_DEF, SUBSET_DEF]
                  \\ rpt strip_tac
                  \\ first_x_assum match_mp_tac \\ fs[]
                  \\ DISJ1_TAC \\ simp[Once usedVars_def])
         \\ `FST iv_f' <= v2 /\ v2 <= SND iv_f'`
              by (first_x_assum match_mp_tac
                  \\ fs[Once usedVars_def, domain_union, UNION_DEF, DIFF_DEF, SUBSET_DEF]
                  \\ rpt strip_tac
                  \\ first_x_assum match_mp_tac \\ fs[]
                  \\ DISJ2_TAC \\ simp[Once usedVars_def])
         \\ qspecl_then [`iv_f`, `iv_f'`, `v1`, `v2`] ASSUME_TAC
           (REWRITE_RULE
              [validIntervalAdd_def, multInterval_def, absIntvUpd_def, contained_def, IVhi_def, IVlo_def]interval_multiplication_valid)
         \\ conj_tac \\ match_mp_tac REAL_LE_TRANS
        >- (qexists_tac `min4 (FST iv_f * FST iv_f') (FST iv_f * SND iv_f') (SND iv_f * FST iv_f') (SND iv_f * SND iv_f')` \\
           metis_tac [])
        >- (qexists_tac `max4 (FST iv_f * FST iv_f') (FST iv_f * SND iv_f') (SND iv_f * FST iv_f') (SND iv_f * SND iv_f')` \\
           metis_tac []))
     (* Div case *)
     >- (`FST (FST (iv_f,err_f)) <= v1 /\ v1 <= SND (FST (iv_f,err_f))`
           by (first_x_assum match_mp_tac
               \\ fs[Once usedVars_def, domain_union, UNION_DEF, DIFF_DEF, SUBSET_DEF]
               \\ rpt strip_tac
               \\ first_x_assum match_mp_tac \\ fs[]
               \\ DISJ1_TAC \\ simp[Once usedVars_def])
         \\ `FST (FST (iv_f', err_f')) <= v2 /\ v2 <= SND (FST (iv_f', err_f'))`
              by (first_x_assum match_mp_tac
                  \\ fs[Once usedVars_def, domain_union, UNION_DEF, DIFF_DEF, SUBSET_DEF]
                  \\ rpt strip_tac
                  \\ first_x_assum match_mp_tac \\ fs[]
                  \\ DISJ2_TAC \\ simp[Once usedVars_def])
         \\ qspecl_then [`iv_f`, `iv_f'`, `v1`, `v2`] ASSUME_TAC
              (REWRITE_RULE
                [validIntervalSub_def, divideInterval_def, multInterval_def, invertInterval_def, absIntvUpd_def, contained_def, IVhi_def, IVlo_def, GSYM REAL_INV_1OVER]interval_division_valid)
         \\ fs[evalBinop_def, isSupersetInterval_def, absIntvUpd_def, IVlo_def, IVhi_def, divideInterval_def, multInterval_def, invertInterval_def, GSYM REAL_INV_1OVER]
         \\ conj_tac \\ match_mp_tac REAL_LE_TRANS
        >- (qexists_tac `min4 (FST iv_f * (inv (SND iv_f')))
             (FST iv_f * (inv (FST iv_f'))) (SND iv_f * (inv (SND iv_f'))) (SND iv_f * (inv (FST iv_f')))` \\
           metis_tac [])
        >- (qexists_tac `max4 (FST iv_f * (inv (SND iv_f')))
             (FST iv_f * (inv (FST iv_f'))) (SND iv_f * (inv (SND iv_f'))) (SND iv_f * (inv (FST iv_f')))` \\
           metis_tac [])
        >- (qexists_tac `min4 (FST iv_f * (inv (SND iv_f')))
             (FST iv_f * (inv (FST iv_f'))) (SND iv_f * (inv (SND iv_f'))) (SND iv_f * (inv (FST iv_f')))` \\
           metis_tac [])
        >- (qexists_tac `max4 (FST iv_f * (inv (SND iv_f')))
             (FST iv_f * (inv (FST iv_f'))) (SND iv_f * (inv (SND iv_f'))) (SND iv_f * (inv (FST iv_f')))` \\
           metis_tac []))));

val getRetExp_def = Define `
  getRetExp (f:'a cmd) =
    case f of
     |Let x e g => getRetExp g
     |Ret e => e`;

val validIntervalboundsCmd_sound = store_thm ("validIntervalboundsCmd_sound",
  ``!f (absenv:analysisResult) E vR (fVars dVars outVars:num_set) elo ehi err P.
      ssa f (union fVars dVars) outVars /\
      bstep f E 0 vR /\
      (!v. v IN (domain dVars) ==>
         ?vR.
           (E v = SOME vR) /\
           (FST (FST (absenv (Var v))) <= vR /\
            vR <= SND (FST (absenv (Var v))))) /\
       (!v. v IN (domain fVars) ==>
          ?vR.
            (E v = SOME vR) /\
            (FST (P v)) <= vR /\ vR <= SND (P v)) /\
      (((domain (freeVars f)) DIFF (domain dVars)) SUBSET (domain fVars)) /\
      validIntervalboundsCmd f absenv P dVars /\
      (absenv (getRetExp f) = ((elo, ehi), err)) ==>
      elo <= vR /\ vR <= ehi``,
  gen_tac \\ Induct_on `f`
  \\ once_rewrite_tac [validIntervalboundsCmd_def]
  \\ rpt gen_tac \\ strip_tac
  >- (inversion `ssa _ _ _` ssa_cases
      \\ inversion `bstep _  _ _ _` bstep_cases
      \\ fs []
      \\ first_x_assum match_mp_tac
      \\ qexistsl_tac [`absenv`, `updEnv n v E`, `fVars`, `insert n () dVars`, `outVars`, `err`, `P`]
      \\ fs [updEnv_def]
      \\ rpt conj_tac
      >- (match_mp_tac ssa_equal_set
          \\ qexists_tac `(insert n () (union fVars dVars))`
          \\ fs [domain_union,  domain_insert]
          \\ once_rewrite_tac [UNION_COMM]
          \\ fs [INSERT_UNION_EQ, UNION_COMM])
      >- (rpt strip_tac
          \\ Cases_on `v' = n` \\ fs[]
          >- (rveq
              \\ rw_sym_asm `FST (absenv e) = _`
              \\ match_mp_tac validIntervalbounds_sound
              \\ qexistsl_tac [`P`, `fVars`, `dVars`, `E`] \\ fs [SUBSET_DEF]
              \\ rpt strip_tac
              \\ first_x_assum match_mp_tac \\ fs [Once freeVars_def, domain_union]
              \\ metis_tac [])
          >- (rveq
              \\ rw_sym_asm `FST (absenv e) = _`
              \\ match_mp_tac validIntervalbounds_sound
              \\ qexistsl_tac [`P`, `fVars`, `dVars`, `E`] \\ fs [SUBSET_DEF]
              \\ rpt strip_tac
              \\ first_x_assum match_mp_tac \\ fs [Once freeVars_def, domain_union]
              \\ metis_tac []))
      >- (rpt strip_tac
          \\ Cases_on `v' = n` \\ fs[domain_union])
      >- (fs [ domain_insert, SUBSET_DEF]
          \\ rpt strip_tac
          \\ first_x_assum match_mp_tac
          \\ once_rewrite_tac [freeVars_def, domain_union]
          \\ fs [domain_union])
      >- (fs [Once getRetExp_def]))
  >- (inversion `ssa _ _ _` ssa_cases
      \\ inversion `bstep _ _ _ _` bstep_cases
      \\ fs [updEnv_def, getRetExp_def]
      \\ drule validIntervalbounds_sound
      \\ disch_then (qspecl_then [`fVars`, `E`, `vR`] ASSUME_TAC)
      \\ qpat_x_assum `absenv _ = _` (fn thm => fs [thm])
      \\ first_x_assum match_mp_tac
      \\ fs [SUBSET_DEF]
      \\ rpt strip_tac
      \\ qpat_x_assum `domain (union fVars dVars) = _` (fn thm => fs [GSYM thm])
      \\ specialize `!x'. x' IN domain (usedVars e) ==> _` `x`
      \\ specialize `x IN domain (usedVars e) ==> _` `x IN domain (usedVars e)`
      \\ fs [domain_union]
      \\ CCONTR_TAC \\ metis_tac []));

val validIntervalbounds_noDivzero_real = store_thm("validIntervalbounds_noDivzero_real",
  ``!(f1 f2:real exp) (absenv:analysisResult) (P:precond) (dVars:num_set).
      validIntervalbounds (Binop Div f1 f2) absenv P dVars ==>
      noDivzero (SND (FST (absenv f2))) (FST (FST (absenv f2)))``,
  rpt strip_tac
  \\ Cases_on `absenv f2` \\ Cases_on `absenv f1` \\ Cases_on `absenv (Binop Div f1 f2)`
  \\ fs [Once validIntervalbounds_def, noDivzero_def, IVhi_def, IVlo_def]);

val validIntervalbounds_validates_iv = store_thm ("validIntervalbounds_validates_iv",
  ``!(f:real exp) (absenv:analysisResult) (P:precond) (dVars:num_set).
      (!v. v IN domain dVars ==>
           valid (FST (absenv (Var v)))) /\
      validIntervalbounds f absenv P dVars ==>
      valid (FST (absenv f))``,
  Induct
  \\ rpt strip_tac
  >- (first_x_assum (fn thm => qspecl_then [`n`] ASSUME_TAC thm)
      \\ Cases_on `absenv (Var n)`
      \\ fs[domain_lookup, valid_def, isSupersetInterval_def, validIntervalbounds_def]
      \\ REAL_ASM_ARITH_TAC)
  >- (rpt strip_tac
      \\ Cases_on `absenv (Const v)` \\ fs[isSupersetInterval_def, valid_def, validIntervalbounds_def]
      \\ match_mp_tac REAL_LE_TRANS
      \\ asm_exists_tac \\ CONJ_TAC \\ fs[IVlo_def, IVhi_def])
  >- (qpat_x_assum `validIntervalbounds _ _ _ _`
        (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE[validIntervalbounds_def] thm))
      \\ Cases_on `absenv (Unop u f)` \\ fs[]
      \\ `valid (FST (absenv f))`
           by (first_x_assum match_mp_tac \\
               qexists_tac `P` \\ qexists_tac `dVars`
               \\ fs[])
      \\ Cases_on `absenv f` \\ fs[]
      \\ Cases_on `u`
      >- (`valid (negateInterval q')`
            by (match_mp_tac iv_neg_preserves_valid \\ fs[])
          \\ fs[valid_def, isSupersetInterval_def]
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs[]
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs[])
      >- (`valid (invertInterval q')`
            by (match_mp_tac iv_inv_preserves_valid \\ fs[])
          \\ fs[valid_def, isSupersetInterval_def]
          >- (match_mp_tac REAL_LE_TRANS
              \\ asm_exists_tac \\ fs[]
              \\ match_mp_tac REAL_LE_TRANS
              \\ asm_exists_tac \\ fs[])
          >- (match_mp_tac REAL_LE_TRANS
              \\ asm_exists_tac \\ fs[]
              \\ match_mp_tac REAL_LE_TRANS
              \\ asm_exists_tac \\ fs[])))
  >- (rename1 `Binop b f1 f2`
      \\ qpat_x_assum `validIntervalbounds _ _ _ _`
           (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE[validIntervalbounds_def] thm))
      \\ Cases_on `absenv (Binop b f1 f2)` \\ rename1 `absenv (Binop b f1 f2) = (iv,err)`
      \\ fs[]
      \\ `valid (FST (absenv f1))`
           by (first_x_assum match_mp_tac
               \\ qexists_tac `P` \\ qexists_tac `dVars`
               \\ fs[])
      \\ `valid (FST (absenv f2))`
           by (first_x_assum match_mp_tac
               \\ qexists_tac `P` \\ qexists_tac `dVars`
               \\ fs[])
      \\ Cases_on `absenv f1` \\ Cases_on `absenv f2`
      \\ rename1 `absenv f1 = (iv_f1, err_f1)`
      \\ rename1 `absenv f2 = (iv_f2, err_f2)`
      \\ fs[]
      \\ Cases_on `b`
      >- (`valid (addInterval iv_f1 iv_f2)`
            by (match_mp_tac iv_add_preserves_valid \\ fs[])
          \\ fs[valid_def, isSupersetInterval_def]
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs []
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs [])
      >- (`valid (subtractInterval iv_f1 iv_f2)`
            by (match_mp_tac iv_sub_preserves_valid \\ fs[])
          \\ fs[valid_def, isSupersetInterval_def]
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs []
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs [])
      >- (`valid (multInterval iv_f1 iv_f2)`
            by (match_mp_tac iv_mult_preserves_valid \\ fs[])
          \\ fs[valid_def, isSupersetInterval_def]
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs []
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs [])
      >- (`valid (divideInterval iv_f1 iv_f2)`
            by (match_mp_tac iv_div_preserves_valid \\ fs[])
          \\ fs[valid_def, isSupersetInterval_def]
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs []
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac \\ fs [])));

val _ = export_theory();
