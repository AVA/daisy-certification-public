(**
 Formalization of the Abstract Syntax Tree of a subset used in the Daisy framework
 **)
open preamble
open simpLib realTheory realLib RealArith
open AbbrevsTheory ExpressionAbbrevsTheory

val _ = new_theory "Commands";

(**
  Next define what a program is.
  Currently no loops, or conditionals.
  Only assignments and return statement
**)
val _ = Datatype `
  cmd = Let num ('v exp) cmd
       | Ret ('v exp)`;

(**
  Define big step semantics for the Daisy language, terminating on a "returned"
  result value
 **)
val (bstep_rules, bstep_ind, bstep_cases) = Hol_reln `
  (!x e s E v eps vR.
      eval_exp eps E e v /\
      bstep s (updEnv x v E) eps vR ==>
      bstep (Let x e s) E eps vR) /\
  (!e E v eps.
      eval_exp eps E e v ==>
      bstep (Ret e) E eps v)`;

(**
  Generate a better case lemma again
**)
val bstep_cases =
  map (GEN_ALL o SIMP_CONV (srw_ss()) [Once bstep_cases])
    [``bstep (Let x e s) E eps vR``,
     ``bstep (Ret e) E eps vR``]
  |> LIST_CONJ |> curry save_thm "bstep_cases";

val [let_b, ret_b] = CONJ_LIST 2 bstep_rules;
save_thm ("let_b", let_b);
save_thm ("ret_b", ret_b);

(**
  The free variables of a command are all used variables of expressions
  without the let bound variables
**)
val freeVars_def = Define `
  freeVars (f: 'a cmd) :num_set =
    case f of
      |Let x e g => delete x (union (usedVars e) (freeVars g))
      |Ret e => usedVars e`;

(**
  The defined variables of a command are all let bound variables
**)
val definedVars_def = Define `
  definedVars (f:'a cmd) :num_set =
    case f of
      |Let (x:num) e g => insert x () (definedVars g)
      |Ret e => LN`;

val _ = export_theory ();
