open preamble
open simpLib realTheory realLib RealArith sptreeTheory
open AbbrevsTheory ExpressionAbbrevsTheory RealSimpsTheory CommandsTheory

val _ = new_theory "Environments";

val (approxEnv_rules, approxEnv_ind, approxEnv_cases) = Hol_reln `
  (!(A:analysisResult).
      approxEnv emptyEnv A LN LN emptyEnv) /\
  (!(E1:env) (E2:env) (A:analysisResult) (fVars:num_set) (dVars:num_set) v1 v2 x.
      approxEnv E1 A fVars dVars E2 /\
      (abs (v1 - v2) <= abs v1 * machineEpsilon) /\
      (lookup x (union fVars dVars) = NONE) ==>
      approxEnv (updEnv x v1 E1) A (insert x () fVars) dVars (updEnv x v2 E2)) /\
  (!(E1:env) (E2:env) (A:analysisResult) (fVars:num_set) (dVars:num_set) v1 v2 x.
      approxEnv E1 A fVars dVars E2 /\
      (abs (v1 - v2) <= SND (A (Var x))) /\
      (lookup x (union fVars dVars) = NONE) ==>
      approxEnv (updEnv x v1 E1) A fVars (insert x () dVars) (updEnv x v2 E2))`;

val [approxRefl, approxUpdFree, approxUpdBound] = CONJ_LIST 3 approxEnv_rules;
save_thm ("approxRefl", approxRefl);
save_thm ("approxUpdFree", approxUpdFree);
save_thm ("approxUpdBound", approxUpdBound);

val _ = export_theory ();;
