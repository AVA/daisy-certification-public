open preamble compilationLib transTheory

val _ = new_theory "checkerBinary"

val checker_compiled = save_thm("checker_compiled",
  compile_x64 500 500 "checker" transTheory.entire_program_def);

val _ = export_theory ();
