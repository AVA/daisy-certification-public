(**
  Formalization of the base expression language for the daisy framework
 **)
open preamble miscTheory
open realTheory realLib sptreeTheory
open AbbrevsTheory

val _ = ParseExtras.temp_tight_equality()

val _ = new_theory "Expressions";

(**
  Expressions will use binary operators.
  Define them first
**)
val _ = Datatype `
  binop = Plus | Sub | Mult | Div`;

(**
  Next define an evaluation function for binary operators.
  Errors are added on the expression evaluation level later
*)
val evalBinop_def = Define `
  evalBinop Plus v1 v2 = v1 + v2 /\
  evalBinop Sub  v1 v2 = v1 - v2 /\
  evalBinop Mult v1 v2 = v1 * v2 /\
  evalBinop Div  v1 v2 = v1 / (v2:real)`;

(**
Expressions will use unary operators.
Define them first
**)
val _ = Datatype `
  unop = Neg | Inv`

(**
Define evaluation for unary operators on reals.
Errors are added in the expression evaluation level later
**)
val evalUnop_def = Define `
  evalUnop Neg v = - v /\
  evalUnop Inv v = inv(v:real)`

(**
  Define Expressions parametric over some value type 'v.
  Will ease reasoning about different instantiations later.
**)
val _ = Datatype `
  exp = Var num
      | Const 'v
      | Unop unop exp
      | Binop binop exp exp`

(**
  Define a perturbation function to ease writing of basic definitions
**)
val perturb_def = Define `
  perturb (r:real) (e:real) = r * (1 + e)`

(**
Define expression evaluation relation parametric by an "error" epsilon.
The result value expresses float computations according to the IEEE standard,
using a perturbation of the real valued computation by (1 + delta), where
|delta| <= machine epsilon.
**)
val (eval_exp_rules, eval_exp_ind, eval_exp_cases) = Hol_reln `
  (!eps E x v.
     E x = SOME v ==>
     eval_exp eps E (Var x) v) /\
  (!eps E n delta.
     abs delta <= eps ==>
     eval_exp eps E (Const n) (perturb n delta)) /\
  (!eps E f1 v1.
     eval_exp eps E f1 v1 ==>
     eval_exp eps E (Unop Neg f1) (evalUnop Neg v1)) /\
  (!eps E f1 v1 delta.
     abs delta <= eps /\
     eval_exp eps E f1 v1 /\
    (~ v1 = 0) ==>
     eval_exp eps E (Unop Inv f1) (perturb (evalUnop Inv v1) delta)) /\
  (!eps E b f1 f2 v1 v2 delta.
     abs delta <= eps /\
     eval_exp eps E f1 v1 /\
     eval_exp eps E f2 v2 /\
    ((b = Div) ==> (~ v2 = 0)) ==>
     eval_exp eps E (Binop b f1 f2) (perturb (evalBinop b v1 v2) delta))`;

(**
  Generate a better case lemma
**)
val eval_exp_cases =
  map (GEN_ALL o SIMP_CONV (srw_ss()) [Once eval_exp_cases])
    [``eval_exp eps E (Var v) res``,
     ``eval_exp eps E (Const n) res``,
     ``eval_exp eps E (Unop u e) res``,
     ``eval_exp eps E (Binop n e1 e2) res``]
  |> LIST_CONJ |> curry save_thm "eval_exp_cases";

val [Var_load, Const_dist, Unop_neg, Unop_inv, Binop_dist] = CONJ_LIST 5 eval_exp_rules;
save_thm ("Var_load", Var_load);
save_thm ("Const_dist", Const_dist);
save_thm ("Unop_neg", Unop_neg);
save_thm ("Unop_inv", Unop_inv);
save_thm ("Binop_dist", Binop_dist);

(**
  Define the set of "used" variables of an expression to be the set of variables
  occuring in it
**)
val usedVars_def = Define `
  usedVars (e: 'a exp) :num_set =
    case e of
      | Var x => insert x () (LN)
      | Unop u e1 => usedVars e1
      | Binop b e1 e2 => union (usedVars e1) (usedVars e2)
      | _ => LN`;

(**
  If |delta| <= 0 then perturb v delta is exactly v.
**)
val delta_0_deterministic = store_thm("delta_0_deterministic",
  ``!(v:real) (delta:real). abs delta <= 0 ==> perturb v delta = v``,
  fs [perturb_def,ABS_BOUNDS,REAL_LE_ANTISYM]);

(**
Evaluation with 0 as machine epsilon is deterministic
**)
val meps_0_deterministic = store_thm("meps_0_deterministic",
  ``!(e: real exp) v1:real v2:real E.
      eval_exp (&0) E e v1 /\ eval_exp (&0) E e v2 ==> v1 = v2``,
  Induct \\ fs [eval_exp_cases]
  \\ rw [] \\ fs [delta_0_deterministic]
  \\ res_tac \\ fs []);

(**
Helping lemma. Needed in soundness proof.
For each evaluation of using an arbitrary epsilon, we can replace it by
evaluating the subExpressions and then binding the result values to different
variables in the Eironment.
**)
val binary_unfolding = store_thm("binary_unfolding",
  ``!(b:binop) (e1:(real)exp) (e2:(real)exp) (eps:real) E (v:real).
      (eval_exp eps E (Binop b e1 e2) v <=>
      (?(v1:real) (v2:real).
         eval_exp eps E e1 v1 /\
         eval_exp eps E e2 v2 /\
         eval_exp eps (updEnv 2 v2 (updEnv 1 v1 emptyEnv)) (Binop b (Var 1) (Var 2)) v))``,
  fs [updEnv_def, eval_exp_cases,APPLY_UPDATE_THM,PULL_EXISTS]
  \\ metis_tac []);

(**
  Analogous lemma for unary expressions
**)
val unary_unfolding = store_thm("unary_unfolding",
  ``!(u:unop) (e1:(real)exp) (eps:real) E (v:real).
       (eval_exp eps E (Unop Inv e1) v <=>
       (?(v1:real).
          eval_exp eps E e1 v1 /\
          eval_exp eps (updEnv 1 v1 emptyEnv) (Unop Inv (Var 1)) v))``,
  fs [updEnv_def, eval_exp_cases,APPLY_UPDATE_THM,PULL_EXISTS]
  \\ metis_tac []);

(*
  Using the parametric Expressions, define boolean Expressions for conditionals
*)
val _ = Datatype `
  bexp = Leq ('v exp) ('v exp)
       | Less ('v exp) ('v exp)`

(*
  Define evaluation of boolean expressions
*)
val (bval_exp_rules, bval_exp_ind, bval_exp_cases) = Hol_reln `
  (!eps E e1 e2 v1 v2.
      eval_exp eps E e1 v1 /\
      eval_exp eps E e2 v2 ==>
      bval eps E (Leq e1 e2) (v1 <= v2))/\
  (!eps E e1 e2 v1 v2.
      eval_exp eps E e1 v1 /\
      eval_exp eps E e2 v2 ==>
      bval eps E (Less e1 e2) (v1 < v2))`;

val bval_exp_cases =
  map (GEN_ALL o SIMP_CONV (srw_ss()) [Once bval_exp_cases])
    [``bval eps E (Leq e1 e2) res``,
     ``bval eps E (Less e1 e2) res``]
  |> LIST_CONJ |> curry save_thm "bval_exp_cases";

(**
  Simplify arithmetic later by making > >= only abbreviations
**)
val _ = overload_on("Gr",``\(e1:('v)exp). \(e2:('v)exp). Less e2 e1``);
val _ = overload_on("Greq",``\(e1:('v)exp). \(e2:('v)exp). Leq e2 e1``);

val _ = export_theory();
