 (**
   This file contains the HOL4 implementation of the error bound validator as well
   as its soundness proof. The function validErrorbound is the Error bound
   validator from the certificate checking process. Under the assumption that a
   valid range arithmetic result has been computed, it can validate error bounds
   encoded in the analysis result. The validator is used in the file
   CertificateChecker.v to build the complete checker.
 **)
open preamble
open simpLib realTheory realLib RealArith pred_setTheory
open AbbrevsTheory ExpressionsTheory RealSimpsTheory DaisyTactics
open ExpressionAbbrevsTheory ErrorBoundsTheory IntervalArithTheory
open IntervalValidationTheory EnvironmentsTheory CommandsTheory ssaPrgsTheory

val _ = new_theory "ErrorValidation";

val _ = Parse.hide "delta"; (* so that it can be used as a variable *)

val validErrorbound_def = Define `
  validErrorbound e (absenv:analysisResult) (dVars:num_set)=
    let (intv, err) = absenv e in
      if (0 <= err)
        then
          case e of
            | Var v => if (lookup v dVars = SOME ()) then T else (maxAbs intv * machineEpsilon <= err)
            | Const n => (maxAbs intv * machineEpsilon <= err)
            | Unop Neg f =>
              if (validErrorbound f absenv dVars)
              then
                err = (SND (absenv f))
              else
                  F
            | Unop Inv f => F
            | Binop op f1 f2 =>
              (if (validErrorbound f1 absenv dVars /\ validErrorbound f2 absenv dVars)
                then
                  let (ive1, err1) = absenv f1 in
                  let (ive2, err2) = absenv f2 in
                  let errIve1 = widenInterval ive1 err1 in
                  let errIve2 = widenInterval ive2 err2 in
                  let upperBoundE1 = maxAbs ive1 in
                  let upperBoundE2 = maxAbs ive2 in
                    case op of
                      | Plus => err1 + err2 + (maxAbs (addInterval errIve1 errIve2) * machineEpsilon) <= err
                      | Sub => err1 + err2 + (maxAbs (subtractInterval errIve1 errIve2) * machineEpsilon) <= err
                      | Mult => (upperBoundE1 * err2 + upperBoundE2 * err1 + err1 * err2) + (maxAbs (multInterval errIve1 errIve2) * machineEpsilon) <= err
                      | Div =>
                        (if (IVhi errIve2 < 0 \/ 0 < IVlo errIve2)
                          then
                            let upperBoundInvE2 = maxAbs (invertInterval ive2) in
                            let minAbsIve2 = minAbsFun (errIve2) in
                            let errInv = (1 / (minAbsIve2 * minAbsIve2)) * err2 in
                              ((upperBoundE1 * errInv + upperBoundInvE2 * err1 + err1 * errInv) + (maxAbs (divideInterval errIve1 errIve2) * machineEpsilon) <= err)
                          else F)
                  else F)
        else F`;

val validErrorboundCmd_def = Define `
  validErrorboundCmd (f:real cmd) (env:analysisResult) (dVars:num_set)=
    case f of
      | Let x e g  =>
        if validErrorboundCmd g env (insert x () dVars)
          then
            (validErrorbound e env dVars/\
            (env e = env (Var x)))
          else F
    | Ret e =>
      validErrorbound e env dVars`;

val err_always_positive = store_thm ("err_always_positive",
  ``!(e:real exp) (absenv:analysisResult) (iv:interval) (err:real) dVars.
      (validErrorbound (e:real exp) (absenv:analysisResult) dVars) /\
      ((iv,err) = absenv e) ==>
      0 <= err``,
  once_rewrite_tac [validErrorbound_def] \\ rpt strip_tac \\
  Cases_on `e` \\
  qpat_assum `(iv,err) = absenv e` (fn thm => fs [GSYM thm]) \\
  Cases_on `absenv e0` \\ Cases_on `absenv e'` \\ fs[]);

val validErrorboundCorrectVariable_ind = prove (
  ``!(E1 E2:env) absenv fVars dVars.
      approxEnv E1 absenv fVars dVars E2 ==>
      !(v:num) (nR nF err nlo nhi:real) (P:precond).
      eval_exp 0 E1 (Var v) nR /\
      eval_exp machineEpsilon E2 (Var v) nF /\
      validIntervalbounds (Var v) absenv P dVars /\
      validErrorbound (Var v) absenv dVars /\
      (!v.
         v IN domain dVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (FST (absenv (Var v))) <= r /\
           r <= SND (FST (absenv (Var v)))) /\
      (!v.
         v IN domain fVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (P v) <= r /\ r <= SND (P v)) /\
      (absenv (Var v) = ((nlo, nhi),err)) ==>
      abs (nR - nF) <= err``,
  (* rule induction on approximation relation *)
  qspec_then
    `\E1 absenv fVars dVars E2.
      !(v:num) (nR nF err nlo nhi:real) (P:precond).
      eval_exp 0 E1 (Var v) nR /\
      eval_exp machineEpsilon E2 (Var v) nF /\
      validIntervalbounds (Var v) absenv P dVars /\
      validErrorbound (Var v) absenv dVars /\
      (!v.
         v IN domain dVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (FST (absenv (Var v))) <= r /\
           r <= SND (FST (absenv (Var v)))) /\
      (!v.
         v IN domain fVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (P v) <= r /\ r <= SND (P v)) /\
      (absenv (Var v) = ((nlo, nhi),err)) ==>
      abs (nR - nF) <= err` (fn thm => ASSUME_TAC (SIMP_RULE std_ss [] thm)) approxEnv_ind
  \\ first_x_assum match_mp_tac
  \\ rpt strip_tac
  \\ inversion `eval_exp 0 _ _ _` eval_exp_cases
  \\ inversion `eval_exp machineEpsilon _ _ _` eval_exp_cases
  >- (fs [emptyEnv_def])
  >- (fs[updEnv_def]
      \\ Cases_on `v = x` \\ rveq
      >- (fs [validErrorbound_def, lookup_union] \\ rveq \\ Cases_on `lookup v fVars` \\ fs[]
          \\ first_assum (qspec_then `v` ASSUME_TAC)
          \\ match_mp_tac REAL_LE_TRANS
          \\ asm_exists_tac
          \\ conj_tac \\ fs []
          \\ match_mp_tac REAL_LE_TRANS
          \\ qexists_tac `maxAbs (nlo, nhi) * machineEpsilon`
          \\ conj_tac \\ fs []
          \\ match_mp_tac REAL_LE_RMUL_IMP
          \\ fs [mEps_geq_zero]
          \\ match_mp_tac contained_leq_maxAbs
          \\ rewrite_tac [contained_def, IVlo_def, IVhi_def]
          \\ qspecl_then [`Var v`, `A`] ASSUME_TAC validIntervalbounds_sound
          \\ rw_asm_star `A (Var v) = _`
          \\ rw_asm_star `A (Var v) = _`
          \\ first_x_assum match_mp_tac
          \\ qexistsl_tac [`P`, `insert v () fVars`, `dVars`, `updEnv v nR E1`]
          \\ fs [updEnv_def, domain_insert]
          \\ rpt conj_tac
          >- (fs [usedVars_def, SUBSET_DEF, domain_insert])
          >- (rpt strip_tac \\ first_x_assum match_mp_tac \\ fs [])
          >- (match_mp_tac Var_load \\ fs [updEnv_def]))
      >- (first_x_assum match_mp_tac
          \\ qexistsl_tac [`v`, `nlo`, `nhi`, `P`]
          \\ fs []
          \\ rpt conj_tac
          >- (match_mp_tac Var_load \\ fs [])
          >- (match_mp_tac Var_load \\ fs [])
          >- (rpt strip_tac
              \\ rpt(first_x_assum (qspec_then `v'` ASSUME_TAC))
              \\ specialize `v' IN domain dVars ==> _` `v' IN domain dVars`
              \\ fs []
              \\ Cases_on `v' = x`
              >- (rveq \\ fs [lookup_union]
                  \\ Cases_on `lookup v' fVars`
                  \\ fs [domain_lookup])
              >- (rveq \\ fs []))
          >- (rpt strip_tac
             \\ first_x_assum (qspec_then `v'` ASSUME_TAC)
             \\ Cases_on `v' = x`
             >- (rveq \\ fs[lookup_union]
                 \\ Cases_on `lookup v' fVars`
                 \\ fs [domain_lookup])
             >- (fs []
                 \\ first_x_assum match_mp_tac
                 \\ fs [domain_insert]))))
  >- (fs [updEnv_def]
      \\ Cases_on `v = x` \\ rveq
      >- (fs [validErrorbound_def, lookup_union]
          \\ rveq \\ Cases_on `lookup v fVars` \\ fs[])
      >- (first_x_assum match_mp_tac
          \\ qexistsl_tac [`v`, `nlo`, `nhi`, `P`]
          \\ rpt conj_tac
          >- (match_mp_tac Var_load \\ fs [])
          >- (match_mp_tac Var_load \\ fs [])
          >- (fs [validIntervalbounds_def, lookup_insert])
          >- (fs [validErrorbound_def, lookup_insert])
          >- (rpt strip_tac
              \\ specialize `!v'. (v' = _) \/ v' IN domain _ ==> _` `v'`
              \\ Cases_on `v' = x`
              >- (rveq \\ fs [lookup_union]
                  \\ Cases_on `lookup v' fVars`
                  \\ fs [domain_lookup])
              >- (fs []
                  \\ first_x_assum match_mp_tac
                  \\ fs [domain_insert]))
          >- (rpt strip_tac
              \\ rpt (first_x_assum (qspec_then `v'` ASSUME_TAC))
              \\ specialize `v' IN domain fVars ==> _` `v' IN domain fVars`
              \\ fs []
              \\ Cases_on `v' = x`
              >- (rveq \\ fs [lookup_union]
                  \\ Cases_on `lookup v' fVars`
                  \\ fs [domain_lookup])
              >- (rveq \\ fs []))
          >- (fs []))));

val validErrorboundCorrectVariable = store_thm ("validErrorboundCorrectVariable",
  ``!(E1 E2:env) absenv fVars dVars  (v:num) (nR nF err nlo nhi:real) (P:precond).
      approxEnv E1 absenv fVars dVars E2 /\
      eval_exp 0 E1 (Var v) nR /\
      eval_exp machineEpsilon E2 (Var v) nF /\
      validIntervalbounds (Var v) absenv P dVars /\
      validErrorbound (Var v) absenv dVars /\
      (!v.
         v IN domain dVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (FST (absenv (Var v))) <= r /\
           r <= SND (FST (absenv (Var v)))) /\
      (!v.
         v IN domain fVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (P v) <= r /\ r <= SND (P v)) /\
      (absenv (Var v) = ((nlo, nhi),err)) ==>
      abs (nR - nF) <= err``,
  rpt strip_tac
  \\ drule validErrorboundCorrectVariable_ind
  \\ disch_then (fn thm => qspecl_then [`v`, `nR`, `nF`, `err`, `nlo`, `nhi`, `P`] ASSUME_TAC thm)
  \\ fs []);

val validErrorboundCorrectConstant = store_thm ("validErrorboundCorrectConstant",
  ``!(E1 E2:env) (absenv:analysisResult) (n nR nF e nlo nhi:real) dVars.
      eval_exp 0 E1 (Const n) nR /\
      eval_exp machineEpsilon E2 (Const n) nF /\
      validErrorbound (Const n) absenv dVars /\
      FST (FST (absenv (Const n))) <= nR /\
      nR <= SND (FST (absenv (Const  n))) /\
      (absenv (Const n) = ((nlo,nhi),e)) ==>
      (abs (nR - nF)) <= e``,
  once_rewrite_tac [validErrorbound_def]
  \\ rpt strip_tac \\ fs[]
  \\ inversion `eval_exp 0 _ (Const n) nR` eval_exp_cases \\ rveq
  \\ simp [delta_0_deterministic]
  \\ inversion `eval_exp machineEpsilon _ _ _` eval_exp_cases \\ rveq
  \\ simp[perturb_def]
  \\ rename1 `abs deltaF <= machineEpsilon`
  \\ simp [Rabs_err_simpl, ABS_MUL]
  \\ match_mp_tac REAL_LE_TRANS
  \\ qexists_tac `maxAbs (nlo, nhi) * machineEpsilon` \\ conj_tac \\ TRY (simp[])
  \\ match_mp_tac REAL_LE_MUL2 \\ rpt (conj_tac) \\ TRY (simp[ABS_POS])
  \\ simp[maxAbs_def]
  \\ match_mp_tac maxAbs
  \\ qspecl_then [`delta`, `n`] (fn thm => fs [thm]) delta_0_deterministic
  \\ qpat_x_assum `absenv _ = _` (fn thm => rule_assum_tac (fn thm2 => REWRITE_RULE [thm] thm2))
  \\ simp[]);

val validErrorboundCorrectAddition = store_thm ("validErrorboundCorrectAddition",
  ``!(E1 E2:env) (absenv:analysisResult) (e1:real exp) (e2:real exp)
     (nR nR1 nR2 nF nF1 nF2:real) (e err1 err2:real) (alo ahi e1lo e1hi e2lo e2hi :real) dVars.
       eval_exp 0 E1 e1 nR1 /\
       eval_exp 0 E1 e2 nR2 /\
       eval_exp 0 E1 (Binop Plus e1 e2) nR /\
       eval_exp machineEpsilon E2 e1 nF1 /\
       eval_exp machineEpsilon E2 e2 nF2 /\
       eval_exp machineEpsilon
         (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv)) (Binop Plus (Var 1) (Var 2)) nF /\
       validErrorbound (Binop Plus e1 e2) absenv dVars /\
       FST (FST (absenv e1)) <= nR1 /\
       nR1 <= SND (FST (absenv e1)) /\
       FST (FST (absenv e2)) <= nR2 /\
       nR2 <= SND (FST (absenv e2)) /\
       (absenv e1 = ((e1lo,e1hi),err1)) /\
       (absenv e2 = ((e2lo, e2hi),err2)) /\
       (absenv (Binop Plus e1 e2) = ((alo,ahi),e)) /\
       abs (nR1 - nF1) <= err1 /\
       abs (nR2 - nF2) <= err2 ==>
       abs (nR - nF) <= e``,
  once_rewrite_tac [validErrorbound_def]
  \\ rpt strip_tac \\ fs[]
  \\ rw_asm `absenv _ = _`
  \\ rw_asm `absenv e1 = _`
  \\ match_mp_tac REAL_LE_TRANS
  \\ qexists_tac `err1 + err2 + (abs (nF1 + nF2) * machineEpsilon)`
  \\ conj_tac
     >- (match_mp_tac add_abs_err_bounded
         \\ qexistsl_tac [`e1`, `nR1`, `e2`, `nR2`, `E1`, `E2`]
         \\ rpt (conj_tac) \\ simp[])
     >- (match_mp_tac REAL_LE_TRANS
         \\ qexists_tac
              `err1 + err2 + maxAbs (
                 addInterval (widenInterval (e1lo,e1hi) err1)
                             (widenInterval (e2lo,e2hi) err2)) * machineEpsilon`
         \\ conj_tac \\ simp[maxAbs_def]
         \\ once_rewrite_tac [REAL_MUL_COMM] \\ match_mp_tac REAL_LE_LMUL_IMP
         \\ conj_tac \\ simp[mEps_geq_zero]
         \\ match_mp_tac maxAbs
         \\ `contained nF1 (widenInterval (e1lo,e1hi) err1)`
              by (match_mp_tac distance_gives_iv
                  \\ qexists_tac `nR1` \\ conj_tac
                  \\ simp[contained_def, IVlo_def, IVhi_def])
         \\ `contained nF2 (widenInterval (e2lo,e2hi) err2)`
              by (match_mp_tac distance_gives_iv
                  \\ qexists_tac `nR2` \\ conj_tac
                  \\ simp[contained_def, IVlo_def, IVhi_def])
         \\ `contained (nF1 + nF2) (addInterval (widenInterval (e1lo, e1hi) err1) (widenInterval (e2lo, e2hi) err2))`
              by (match_mp_tac (ONCE_REWRITE_RULE [validIntervalAdd_def] interval_addition_valid)
                  \\ conj_tac \\ simp[])
         \\ rule_assum_tac (fn thm => REWRITE_RULE [contained_def, IVlo_def, IVhi_def] thm)
         \\ simp[]));

val validErrorboundCorrectSubtraction = store_thm ("validErrorboundCorrectSubtraction",
  ``!(E1 E2:env) (absenv:analysisResult) (e1:real exp) (e2:real exp)
     (nR nR1 nR2 nF nF1 nF2:real) (e err1 err2:real) (alo ahi e1lo e1hi e2lo e2hi:real) dVars.
       eval_exp 0 E1 e1 nR1 /\
       eval_exp 0 E1 e2 nR2 /\
       eval_exp 0 E1 (Binop Sub e1 e2) nR /\
       eval_exp machineEpsilon E2 e1 nF1 /\
       eval_exp machineEpsilon E2 e2 nF2 /\
       eval_exp machineEpsilon
         (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv)) (Binop Sub (Var 1) (Var 2)) nF /\
       validErrorbound (Binop Sub e1 e2) absenv dVars /\
       FST (FST (absenv e1)) <= nR1 /\
       nR1 <= SND (FST (absenv e1)) /\
       FST (FST (absenv e2)) <= nR2 /\
       nR2 <= SND (FST (absenv e2)) /\
       (absenv e1 = ((e1lo,e1hi),err1)) /\
       (absenv e2 = ((e2lo, e2hi),err2)) /\
       (absenv (Binop Sub e1 e2) = ((alo,ahi),e)) /\
       abs (nR1 - nF1) <= err1 /\
       abs (nR2 - nF2) <= err2 ==>
       abs (nR - nF) <= e``,
  once_rewrite_tac [validErrorbound_def]
  \\ rpt strip_tac \\ fs[]
  \\ match_mp_tac REAL_LE_TRANS
  \\ qexists_tac `err1 + err2 + (abs (nF1 - nF2) * machineEpsilon)`
  \\ conj_tac
  >- (match_mp_tac subtract_abs_err_bounded
      \\ qexistsl_tac [`e1`, `nR1`, `e2`, `nR2`, `E1`, `E2`]
      \\ rpt (conj_tac) \\ simp[])
  >- (match_mp_tac REAL_LE_TRANS
      \\ qexists_tac
           `err1 + err2 + maxAbs (
                            subtractInterval (widenInterval (e1lo,e1hi) err1)
                                             (widenInterval (e2lo,e2hi) err2)) * machineEpsilon`
      \\ conj_tac \\ simp[maxAbs_def]
      \\ once_rewrite_tac [REAL_MUL_COMM] \\ match_mp_tac REAL_LE_LMUL_IMP
      \\ conj_tac \\ simp[mEps_geq_zero]
      \\ match_mp_tac maxAbs
      \\ `contained nF1 (widenInterval (e1lo,e1hi) err1)`
            by (match_mp_tac distance_gives_iv
                \\ qexists_tac `nR1` \\ conj_tac
                \\ simp[contained_def, IVlo_def, IVhi_def])
      \\ `contained nF2 (widenInterval (e2lo,e2hi) err2)`
           by (match_mp_tac distance_gives_iv
               \\ qexists_tac `nR2` \\ conj_tac
               \\ simp[contained_def, IVlo_def, IVhi_def])
      \\ `contained (nF1 - nF2) (subtractInterval (widenInterval (e1lo, e1hi) err1) (widenInterval (e2lo, e2hi) err2))`
           by (match_mp_tac (ONCE_REWRITE_RULE [validIntervalSub_def] interval_subtraction_valid)
               \\ conj_tac \\ simp[])
      \\ rule_assum_tac (fn thm => REWRITE_RULE [contained_def, IVlo_def, IVhi_def] thm)
      \\ simp[]));

val validErrorboundCorrectMult = store_thm ("validErrorboundCorrectMult",
  ``!(E1 E2:env) (absenv:analysisResult) (e1:real exp) (e2:real exp)
     (nR nR1 nR2 nF nF1 nF2:real) (e err1 err2:real) (alo ahi e1lo e1hi e2lo e2hi :real) dVars.
       eval_exp 0 E1 e1 nR1 /\
       eval_exp 0 E1 e2 nR2 /\
       eval_exp 0 E1 (Binop Mult e1 e2) nR /\
       eval_exp machineEpsilon E2 e1 nF1 /\
       eval_exp machineEpsilon E2 e2 nF2 /\
       eval_exp machineEpsilon
         (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv)) (Binop Mult (Var 1) (Var 2)) nF /\
       validErrorbound (Binop Mult e1 e2) absenv dVars /\
       FST (FST (absenv e1)) <= nR1 /\
       nR1 <= SND (FST (absenv e1)) /\
       FST (FST (absenv e2)) <= nR2 /\
       nR2 <= SND (FST (absenv e2)) /\
       (absenv e1 = ((e1lo,e1hi),err1)) /\
       (absenv e2 = ((e2lo, e2hi),err2)) /\
       (absenv (Binop Mult e1 e2) = ((alo,ahi),e)) /\
       abs (nR1 - nF1) <= err1 /\
       abs (nR2 - nF2) <= err2 ==>
       abs (nR - nF) <= e``,
  once_rewrite_tac [validErrorbound_def]
  \\ rpt strip_tac \\ fs[]
  \\ qpat_x_assum `absenv (Binop _ _ _) = _` (fn thm => fs [thm] \\ ASSUME_TAC thm)
  \\ qpat_x_assum `absenv e1 = _` (fn thm => fs [thm] \\ ASSUME_TAC thm)
  \\ qpat_x_assum `absenv e2 = _` (fn thm => fs [thm] \\ ASSUME_TAC thm)
  \\ `0 <= err1`
       by (match_mp_tac err_always_positive
           \\ qexistsl_tac [`e1`, `absenv`, `(e1lo,e1hi)`, `dVars`] \\ fs[])
  \\ `0 <= err2`
       by (match_mp_tac err_always_positive
           \\ qexistsl_tac [`e2`, `absenv`, `(e2lo,e2hi)`, `dVars`] \\ fs[])
  \\ match_mp_tac REAL_LE_TRANS
  \\ qexists_tac `abs (nR1 * nR2 - nF1 * nF2) + abs (nF1 * nF2) * machineEpsilon`
  \\ conj_tac
  >- (match_mp_tac mult_abs_err_bounded
      \\ qexistsl_tac [`e1`, `e2`, `err1`, `err2`, `E1`, `E2`]
      \\ fs [])
  >- (match_mp_tac REAL_LE_TRANS
      \\ qexists_tac `maxAbs (e1lo,e1hi) * err2 + maxAbs (e2lo,e2hi) * err1 +
                   err1 * err2 +
                   maxAbs (multInterval (widenInterval (e1lo,e1hi) err1)
                     (widenInterval (e2lo,e2hi) err2)) * machineEpsilon`
      \\ conj_tac \\ TRY(simp[])
      \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac
      >- (`nR1 <= maxAbs (e1lo, e1hi)`
            by (match_mp_tac contained_leq_maxAbs_val
                \\ fs[contained_def, IVlo_def, IVhi_def])
          \\ `nR2 <= maxAbs (e2lo, e2hi)`
               by (match_mp_tac contained_leq_maxAbs_val
                   \\ fs[contained_def, IVlo_def, IVhi_def])
          \\`-nR1 <= maxAbs (e1lo, e1hi)`
               by (match_mp_tac contained_leq_maxAbs_neg_val
                   \\ fs[contained_def, IVlo_def, IVhi_def])
          \\ `-nR2 <= maxAbs (e2lo, e2hi)`
               by (match_mp_tac contained_leq_maxAbs_neg_val
                   \\ fs[contained_def, IVlo_def, IVhi_def])
          \\ `nR1 * err2 <= maxAbs (e1lo, e1hi) * err2`
               by (match_mp_tac REAL_LE_RMUL_IMP \\ fs[])
          \\ `-nR1 * err2 <= maxAbs (e1lo, e1hi) * err2`
               by (match_mp_tac REAL_LE_RMUL_IMP \\ fs[])
          \\ `nR2 * err1 <= maxAbs (e2lo, e2hi) * err1`
               by (match_mp_tac REAL_LE_RMUL_IMP \\ fs[])
          \\ `-nR2 * err1 <= maxAbs (e2lo, e2hi) * err1`
               by (match_mp_tac REAL_LE_RMUL_IMP \\ fs[])
          \\ `- (err1 * err2) <= err1 * err2`
               by (fs[REAL_NEG_LMUL] \\ match_mp_tac REAL_LE_RMUL_IMP \\ REAL_ASM_ARITH_TAC)
          \\ `0 <= maxAbs (e1lo, e1hi) * err2` by REAL_ASM_ARITH_TAC
          \\ `maxAbs (e1lo, e1hi) * err2 <= maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
               by REAL_ASM_ARITH_TAC
          \\ `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1 <=
                maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1 + err1 * err2`
                  by REAL_ASM_ARITH_TAC
          \\ rpt (qpat_x_assum `eval_exp _ _ _ _ _` kall_tac)
          \\ rpt (qpat_x_assum `validErrorbound _ _` kall_tac)
          \\ `! (x:real). ((abs x = x) /\ 0 < x) \/ ((abs x = - x) /\ x <= 0)` by REAL_ASM_ARITH_TAC
          (* Large case distinction for
             a) different cases of the value of Rabs (...) and
             b) wether arguments of multiplication in (nf1 * nF2) are < or >= 0  *)
         \\ qpat_assum `!x. (A /\ B) \/ C` (fn thm => qspecl_then [`nR1 - nF1` ] DISJ_CASES_TAC thm)
         \\ qpat_assum `!x. (A /\ B) \/ C` (fn thm => qspecl_then [`nR2 - nF2` ] DISJ_CASES_TAC thm)
         \\ fs[]
         \\ rpt (qpat_x_assum `abs _ = _` (fn thm => RULE_ASSUM_TAC (fn thm2 => ONCE_REWRITE_RULE [thm] thm2)))
          (* All positive *)
          >- (`nF1 <= nR1 + err1` by (match_mp_tac err_up \\ simp[])
              \\ `nF2 <= nR2 + err2` by (match_mp_tac err_up \\ simp[])
              \\ qpat_assum `!x. (A /\ B) \/ C`
                   (fn thm => qspecl_then [`nR1 * nR2 - nF1 * nF2` ] DISJ_CASES_TAC thm)
	      \\ fs[real_sub]
              (* Absolute value positive *)
              >-(qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nF1 * (- (nR2 + err2))` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                        >- (match_mp_tac REAL_LT_IMP_LE \\ simp[])
                        >- (simp[REAL_LE_NEG]))
                    >- (qspecl_then [`- (nR2 + err2)`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 - err1) * - (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                            >- (`nR1 * nR2 + (nR1 - err1) * - (nR2 + err2) = - nR1 * err2 + nR2 * err1 + err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\simp[]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 + err1) * - (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`nR1 * nR2 + (nR1 + err1) * - (nR2 + err2) = - nR1 * err2 + - nR2 * err1 + - err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ TRY(simp[GSYM REAL_NEG_LMUL]) \\
                                match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[REAL_NEG_LMUL]))))
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nF1 * - (nR2 - err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ simp[REAL_LE_NEG] \\
                        match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub])
                    >- (qspecl_then [`- (nR2 - err2)`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 - err1) * - (nR2 - err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                            >- (`nR1 * nR2 + (nR1 - err1) * - (nR2 - err2) = nR1 * err2 + nR2 * err1 + - err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[GSYM REAL_NEG_LMUL] \\
                                match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[REAL_NEG_LMUL]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 + err1) * - (nR2 - err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`nR1 * nR2 + (nR1 + err1) * - (nR2 - err2) = nR1 * err2 + - nR2 * err1 + err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[GSYM REAL_NEG_LMUL] )))))
              (* Absolute value negative *)
              >- (simp[REAL_NEG_ADD] \\
				  qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nF1 * (nR2 - err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                        >- (match_mp_tac REAL_LT_IMP_LE \\ simp[])
                        >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                    >- (qspecl_then [`nR2 - err2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 - err1) * (nR2 - err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                            >- (`-(nR1 * nR2) + (nR1 - err1) * (nR2 - err2) = - nR1 * err2 + - nR2 * err1 + err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\simp[]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 + err1) * (nR2 - err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`-(nR1 * nR2) + (nR1 + err1) * (nR2 - err2) = - nR1 * err2 + nR2 * err1 + - err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ TRY(simp[GSYM REAL_NEG_LMUL]) \\
                                match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[REAL_NEG_LMUL]))))
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nF1 * (nR2 + err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ simp[REAL_LE_NEG])
                    >- (qspecl_then [`nR2 + err2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 - err1) * (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                            >- (`-(nR1 * nR2) + (nR1 - err1) * (nR2 + err2) = nR1 * err2 + - nR2 * err1 + - err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[GSYM REAL_NEG_LMUL] \\
                                match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[REAL_NEG_LMUL]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 + err1) * (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`-(nR1 * nR2) + (nR1 + err1) * (nR2 + err2) = nR1 * err2 + nR2 * err1 + err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[GSYM REAL_NEG_LMUL]))))))
      	  (* First positive, second negative *)
          >- (`nF1 <= nR1 + err1` by (match_mp_tac err_up \\ simp[]) \\
			  `nF2 <= nR2 + err2`
				by (once_rewrite_tac[REAL_ADD_COMM] \\ simp [GSYM REAL_LE_SUB_RADD] \\
				    once_rewrite_tac [REAL_ADD_COMM, GSYM REAL_NEG_SUB] \\ simp[] ) \\
              qpat_assum `!x. (A /\ B) \/ C` (fn thm => qspecl_then [`nR1 * nR2 - nF1 * nF2` ] DISJ_CASES_TAC thm) \\
			  fs[real_sub]
			  (* Absolute value positive *)
              >-(qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nF1 * (- (nR2 + err2))` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                        >- (match_mp_tac REAL_LT_IMP_LE \\ simp[])
                        >- (simp[REAL_LE_NEG]))
                    >- (qspecl_then [`- (nR2 + err2)`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 - err1) * - (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                            >- (`nR1 * nR2 + (nR1 - err1) * - (nR2 + err2) = - nR1 * err2 + nR2 * err1 + err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\simp[]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 + err1) * - (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`nR1 * nR2 + (nR1 + err1) * - (nR2 + err2) = - nR1 * err2 + - nR2 * err1 + - err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ TRY(simp[GSYM REAL_NEG_LMUL]) \\
                                match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[REAL_NEG_LMUL]))))
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nF1 * -nR2` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ simp[REAL_LE_NEG] \\
						qpat_x_assum `nR2 + - nF2 <= _ `
						  (fn thm => ASSUME_TAC (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))\\
						simp[])
                    >- (qspecl_then [`- nR2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 - err1) * - nR2` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                            >- (`nR1 * nR2 + (nR1 - err1) * - nR2 = nR2 * err1`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
                                      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
                                      \\ qexists_tac `maxAbs (e2lo,e2hi) * err1` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]
                                      \\ once_rewrite_tac [REAL_ADD_COMM]
                                      \\ simp [REAL_LE_ADDR]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 + err1) * - nR2` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`nR1 * nR2 + (nR1 + err1) * - nR2 = - nR2 * err1`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e2lo,e2hi) * err1` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]
				      \\ once_rewrite_tac [REAL_ADD_COMM]
				      \\ simp [REAL_LE_ADDR])))))
              (* Absolute value negative *)
              >- (simp[REAL_NEG_ADD] \\
				  qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nF1 * nR2` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                        >- (match_mp_tac REAL_LT_IMP_LE \\ simp[])
                        >- (qpat_x_assum `nR2 + - nF2 <= _ `
                              (fn thm =>
                                  ASSUME_TAC
                                    (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))
			    \\ simp[]))
                    >- (qspecl_then [`nR2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 - err1) * nR2` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                            >- (`-(nR1 * nR2) + (nR1 - err1) * nR2 = - nR2 * err1`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e2lo,e2hi) * err1` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]
				      \\ once_rewrite_tac [REAL_ADD_COMM]
				      \\ simp [REAL_LE_ADDR]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 + err1) * nR2` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`-(nR1 * nR2) + (nR1 + err1) * nR2 = nR2 * err1`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e2lo,e2hi) * err1` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]
				      \\ once_rewrite_tac [REAL_ADD_COMM]
				      \\ simp [REAL_LE_ADDR]))))
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nF1 * (nR2 + err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ simp[REAL_LE_NEG])
                    >- (qspecl_then [`nR2 + err2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 - err1) * (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                            >- (`-(nR1 * nR2) + (nR1 - err1) * (nR2 + err2) = nR1 * err2 + - nR2 * err1 + - err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[GSYM REAL_NEG_LMUL] \\
                                match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[REAL_NEG_LMUL]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 + err1) * (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`-(nR1 * nR2) + (nR1 + err1) * (nR2 + err2) = nR1 * err2 + nR2 * err1 + err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[GSYM REAL_NEG_LMUL]))))))
          (* First negative, second positive *)
          >- (`nF2 <= nR2 + err2` by (match_mp_tac err_up \\ simp[]) \\
			  `nF1 <= nR1 + err1`
				by (once_rewrite_tac[REAL_ADD_COMM] \\ simp [GSYM REAL_LE_SUB_RADD] \\
				    once_rewrite_tac [REAL_ADD_COMM, GSYM REAL_NEG_SUB] \\ simp[]) \\
              qpat_assum `!x. (A /\ B) \/ C` (fn thm => qspecl_then [`nR1 * nR2 - nF1 * nF2` ] DISJ_CASES_TAC thm) \\
			  fs[real_sub]
			  (* Absolute value positive *)
              >-(qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nF1 * - (nR2 + err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                        >- (match_mp_tac REAL_LT_IMP_LE \\ simp[])
                        >- (simp[REAL_LE_NEG]))
                    >- (qspecl_then [`- (nR2 + err2)`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nR1 * - (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (qpat_x_assum `nR1 + - nF1 <= _ `
									(fn thm => ASSUME_TAC (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))\\
								   simp[]))
                            >- (`nR1 * nR2 + nR1 * - (nR2 + err2) = - nR1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 + err1) * - (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`nR1 * nR2 + (nR1 + err1) * - (nR2 + err2) = - nR1 * err2 + - nR2 * err1 + - err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
                                      \\ simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ TRY(simp[GSYM REAL_NEG_LMUL])
                                      \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[REAL_NEG_LMUL]))))
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nF1 * - (nR2 - err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ simp[REAL_LE_NEG] \\
                        match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub])
                    >- (qspecl_then [`- (nR2 - err2)`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nR1 * - (nR2 - err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (qpat_x_assum `nR1 + - nF1 <= _ `
                                     (fn thm =>
                                         ASSUME_TAC
                                           (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))
				   \\ simp[]))
                            >- (`nR1 * nR2 + nR1 * - (nR2 - err2) = nR1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 + err1) * - (nR2 - err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`nR1 * nR2 + (nR1 + err1) * - (nR2 - err2) = nR1 * err2 + - nR2 * err1 + err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
                                \\ simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[GSYM REAL_NEG_LMUL] )))))
              (* Absolute value negative *)
              >- (simp[REAL_NEG_ADD] \\
				  qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nF1 * (nR2 - err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                        >- (match_mp_tac REAL_LT_IMP_LE \\ simp[])
                        >- (match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub]))
                    >- (qspecl_then [`nR2 - err2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nR1 * (nR2 - err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (qpat_x_assum `nR1 + - nF1 <= _ `
                                     (fn thm =>
                                          ASSUME_TAC
                                            (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))
				   \\ simp[]))
                            >- (`-(nR1 * nR2) + nR1 * (nR2 - err2) = - nR1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 + err1) * (nR2 - err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`-(nR1 * nR2) + (nR1 + err1) * (nR2 - err2) = - nR1 * err2 + nR2 * err1 + - err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
                                \\ simp[] \\ match_mp_tac REAL_LE_ADD2
                                \\ conj_tac \\ TRY(simp[GSYM REAL_NEG_LMUL])
                                \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[REAL_NEG_LMUL]))))
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nF1 * (nR2 + err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ simp[REAL_LE_NEG])
                    >- (qspecl_then [`nR2 + err2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nR1 * (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (qpat_x_assum `nR1 + - nF1 <= _ `
                                     (fn thm =>
                                         ASSUME_TAC
                                           (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))
				   \\ simp[]))
                            >- (`-(nR1 * nR2) + nR1 * (nR2 + err2) = nR1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 + err1) * (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`-(nR1 * nR2) + (nR1 + err1) * (nR2 + err2) = nR1 * err2 + nR2 * err1 + err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB] \\
                                      fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC] \\
                                      fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM]) \\
                                simp[] \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac \\ simp[GSYM REAL_NEG_LMUL]))))))
          (* Both negative *)
          >- (`nF1 <= nR1 + err1`
                by (once_rewrite_tac[REAL_ADD_COMM]
                    \\ simp [GSYM REAL_LE_SUB_RADD]
	            \\ once_rewrite_tac [REAL_ADD_COMM, GSYM REAL_NEG_SUB] \\ simp[])
              \\ `nF2 <= nR2 + err2`
                   by (once_rewrite_tac[REAL_ADD_COMM]
                       \\ simp [GSYM REAL_LE_SUB_RADD]
                       \\ once_rewrite_tac [REAL_ADD_COMM, GSYM REAL_NEG_SUB] \\ simp[])
              \\ `nR1 <= nF1`
                   by (qpat_x_assum `nR1 - nF1 <= _ `
                         (fn thm =>
                             ASSUME_TAC
                               (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))
                       \\ simp[])
              \\ `nR2 <= nF2`
                   by (qpat_x_assum `nR2 - nF2 <= _ `
                         (fn thm =>
                             ASSUME_TAC (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))
                       \\ simp[])
              \\ qpat_assum `!x. (A /\ B) \/ C`
                   (fn thm => qspecl_then [`nR1 * nR2 - nF1 * nF2` ] DISJ_CASES_TAC thm)
	      \\ fs[real_sub]
	      (* Absolute value positive *)
              >-(qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nF1 * - (nR2 + err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                        >- (match_mp_tac REAL_LT_IMP_LE \\ simp[])
                        >- (simp[REAL_LE_NEG]))
                    >- (qspecl_then [`- (nR2 + err2)`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nR1 * - (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (qpat_x_assum `nR1 + - nF1 <= _ `
                                     (fn thm =>
                                         ASSUME_TAC
                                           (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))
				   \\ simp[]))
                            >- (`nR1 * nR2 + nR1 * - (nR2 + err2) = - nR1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2`
                                      \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
                                      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]))
                        >- (match_mp_tac REAL_LE_TRANS
                            \\ qexists_tac `nR1 * nR2 + (nR1 + err1) * - (nR2 + err2)`
                            \\ conj_tac
                            >- (fs [REAL_NEG_RMUL]
                                \\ once_rewrite_tac [REAL_MUL_COMM]
                                \\ match_mp_tac REAL_LE_LMUL_IMP
                                \\ conj_tac \\ fs[])
                            >- (`nR1 * nR2 + (nR1 + err1) * - (nR2 + err2) = - nR1 * err2 + - nR2 * err1 + - err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
                                \\ simp[] \\ match_mp_tac REAL_LE_ADD2
                                \\ conj_tac \\ TRY(simp[GSYM REAL_NEG_LMUL])
                                \\ match_mp_tac REAL_LE_ADD2
                                \\ conj_tac \\ simp[REAL_NEG_LMUL]))))
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nF1 * - nR2` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ simp[REAL_LE_NEG] \\
                        match_mp_tac REAL_LE_ADD_FLIP \\ simp[real_sub])
                    >- (qspecl_then [`- nR2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + nR1 * - nR2` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (simp[]))
                            >- (`nR1 * nR2 + nR1 * - nR2 = 0`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2`
                                      \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `nR1 * nR2 + (nR1 + err1) * - nR2` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`nR1 * nR2 + (nR1 + err1) * - nR2 = - nR2 * err1`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e2lo, e2hi) * err1` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]
				      \\ once_rewrite_tac [REAL_ADD_COMM]
				      \\ simp[REAL_LE_ADDR])))))
              (* Absolute value negative *)
              >- (simp[REAL_NEG_ADD] \\
				  qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nF1 * nR2` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                        >- (match_mp_tac REAL_LT_IMP_LE \\ simp[])
                        >- (simp[]))
                    >- (qspecl_then [`nR2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nR1 * nR2` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (simp[]))
                            >- (`-(nR1 * nR2) + nR1 * nR2 = 0`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2` \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]))
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + (nR1 + err1) * nR2` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ fs[])
                            >- (`-(nR1 * nR2) + (nR1 + err1) * nR2 = nR2 * err1`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
				      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]
				      \\ match_mp_tac REAL_LE_TRANS
				      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]
				      \\ once_rewrite_tac [REAL_ADD_COMM]
				      \\ simp[REAL_LE_ADDR]))))
                 >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nF1 * (nR2 + err2)` \\ conj_tac
                    >- (fs [REAL_NEG_RMUL] \\ match_mp_tac REAL_LE_LMUL_IMP \\ conj_tac \\ simp[REAL_LE_NEG])
                    >- (qspecl_then [`nR2 + err2`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
                        >- (match_mp_tac REAL_LE_TRANS \\ qexists_tac `-(nR1 * nR2) + nR1 * (nR2 + err2)` \\ conj_tac
                            >- (fs [REAL_NEG_RMUL] \\ once_rewrite_tac [REAL_MUL_COMM] \\
                                match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\ conj_tac
                               >- (fs[] \\ match_mp_tac REAL_LT_IMP_LE \\ simp[])
                               >- (qpat_x_assum `nR1 + - nF1 <= _ `
                                     (fn thm =>
                                         ASSUME_TAC
                                           (SIMP_RULE bool_ss [GSYM real_sub, REAL_LE_SUB_RADD, REAL_ADD_LID] thm))
				   \\ simp[]))
                            >- (`-(nR1 * nR2) + nR1 * (nR2 + err2) = nR1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
                                      \\ simp[] \\ match_mp_tac REAL_LE_TRANS
                                      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2` \\ conj_tac \\ simp[]
                                      \\ match_mp_tac REAL_LE_TRANS
                                      \\ qexists_tac `maxAbs (e1lo, e1hi) * err2 + maxAbs (e2lo, e2hi) * err1`
                                      \\ conj_tac \\ simp[]))
                        >- (match_mp_tac REAL_LE_TRANS
                            \\ qexists_tac `-(nR1 * nR2) + (nR1 + err1) * (nR2 + err2)`
                            \\ conj_tac
                            >- (fs [REAL_NEG_RMUL]
                                \\ once_rewrite_tac [REAL_MUL_COMM]
                                \\ match_mp_tac REAL_LE_LMUL_IMP
                                \\ conj_tac \\ fs[])
                            >- (`-(nR1 * nR2) + (nR1 + err1) * (nR2 + err2) = nR1 * err2 + nR2 * err1 + err1 * err2`
                                  by (fs[real_sub,REAL_RDISTRIB]
                                      \\ fs [GSYM REAL_SUB_LNEG, real_sub, REAL_LDISTRIB, REAL_NEG_MUL2, REAL_ADD_ASSOC]
                                      \\ fs [GSYM real_sub, REAL_SUB_REFL, GSYM REAL_NEG_RMUL, REAL_MUL_COMM])
                                \\ simp[] \\ match_mp_tac REAL_LE_ADD2
                                \\ conj_tac \\ simp[GSYM REAL_NEG_LMUL])))))))
      >- (simp[maxAbs_def]
          \\ once_rewrite_tac [REAL_MUL_COMM] \\ match_mp_tac REAL_LE_LMUL_IMP
          \\ conj_tac \\ simp[mEps_geq_zero]
          \\ match_mp_tac maxAbs
          \\ `contained nF1 (widenInterval (e1lo,e1hi) err1)`
               by (match_mp_tac distance_gives_iv
                   \\ qexists_tac `nR1` \\ conj_tac \\ simp[contained_def, IVlo_def, IVhi_def])
          \\ `contained nF2 (widenInterval (e2lo,e2hi) err2)`
            by (match_mp_tac distance_gives_iv
                \\ qexists_tac `nR2` \\ conj_tac \\ simp[contained_def, IVlo_def, IVhi_def])
          \\ `contained (nF1 * nF2) (multInterval (widenInterval (e1lo, e1hi) err1) (widenInterval (e2lo, e2hi) err2))`
            by (match_mp_tac interval_multiplication_valid
                \\ conj_tac \\ simp[])
          \\ rule_assum_tac (fn thm => REWRITE_RULE [contained_def, IVlo_def, IVhi_def] thm)
          \\ simp[])));

val validErrorboundCorrectDiv = store_thm ("validErrorboundCorrectDiv",
  ``!(E1 E2:env) (absenv:analysisResult) (e1:real exp) (e2:real exp)
     (nR nR1 nR2 nF nF1 nF2:real) (e err1 err2:real) (alo ahi e1lo e1hi e2lo e2hi :real) dVars.
       eval_exp 0 E1 e1 nR1 /\
       eval_exp 0 E1 e2 nR2 /\
       eval_exp 0 E1 (Binop Div e1 e2) nR /\
       eval_exp machineEpsilon E2 e1 nF1 /\
       eval_exp machineEpsilon E2 e2 nF2 /\
       eval_exp machineEpsilon
         (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv)) (Binop Div (Var 1) (Var 2)) nF /\
       validErrorbound (Binop Div e1 e2) absenv dVars /\
       (e2hi < 0 \/ 0 < e2lo) /\
       FST (FST (absenv e1)) <= nR1 /\
       nR1 <= SND (FST (absenv e1)) /\
       FST (FST (absenv e2)) <= nR2 /\
       nR2 <= SND (FST (absenv e2)) /\
       (absenv e1 = ((e1lo,e1hi),err1)) /\
       (absenv e2 = ((e2lo, e2hi),err2)) /\
       (absenv (Binop Div e1 e2) = ((alo,ahi),e)) /\
       abs (nR1 - nF1) <= err1 /\
       abs (nR2 - nF2) <= err2 ==>
       abs (nR - nF) <= e``,
  rewrite_tac [Once validErrorbound_def, GSYM noDivzero_def]
  \\ rpt (strip_tac)
  \\ fs[]
  \\ `0 <= err1`
       by (match_mp_tac err_always_positive
           \\ qexistsl_tac [`e1`, `absenv`, `(e1lo,e1hi)`, `dVars`] \\ fs[])
  \\ `0 <= err2`
       by (match_mp_tac err_always_positive
           \\ qexistsl_tac [`e2`, `absenv`, `(e2lo,e2hi)`, `dVars`] \\ fs[])
  \\ match_mp_tac REAL_LE_TRANS
  \\ qexists_tac `abs (nR1 / nR2 - nF1 / nF2) + abs (nF1 / nF2) * machineEpsilon`
  \\ conj_tac
  >- (match_mp_tac div_abs_err_bounded
      \\ qexistsl_tac [`e1`, `e2`, `err1`, `err2`, `E1`, `E2`]
      \\ fs [])
  >- (match_mp_tac REAL_LE_TRANS
      \\ once_rewrite_tac [CONJ_SYM]
      \\ asm_exists_tac
      \\ once_rewrite_tac [CONJ_SYM]
      \\ conj_tac \\ rw[]
      \\ `contained nF1 (widenInterval (e1lo,e1hi) err1)`
           by (match_mp_tac distance_gives_iv
               \\ qexists_tac `nR1` \\ conj_tac
               \\ simp[contained_def, IVlo_def, IVhi_def])
      \\ `contained nF2 (widenInterval (e2lo,e2hi) err2)`
            by (match_mp_tac distance_gives_iv
                \\ qexists_tac `nR2` \\ conj_tac
                \\ simp[contained_def, IVlo_def, IVhi_def])
      \\ match_mp_tac REAL_LE_ADD2 \\ conj_tac
      >- (rpt (qpat_x_assum `eval_exp _ _ _ _ _ _` kall_tac)
          \\ `contained (inv nR2) (invertInterval (e2lo, e2hi))`
                by (match_mp_tac interval_inversion_valid \\ conj_tac
                    \\ fs[contained_def, IVlo_def, IVhi_def, noDivzero_def])
          \\ `contained (inv nF2) (invertInterval (widenInterval (e2lo, e2hi) err2))`
                by (match_mp_tac interval_inversion_valid \\ conj_tac
                    \\ fs[contained_def, IVlo_def, IVhi_def, noDivzero_def])
          \\ `nR1 <= maxAbs (e1lo, e1hi)`
                by (match_mp_tac contained_leq_maxAbs_val
                    \\ fs[contained_def, IVlo_def, IVhi_def])
          \\ `inv nR2 <= maxAbs (invertInterval(e2lo, e2hi))`
                by (match_mp_tac contained_leq_maxAbs_val
                    \\ fs[contained_def, IVlo_def, IVhi_def])
          \\ `-nR1 <= maxAbs (e1lo, e1hi)`
                by (match_mp_tac contained_leq_maxAbs_neg_val
                    \\ fs[contained_def, IVlo_def, IVhi_def])
          \\ `- inv nR2 <= maxAbs (invertInterval (e2lo, e2hi))`
                by (match_mp_tac contained_leq_maxAbs_neg_val
                    \\ fs[contained_def, IVlo_def, IVhi_def])
          \\ `nR1 * err2 <= maxAbs (e1lo, e1hi) * err2`
                by (match_mp_tac REAL_LE_RMUL_IMP \\ fs[])
          \\ `-nR1 * err2 <= maxAbs (e1lo, e1hi) * err2`
                by (match_mp_tac REAL_LE_RMUL_IMP \\ fs[])
          \\ `inv nR2 * err1 <= maxAbs (invertInterval(e2lo, e2hi)) * err1`
                by (match_mp_tac REAL_LE_RMUL_IMP \\ fs[])
          \\ `- inv nR2 * err1 <= maxAbs (invertInterval(e2lo, e2hi)) * err1`
                by (match_mp_tac REAL_LE_RMUL_IMP \\ fs[])
          \\ `- (err1 * err2) <= err1 * err2`
                by (fs[REAL_NEG_LMUL] \\ match_mp_tac REAL_LE_RMUL_IMP
                    \\ REAL_ASM_ARITH_TAC)
          \\ `0 <= maxAbs (e1lo, e1hi) * err2` by REAL_ASM_ARITH_TAC
          \\ `0 <= maxAbs (invertInterval (e2lo, e2hi)) * err1` by REAL_ASM_ARITH_TAC
          \\ `maxAbs (e1lo, e1hi) * err2 <= maxAbs (e1lo, e1hi) * err2 + maxAbs (invertInterval (e2lo, e2hi)) * err1`
                by (REAL_ASM_ARITH_TAC)
          \\ `maxAbs (e1lo, e1hi) * err2 + maxAbs (invertInterval (e2lo, e2hi)) * err1 <=
                 maxAbs (e1lo, e1hi) * err2 + maxAbs (invertInterval (e2lo, e2hi)) * err1 + err1 * err2`
                   by REAL_ASM_ARITH_TAC
               (* Case distinction for divisor range
				  positive or negative in float and real valued execution *)
          \\ rpt (qpat_x_assum `validErrorbound _ _ ` kall_tac)
          \\ rpt (qpat_x_assum `absenv _ = _` kall_tac)
          \\ rpt (qpat_x_assum `isSupersetInterval _ _` kall_tac)
          \\ rpt (qpat_x_assum `maxAbs (e1lo,e1hi) *
                                  (1 /
                                  (minAbsFun (widenInterval (e2lo,e2hi) err2) *
                                   minAbsFun (widenInterval (e2lo,e2hi) err2)) * err2) +
                                  maxAbs (invertInterval (e2lo,e2hi)) * err1 +
                                  err1 *
                                  (1 /
                                   (minAbsFun (widenInterval (e2lo,e2hi) err2) *
                                    minAbsFun (widenInterval (e2lo,e2hi) err2)) * err2) +
                                  maxAbs
                                      (divideInterval (widenInterval (e1lo,e1hi) err1)
                                                      (widenInterval (e2lo,e2hi) err2)) * machineEpsilon  <= e` kall_tac)
          \\ fs [IVlo_def, IVhi_def, widenInterval_def, contained_def, noDivzero_def]
     		   (* The range of the divisor lies in the range from -infinity until 0 *)
          >- (`abs (inv nR2 - inv nF2) <= err2 * inv ((e2hi + err2) * (e2hi + err2))`
                 by (match_mp_tac err_prop_inversion_neg \\ qexists_tac `e2lo` \\simp[])
              \\ fs [widenInterval_def, IVlo_def, IVhi_def]
              \\ `minAbsFun (e2lo - err2, e2hi + err2) = - (e2hi + err2)`
                    by (match_mp_tac minAbs_negative_iv_is_hi \\ REAL_ASM_ARITH_TAC)
              \\ simp[]
	      \\ qpat_x_assum `minAbsFun _ = _ ` kall_tac
              \\ `nF1 <= err1 + nR1` by REAL_ASM_ARITH_TAC
              \\ `nR1 - err1 <= nF1` by REAL_ASM_ARITH_TAC
              \\ `(nR2 - nF2 > 0 /\ nR2 - nF2 <= err2) \/ (nR2 - nF2 <= 0 /\ - (nR2 - nF2) <= err2)`
                    by REAL_ASM_ARITH_TAC
              (* Positive case for abs (nR2 - nF2) <= err2 *)
              >- (`nF2 < nR2` by REAL_ASM_ARITH_TAC
                  \\ qpat_x_assum `nF2 < nR2` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [GSYM REAL_LT_NEG] thm))
                  \\ `inv (- nF2) < inv (- nR2)` by (match_mp_tac REAL_LT_INV \\ REAL_ASM_ARITH_TAC)
                  \\ `inv (- nF2) = - (inv nF2)` by (match_mp_tac (GSYM REAL_NEG_INV) \\ REAL_ASM_ARITH_TAC)
                  \\ `inv (- nR2) = - (inv nR2)` by (match_mp_tac (GSYM REAL_NEG_INV) \\ REAL_ASM_ARITH_TAC)
		  \\ rpt (
                       qpat_x_assum `inv (- _) = - (inv _)`
                         (fn thm => rule_assum_tac (fn hyp => REWRITE_RULE [thm] hyp)))
                  \\ `inv nR2 < inv nF2` by REAL_ASM_ARITH_TAC
		  \\ qpat_x_assum `- _ < - _` kall_tac
                  \\ `inv nR2 - inv nF2 < 0` by REAL_ASM_ARITH_TAC
                  \\ `- (nR2⁻¹ − nF2⁻¹) ≤ err2 * ((e2hi + err2) * (e2hi + err2))⁻¹` by REAL_ASM_ARITH_TAC
                  \\ `inv nF2 <= inv nR2 + err2 * inv ((e2hi + err2) * (e2hi + err2))` by REAL_ASM_ARITH_TAC
                  \\ `inv nR2 - err2 * inv ((e2hi + err2) * (e2hi + err2)) <= inv nF2` by REAL_ASM_ARITH_TAC
                  (* Next do a case distinction for the absolute value *)
                  \\ `! (x:real). ((abs x = x) /\ 0 <= x) \/ ((abs x = - x) /\ x < 0)` by REAL_ASM_ARITH_TAC
		  \\ qpat_x_assum `!x. A /\ B \/ C`
                       (fn thm => qspec_then `(nR1:real / nR2:real) - (nF1:real / nF2:real)` DISJ_CASES_TAC thm)
		  (* Case 1: Absolute value positive *)
		  >- (fs[real_sub, real_div, REAL_NEG_LMUL]
                      \\ qspecl_then [`-nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
		      (* -nF1 < 0 *)
		      >- (match_mp_tac REAL_LE_TRANS
                          \\ qexists_tac `nR1 * inv nR2 + - nF1 * (inv nR2 - err2 * inv ((e2hi + err2) * (e2hi + err2)))`
                          \\ conj_tac
			  >- (fs[REAL_LE_LADD]
                              \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L
			      \\ conj_tac \\ REAL_ASM_ARITH_TAC)
			  >- (qabbrev_tac `err_inv = (err2 * ((e2hi + err2) * (e2hi + err2))⁻¹)`
                              \\ qspecl_then [`inv nR2 - err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
			      >- (match_mp_tac REAL_LE_TRANS
                                  \\ qexists_tac `nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 - err_inv)`
                                  \\ conj_tac
                                  >- (fs [REAL_LE_ADD]
                                      \\ once_rewrite_tac [REAL_MUL_COMM]
				      \\ match_mp_tac REAL_MUL_LE_COMPAT_NEG_L
				      \\ conj_tac \\ TRY REAL_ASM_ARITH_TAC
                                      \\ fs [REAL_LE_NEG])
				  >- (`nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 - err_inv) =
				         nR1 * err_inv + - (inv nR2) * err1 + err1 * err_inv`
                                           by REAL_ASM_ARITH_TAC
                                      \\ simp[REAL_NEG_MUL2]
				      \\ qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
                                           (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM
                                      \\ qunabbrev_tac `err_inv`
                                      \\ match_mp_tac REAL_LE_ADD2
				      \\ conj_tac \\ TRY REAL_ASM_ARITH_TAC
				      \\ match_mp_tac REAL_LE_ADD2
				      \\ conj_tac \\ TRY REAL_ASM_ARITH_TAC
				      \\ match_mp_tac REAL_LE_RMUL_IMP
				      \\ conj_tac \\ REAL_ASM_ARITH_TAC))
			      >- (match_mp_tac REAL_LE_TRANS
                                  \\ qexists_tac `nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 - err_inv)`
                                  \\ conj_tac
				  >- (fs [REAL_LE_ADD]
                                      \\ match_mp_tac REAL_LE_RMUL_IMP
				      \\ conj_tac \\ REAL_ASM_ARITH_TAC)
				  >- (`nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 - err_inv) =
				         nR1 * err_inv + inv nR2 * err1 - err1 * err_inv`
                                           by REAL_ASM_ARITH_TAC
                                      \\ simp[REAL_NEG_MUL2]
				      \\ qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
				           (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM
                                      \\ qunabbrev_tac `err_inv`
                                      \\ simp [real_sub]
				      \\ match_mp_tac REAL_LE_ADD2
				      \\ conj_tac
				      >- (match_mp_tac REAL_LE_ADD2
                                          \\ conj_tac \\ TRY REAL_ASM_ARITH_TAC
					  \\ match_mp_tac REAL_LE_RMUL_IMP
					  \\ conj_tac \\ REAL_ASM_ARITH_TAC)
				      >- (simp [REAL_NEG_LMUL]
                                          \\ match_mp_tac REAL_LE_RMUL_IMP
					  \\ conj_tac \\ REAL_ASM_ARITH_TAC)))))
							(* 0 <= - nF1 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `nR1 * inv nR2 + - nF1 * (inv nR2 + err2 * inv ((e2hi + err2) * (e2hi + err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_LE_LMUL_IMP \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2hi + err2) * (e2hi + err2))⁻¹)` \\
									qspecl_then [`inv nR2 + err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 + err_inv) =
											  - nR1 * err_inv + - (inv nR2) * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp[real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (simp [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 + err_inv) =
											  - nR1 * err_inv + inv nR2 * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)))))
						(* Case 2: Absolute value negative *)
						>- (fs[real_sub, real_div, REAL_NEG_LMUL, REAL_NEG_ADD] \\
							qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
							(* nF1 < 0 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `- nR1 * inv nR2 + nF1 * (inv nR2 - err2 * inv ((e2hi + err2) * (e2hi + err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2hi + err2) * (e2hi + err2))⁻¹)` \\
									qspecl_then [`inv nR2 - err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
 											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 - err_inv) =
											 - nR1 * err_inv + - (inv nR2) * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((-e2hi + -err2) * (-e2hi + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 - err_inv) =
											 - nR1 * err_inv + inv nR2 * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((-e2hi + -err2) * (-e2hi + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp [real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2, REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))))
							(* 0 <= - nF1 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `- nR1 * inv nR2 + nF1 * (inv nR2 + err2 * inv ((e2hi + err2) * (e2hi + err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_LE_LMUL_IMP \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2hi + err2) * (e2hi + err2))⁻¹)` \\
									qspecl_then [`inv nR2 + err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `-nR1 * inv nR2 + (nR1 - err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 + err_inv) =
											  nR1 * err_inv + - (inv nR2) * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((-e2hi + -err2) * (-e2hi + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp[real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2, REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2, REAL_NEG_LMUL, REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 + err_inv) =
											  nR1 * err_inv + inv nR2 * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((-e2hi + -err2) * (-e2hi + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2, REAL_NEG_LMUL] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))))))
					(* Negative case for abs (nR2 - nF2) <= err2 *)
					>- (fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2, REAL_NEG_LMUL] \\
						`nR2 <= nF2` by REAL_ASM_ARITH_TAC \\
						qpat_x_assum `nR2 <= nF2` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [GSYM REAL_LE_NEG] thm)) \\
						`inv (- nR2) <= inv (- nF2)` by (match_mp_tac REAL_INV_LE_ANTIMONO_IMPR \\ REAL_ASM_ARITH_TAC) \\
						`inv (- nR2) = - (inv nR2)` by (match_mp_tac (GSYM REAL_NEG_INV) \\ REAL_ASM_ARITH_TAC) \\
						`inv (- nF2) = - (inv nF2)` by (match_mp_tac (GSYM REAL_NEG_INV) \\ REAL_ASM_ARITH_TAC) \\
						rpt (
	        			  qpat_x_assum `inv (- _) = - (inv _)`
						    (fn thm => rule_assum_tac (fn hyp => REWRITE_RULE [thm] hyp))) \\
						`inv nF2 <= inv nR2` by REAL_ASM_ARITH_TAC \\
						qpat_x_assum `- _ <= - _` kall_tac \\
						`0 <= inv nR2 - inv nF2` by REAL_ASM_ARITH_TAC \\
						`(nR2⁻¹ − nF2⁻¹) ≤ err2 * ((e2hi + err2) * (e2hi + err2))⁻¹` by REAL_ASM_ARITH_TAC \\
						`inv nF2 <= inv nR2 + err2 * inv ((e2hi + err2) * (e2hi + err2))` by REAL_ASM_ARITH_TAC \\
						`inv nR2 - err2 * inv ((e2hi + err2) * (e2hi + err2)) <= inv nF2` by REAL_ASM_ARITH_TAC \\
						(* Next do a case distinction for the absolute value *)
						`! (x:real). ((abs x = x) /\ 0 <= x) \/ ((abs x = - x) /\ x < 0)` by REAL_ASM_ARITH_TAC \\
						qpat_x_assum `!x. A /\ B \/ C`
						  (fn thm => qspec_then `(nR1:real / nR2:real) - (nF1:real / nF2:real)` DISJ_CASES_TAC thm) \\
						fs[real_sub, real_div, REAL_NEG_LMUL, REAL_NEG_ADD]
						(* Case 1: Absolute value positive *)
						>- (qspecl_then [`-nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
							(* -nF1 < 0 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `nR1 * inv nR2 + - nF1 * (inv nR2 - err2 * inv ((e2hi + err2) * (e2hi + err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2hi + err2) * (e2hi + err2))⁻¹)` \\
									qspecl_then [`inv nR2 - err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 - err_inv) =
											  nR1 * err_inv + - (inv nR2) * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 - err_inv) =
											  nR1 * err_inv + inv nR2 * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp [real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (simp [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))))
							(* 0 <= - nF1 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `nR1 * inv nR2 + - nF1 * (inv nR2 + err2 * inv ((e2hi + err2) * (e2hi + err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_LE_LMUL_IMP \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2hi + err2) * (e2hi + err2))⁻¹)` \\
									qspecl_then [`inv nR2 + err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 + err_inv) =
											  - nR1 * err_inv + - (inv nR2) * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp[real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (simp [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 + err_inv) =
											  - nR1 * err_inv + inv nR2 * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)))))
						(* Case 2: Absolute value negative *)
						>- (qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
							(* nF1 < 0 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `- nR1 * inv nR2 + nF1 * (inv nR2 - err2 * inv ((e2hi + err2) * (e2hi + err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2hi + err2) * (e2hi + err2))⁻¹)` \\
									qspecl_then [`inv nR2 - err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
 											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 - err_inv) =
											 - nR1 * err_inv + - (inv nR2) * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 - err_inv) =
											 - nR1 * err_inv + inv nR2 * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp [real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2, REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))))
							(* 0 <= - nF1 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `- nR1 * inv nR2 + nF1 * (inv nR2 + err2 * inv ((e2hi + err2) * (e2hi + err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_LE_LMUL_IMP \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2hi + err2) * (e2hi + err2))⁻¹)` \\
									qspecl_then [`inv nR2 + err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `-nR1 * inv nR2 + (nR1 - err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 + err_inv) =
											  nR1 * err_inv + - (inv nR2) * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp[real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (fs [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 + err_inv) =
											  nR1 * err_inv + inv nR2 * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2hi + err2) * (e2hi + err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)))))))
               >- (CCONTR_TAC \\
                   rule_assum_tac (fn thm => REWRITE_RULE[IVlo_def, IVhi_def, widenInterval_def] thm) \\
                   `e2lo <= e2hi` by REAL_ASM_ARITH_TAC \\
                   `e2lo <= e2hi + err2` by REAL_ASM_ARITH_TAC \\
                   `e2lo <= e2hi + err2` by REAL_ASM_ARITH_TAC \\
                   REAL_ASM_ARITH_TAC)
               >- (CCONTR_TAC \\
                   rule_assum_tac (fn thm => REWRITE_RULE[IVlo_def, IVhi_def, widenInterval_def] thm) \\
                   `e2lo <= e2hi` by REAL_ASM_ARITH_TAC \\
                   `e2lo - err2 <= e2hi` by REAL_ASM_ARITH_TAC \\
                   REAL_ASM_ARITH_TAC)
			   (* The range of the divisor lies in the range from 0 to infinity *)
               >- (rule_assum_tac
				    (fn thm =>
                      REWRITE_RULE[IVlo_def, IVhi_def, widenInterval_def, contained_def, invertInterval_def] thm) \\
				   `abs (inv nR2 - inv nF2) <= err2 * (inv ((e2lo - err2) * (e2lo -err2)))`
                     by (match_mp_tac err_prop_inversion_pos \\
                         qexists_tac `e2hi` \\ rpt(conj_tac) \\
                         fs[contained_def, IVlo_def, IVhi_def]) \\
				   fs [widenInterval_def, IVlo_def, IVhi_def, invertInterval_def] \\
				   `minAbsFun (e2lo - err2, e2hi + err2) = (e2lo - err2)`
                     by (match_mp_tac minAbs_positive_iv_is_lo \\ REAL_ASM_ARITH_TAC) \\
				   simp[] \\
				   qpat_x_assum `minAbsFun _ = _ ` kall_tac \\
				   `nF1 <= err1 + nR1` by REAL_ASM_ARITH_TAC \\
				   `nR1 - err1 <= nF1` by REAL_ASM_ARITH_TAC \\
				   `(nR2 - nF2 > 0 /\ nR2 - nF2 <= err2) \/ (nR2 - nF2 <= 0 /\ - (nR2 - nF2) <= err2)`
                     by REAL_ASM_ARITH_TAC
					(* Positive case for abs (nR2 - nF2) <= err2 *)
					>- (`nF2 < nR2` by REAL_ASM_ARITH_TAC \\
						`inv nR2 < inv nF2` by (match_mp_tac REAL_LT_INV \\ TRY REAL_ASM_ARITH_TAC) \\
						`inv nR2 - inv nF2 < 0` by REAL_ASM_ARITH_TAC \\
						`nR2⁻¹ − nF2⁻¹ ≤ err2 * ((e2lo - err2) * (e2lo - err2))⁻¹` by REAL_ASM_ARITH_TAC \\
						`inv nF2 <= inv nR2 + err2 * inv ((e2lo - err2) * (e2lo - err2))` by REAL_ASM_ARITH_TAC \\
						`inv nR2 - err2 * inv ((e2lo - err2) * (e2lo - err2)) <= inv nF2` by REAL_ASM_ARITH_TAC \\
						(* Next do a case distinction for the absolute value *)
						`! (x:real). ((abs x = x) /\ 0 <= x) \/ ((abs x = - x) /\ x < 0)` by REAL_ASM_ARITH_TAC \\
						qpat_x_assum `!x. A /\ B \/ C`
						  (fn thm => qspec_then `(nR1:real / nR2:real) - (nF1:real / nF2:real)` DISJ_CASES_TAC thm)
						(* Case 1: Absolute value positive *)
						>- (fs[real_sub, real_div, REAL_NEG_LMUL] \\
							qspecl_then [`-nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
							(* -nF1 < 0 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `nR1 * inv nR2 + - nF1 * (inv nR2 - err2 * inv ((e2lo - err2) * (e2lo - err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2lo - err2) * (e2lo - err2))⁻¹)` \\
									qspecl_then [`inv nR2 - err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 - err_inv) =
											  nR1 * err_inv + - (inv nR2) * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + - err2) * (e2lo + - err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											simp[GSYM real_sub] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 - err_inv) =
											  nR1 * err_inv + inv nR2 * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp [real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (simp [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))))
							(* 0 <= - nF1 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `nR1 * inv nR2 + - nF1 * (inv nR2 + err2 * inv ((e2lo - err2) * (e2lo - err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_LE_LMUL_IMP \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2lo - err2) * (e2lo - err2))⁻¹)` \\
									qspecl_then [`inv nR2 + err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 + err_inv) =
											  - nR1 * err_inv + - (inv nR2) * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp[real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (simp [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 + err_inv) =
											  - nR1 * err_inv + inv nR2 * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											simp [GSYM real_sub] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)))))
						(* Case 2: Absolute value negative *)
						>- (fs[real_sub, real_div, REAL_NEG_LMUL, REAL_NEG_ADD] \\
							qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
							(* nF1 < 0 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `- nR1 * inv nR2 + nF1 * (inv nR2 - err2 * inv ((e2lo - err2) * (e2lo - err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2lo - err2) * (e2lo - err2))⁻¹)` \\
									qspecl_then [`inv nR2 - err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
 											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 - err_inv) =
											 - nR1 * err_inv + - (inv nR2) * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											simp [GSYM real_sub] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 - err_inv) =
											 - nR1 * err_inv + inv nR2 * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp [real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2, REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))))
							(* 0 <= - nF1 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `- nR1 * inv nR2 + nF1 * (inv nR2 + err2 * inv ((e2lo - err2) * (e2lo - err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_LE_LMUL_IMP \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2lo - err2) * (e2lo - err2))⁻¹)` \\
									qspecl_then [`inv nR2 + err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `-nR1 * inv nR2 + (nR1 - err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 + err_inv) =
											  nR1 * err_inv + - (inv nR2) * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp[real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (fs [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 + err_inv) =
											  nR1 * err_inv + inv nR2 * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											simp [GSYM real_sub] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))))))
					(* Negative case for abs (nR2 - nF2) <= err2 *)
					>- (fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2, REAL_NEG_LMUL] \\
						`nR2 <= nF2` by REAL_ASM_ARITH_TAC \\
						`inv nF2 <= inv nR2` by (match_mp_tac REAL_INV_LE_ANTIMONO_IMPR \\ REAL_ASM_ARITH_TAC) \\
						`0 <= inv nR2 - inv nF2` by REAL_ASM_ARITH_TAC \\
						`(nR2⁻¹ − nF2⁻¹) ≤ err2 * ((e2lo - err2) * (e2lo - err2))⁻¹` by REAL_ASM_ARITH_TAC \\
						`inv nF2 <= inv nR2 + err2 * inv ((e2lo - err2) * (e2lo - err2))` by REAL_ASM_ARITH_TAC \\
						`inv nR2 - err2 * inv ((e2lo - err2) * (e2lo - err2)) <= inv nF2` by REAL_ASM_ARITH_TAC \\
						(* Next do a case distinction for the absolute value *)
						`! (x:real). ((abs x = x) /\ 0 <= x) \/ ((abs x = - x) /\ x < 0)` by REAL_ASM_ARITH_TAC \\
						qpat_x_assum `!x. A /\ B \/ C`
						  (fn thm => qspec_then `(nR1:real / nR2:real) - (nF1:real / nF2:real)` DISJ_CASES_TAC thm) \\
						fs[real_sub, real_div, REAL_NEG_LMUL, REAL_NEG_ADD]
						(* Case 1: Absolute value positive *)
						>- (qspecl_then [`-nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
							(* -nF1 < 0 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `nR1 * inv nR2 + - nF1 * (inv nR2 - err2 * inv ((e2lo - err2) * (e2lo - err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2lo - err2) * (e2lo - err2))⁻¹)` \\
									qspecl_then [`inv nR2 - err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 - err_inv) =
											  nR1 * err_inv + - (inv nR2) * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											simp [GSYM real_sub] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 - err_inv) =
											  nR1 * err_inv + inv nR2 * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp [real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (simp [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))))
							(* 0 <= - nF1 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `nR1 * inv nR2 + - nF1 * (inv nR2 + err2 * inv ((e2lo - err2) * (e2lo - err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_LE_LMUL_IMP \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2lo - err2) * (e2lo - err2))⁻¹)` \\
									qspecl_then [`inv nR2 + err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`nR1 * inv nR2 + - (nR1 + err1) * (inv nR2 + err_inv) =
											  - nR1 * err_inv + - (inv nR2) * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp[real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (simp [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`nR1 * inv nR2 + - (nR1 + - err1) * (inv nR2 + err_inv) =
											  - nR1 * err_inv + inv nR2 * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											simp [GSYM real_sub] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)))))
						(* Case 2: Absolute value negative *)
						>- (qspecl_then [`nF1`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
							(* nF1 < 0 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `- nR1 * inv nR2 + nF1 * (inv nR2 - err2 * inv ((e2lo - err2) * (e2lo - err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_MUL_LE_COMPAT_NEG_L \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2lo - err2) * (e2lo - err2))⁻¹)` \\
									qspecl_then [`inv nR2 - err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
 											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 - err_inv) =
											 - nR1 * err_inv + - (inv nR2) * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											simp [GSYM real_sub] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 - err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 - err_inv) =
											 - nR1 * err_inv + inv nR2 * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp [real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (fs [GSYM REAL_NEG_ADD, REAL_NEG_MUL2, REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))))
							(* 0 <= - nF1 *)
							>- (match_mp_tac REAL_LE_TRANS \\
								qexists_tac `- nR1 * inv nR2 + nF1 * (inv nR2 + err2 * inv ((e2lo - err2) * (e2lo - err2)))` \\
								conj_tac
								>- (fs[REAL_LE_LADD] \\
									match_mp_tac REAL_LE_LMUL_IMP \\
									conj_tac \\ REAL_ASM_ARITH_TAC)
								>- (qabbrev_tac `err_inv = (err2 * ((e2lo - err2) * (e2lo - err2))⁻¹)` \\
									qspecl_then [`inv nR2 + err_inv`, `0`] DISJ_CASES_TAC REAL_LTE_TOTAL
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `-nR1 * inv nR2 + (nR1 - err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											once_rewrite_tac [REAL_MUL_COMM] \\
											match_mp_tac REAL_MUL_LE_COMPAT_NEG_L\\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											fs [REAL_LE_NEG])
										>- (`- nR1 * inv nR2 + (nR1 - err1) * (inv nR2 + err_inv) =
											  nR1 * err_inv + - (inv nR2) * err1 - err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											simp[real_sub] \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac
											>- (match_mp_tac REAL_LE_ADD2 \\
												conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)
											>- (fs [REAL_NEG_LMUL] \\
												match_mp_tac REAL_LE_RMUL_IMP \\
												conj_tac \\ REAL_ASM_ARITH_TAC)))
									>- (match_mp_tac REAL_LE_TRANS \\
										qexists_tac `- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 + err_inv)` \\
										conj_tac
										>- (fs [REAL_LE_ADD] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC)
										>- (`- nR1 * inv nR2 + (nR1 + err1) * (inv nR2 + err_inv) =
											  nR1 * err_inv + inv nR2 * err1 + err1 * err_inv`
										      by REAL_ASM_ARITH_TAC \\
											simp[REAL_NEG_MUL2] \\
											qspecl_then [`inv ((e2lo + -err2) * (e2lo + -err2))`,`err2`]
											  (fn thm => ONCE_REWRITE_TAC [thm]) REAL_MUL_COMM \\
											qunabbrev_tac `err_inv` \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											match_mp_tac REAL_LE_ADD2 \\
											conj_tac \\ TRY REAL_ASM_ARITH_TAC \\
											simp [GSYM real_sub] \\
											match_mp_tac REAL_LE_RMUL_IMP \\
											conj_tac \\ REAL_ASM_ARITH_TAC))))))))
           >- (simp[maxAbs_def] \\
               once_rewrite_tac [REAL_MUL_COMM] \\ match_mp_tac REAL_LE_LMUL_IMP \\
               conj_tac \\ simp[mEps_geq_zero] \\
               match_mp_tac maxAbs \\
               `contained (nF1 / nF2) (divideInterval (widenInterval (e1lo, e1hi) err1) (widenInterval (e2lo, e2hi) err2))`
                 by (match_mp_tac interval_division_valid \\
                     conj_tac \\ fs[]) \\
              rule_assum_tac (fn thm => REWRITE_RULE [contained_def, IVlo_def, IVhi_def] thm) \\
              fs[widenInterval_def, IVlo_def, IVhi_def,noDivzero_def])));

val validErrorbound_sound = store_thm ("validErrorbound_sound",
  ``!(e:real exp) (E1 E2:env) (absenv:analysisResult)
     (nR nF err:real) (P:precond) (elo ehi:real) (fVars:num_set) dVars.
       approxEnv E1 absenv fVars dVars E2 /\
       ((domain (usedVars e)) DIFF (domain dVars)) SUBSET (domain fVars) /\
       eval_exp 0 E1 e nR /\
       eval_exp machineEpsilon E2 e nF /\
       validErrorbound e absenv dVars /\
       validIntervalbounds e absenv P dVars /\
     (!v.
         v IN domain dVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (FST (absenv (Var v))) <= r /\
           r <= SND (FST (absenv (Var v)))) /\
      (!v.
         v IN domain fVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (P v) <= r /\ r <= SND (P v)) /\
	(absenv e = ((elo,ehi),err)) ==>
	abs (nR - nF) <= err``,
  Induct_on `e` \\ rpt strip_tac \\ fs[]
  >- (drule validErrorboundCorrectVariable
      \\ disch_then (qspecl_then [`n`, `nR`, `nF`, `err`, `elo`, `ehi`, `P`] match_mp_tac)
      \\ fs [])
  >- (drule validErrorboundCorrectConstant \\
      disch_then (
        qspecl_then [`E2`, `absenv`, `nF`, `err`, `elo`, `ehi`, `dVars`]
           match_mp_tac)
      \\ fs[]
      \\ drule validIntervalbounds_sound
      \\ disch_then (
           qspecl_then [`fVars`, `E1`, `nR`] ASSUME_TAC)
      \\ qpat_x_assum `absenv _ = _` (fn thm => fs[thm])
      \\ first_x_assum match_mp_tac
      \\ fs [usedVars_def, SUBSET_DEF])
  >- (qpat_x_assum `validErrorbound _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validErrorbound_def] thm))
      \\ qpat_x_assum `absenv _ = _` (fn thm => fs[thm])
      \\ Cases_on `u` \\ fs []
      \\ rveq
      \\ Cases_on `absenv e`
      \\ fs[]
      \\ inversion `eval_exp 0 _ _ _` eval_exp_cases
      \\ inversion `eval_exp machineEpsilon _ _ _` eval_exp_cases
      \\ fs [evalUnop_def]
      \\ once_rewrite_tac [real_sub] \\ once_rewrite_tac [GSYM REAL_NEG_ADD]
      \\ once_rewrite_tac [ABS_NEG]
      \\ once_rewrite_tac [GSYM real_sub]
      \\ first_x_assum match_mp_tac
      \\ qexistsl_tac [`E1`, `E2`, `absenv`, `P`, `FST q`, `SND q`, `fVars`, `dVars`]
      \\ fs [Once usedVars_def]
      \\ rw_thm_asm `validIntervalbounds _ _ _ _` validIntervalbounds_def
      \\ fs []
      \\ Cases_on `absenv e` \\ Cases_on `absenv (Unop Neg e)`
      \\ fs [])
  >- (rename1 `Binop op e1 e2`
      \\ inversion `eval_exp 0 _ _ _` eval_exp_cases
      \\ rename1 `eval_exp 0 _ e1 nR1`
      \\ rename1 `eval_exp 0 _ e2 nR2`
      \\ qpat_x_assum `eval_exp machineEpsilon _ _ _`
           (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [binary_unfolding] thm))
      \\ fs []
      \\ rename1 `eval_exp machineEpsilon _ e1 nF1`
      \\ rename1 `eval_exp machineEpsilon _ e2 nF2`
      \\ rveq
      \\ Cases_on `absenv e1` \\ rename1 `absenv e1 = (iv, err1)`
      \\ Cases_on `iv` \\ rename1 `absenv e1  = ((e1lo, e1hi),err1)`
      \\ Cases_on `absenv e2` \\ rename1 `absenv e2 = (iv, err2)`
      \\ Cases_on `iv` \\ rename1 `absenv e2 = ((e2lo, e2hi), err2)`
      \\ `abs (nR2 - nF2) <= err2`
                by (qpat_x_assum
                      `∀E1 E2 absenv nR nF err P elo ehi fVars dVars.
                         _ /\ _ /\
                         eval_exp 0 E1 e2 nR ∧
                         eval_exp machineEpsilon E2 e2 nF ∧
                         _ ⇒
                         abs (nR − nF) ≤ err`
                      match_mp_tac
                    \\ qexistsl_tac [`E1`, `E2`, `absenv`, `P`, `e2lo`, `e2hi`, `fVars`, `dVars`]
                    \\ qpat_x_assum `validErrorbound _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validErrorbound_def] thm))
                    \\ qpat_x_assum `validIntervalbounds _ _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validIntervalbounds_def] thm))
                    \\ fs[]
                    \\ rpt (qpat_x_assum `absenv _ = _ ` (fn thm => rule_assum_tac (fn asm => REWRITE_RULE [thm] asm)))
                    \\ fs[SUBSET_DEF]
                    \\ rpt strip_tac
                    \\ first_x_assum match_mp_tac
                    \\ once_rewrite_tac [usedVars_def]
                    \\ fs [domain_union])
          \\ `abs (nR1 - nF1) <= err1`
               by (qpat_x_assum
                     `∀E1 E2 absenv nR nF err P elo ehi fVars dVars.
                        _ ∧ _ /\
                        eval_exp 0 E1 e1 nR ∧
                        eval_exp machineEpsilon E2 e1 nF ∧
                        _ ⇒
                        abs (nR − nF) ≤ err`
                      match_mp_tac
                   \\ qexistsl_tac [`E1`, `E2`, `absenv`, `P`, `e1lo`, `e1hi`, `fVars`, `dVars`]
                   \\ qpat_x_assum `validErrorbound _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validErrorbound_def] thm))
                   \\ qpat_x_assum `validIntervalbounds _ _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validIntervalbounds_def] thm))
                   \\ fs[]
                   \\ rpt (qpat_x_assum `absenv _ = _ ` (fn thm => rule_assum_tac (fn asm => REWRITE_RULE [thm] asm)))
                    \\ fs[SUBSET_DEF]
                    \\ rpt strip_tac
                    \\ first_x_assum match_mp_tac
                    \\ once_rewrite_tac [usedVars_def]
                    \\ fs [domain_union])
          \\ `FST (FST (absenv e1)) <= nR1 /\ nR1 <= SND (FST (absenv e1))`
               by (match_mp_tac validIntervalbounds_sound
                   \\ qexistsl_tac [`P`, `fVars`, `dVars`, `E1`]
                   \\ qpat_x_assum `validIntervalbounds _ _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validIntervalbounds_def] thm))
                   \\ rpt (qpat_x_assum `absenv _ = _ ` (fn thm => fs [thm]))
                   \\ fs[SUBSET_DEF]
                   \\ rpt strip_tac
                   \\ first_x_assum match_mp_tac
                   \\ once_rewrite_tac [usedVars_def]
                   \\ fs [domain_union])
          \\ `FST (FST (absenv e2)) <= nR2 /\ nR2 <= SND (FST (absenv e2))`
               by (match_mp_tac validIntervalbounds_sound
                   \\ qexistsl_tac [`P`, `fVars`, `dVars`, `E1`]
                   \\ qpat_x_assum `validIntervalbounds _ _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validIntervalbounds_def] thm))
                   \\ rpt (qpat_x_assum `absenv _ = _ ` (fn thm => fs [thm]))
                   \\ fs[SUBSET_DEF]
                   \\ rpt strip_tac
                   \\ first_x_assum match_mp_tac
                   \\ once_rewrite_tac [usedVars_def]
                   \\ fs [domain_union])
          \\ Cases_on `op`
          >- (match_mp_tac validErrorboundCorrectAddition
              \\ qexistsl_tac [`E1`, `E2`, `absenv`, `e1`, `e2`, `nR1`, `nR2`,
                               `nF1`, `nF2`, `err1`, `err2`, `elo`, `ehi`,
                               `e1lo`, `e1hi`, `e2lo`, `e2hi`, `dVars`]
              \\ rpt (qpat_x_assum `absenv _ = _ ` (fn thm => fs [thm]))
              \\ match_mp_tac Binop_dist \\ fs [])
          >- (match_mp_tac validErrorboundCorrectSubtraction
              \\ qexistsl_tac [`E1`, `E2`, `absenv`, `e1`, `e2`, `nR1`, `nR2`,
                               `nF1`, `nF2`, `err1`, `err2`, `elo`, `ehi`,
                               `e1lo`, `e1hi`, `e2lo`, `e2hi`, `dVars`]
              \\ rpt (qpat_x_assum `absenv _ = _ ` (fn thm => fs [thm]))
              \\ match_mp_tac Binop_dist \\ fs [])
          >- (match_mp_tac validErrorboundCorrectMult
              \\ qexistsl_tac [`E1`, `E2`, `absenv`, `e1`, `e2`, `nR1`, `nR2`,
                               `nF1`, `nF2`, `err1`, `err2`, `elo`, `ehi`,
                               `e1lo`, `e1hi`, `e2lo`, `e2hi`, `dVars`]
              \\ rpt (qpat_x_assum `absenv _ = _ ` (fn thm => fs [thm]))
              \\ match_mp_tac Binop_dist \\ fs [])
          >- (match_mp_tac validErrorboundCorrectDiv
              \\ qexistsl_tac [`E1`, `E2`, `absenv`, `e1`, `e2`, `nR1`, `nR2`,
                               `nF1`, `nF2`, `err1`, `err2`, `elo`, `ehi`,
                               `e1lo`, `e1hi`, `e2lo`, `e2hi`, `dVars`]
              \\ fs []
              \\ conj_tac
              >- (match_mp_tac Binop_dist \\ fs [])
              >- (fs [Once validIntervalbounds_def, IVhi_def, IVlo_def]))));

val validErrorboundCmd_sound = store_thm ("validErrorboundCmd_sound",
  ``!(f:real cmd) (absenv:analysisResult) (E1 E2:env)
     (outVars fVars dVars:num_set) (vR vF elo ehi err:real) (P:precond).
      approxEnv E1 absenv fVars dVars E2 /\
      ssa f (union fVars dVars) outVars /\
      ((domain (freeVars f)) DIFF (domain dVars)) SUBSET (domain fVars) /\
      bstep f E1 0 vR /\
      bstep f E2 machineEpsilon vF /\
      validErrorboundCmd f absenv dVars /\
      validIntervalboundsCmd f absenv P dVars /\
      (!v.
         v IN domain dVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (FST (absenv (Var v))) <= r /\
           r <= SND (FST (absenv (Var v)))) /\
       (!v.
         v IN domain fVars ==>
         ?r.
           (E1 v = SOME r) /\
           FST (P v) <= r /\ r <= SND (P v)) /\
      (absenv (getRetExp f) = ((elo,ehi),err)) ==>
     abs (vR - vF) <= err``,
  Induct_on `f` \\ rpt strip_tac
  >- (inversion `bstep _ _ 0 _` bstep_cases \\ rveq
      \\ inversion `bstep _ _ machineEpsilon _` bstep_cases \\ rveq
      \\ inversion `ssa _ _ _` ssa_cases \\ rveq
      \\ rename1 `eval_exp 0 _ e vr`
      \\ rename1 `eval_exp machineEpsilon _ e vf`
      \\ first_x_assum match_mp_tac
      \\ qpat_x_assum `validErrorboundCmd _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validErrorboundCmd_def] thm))
      \\ qpat_x_assum `validIntervalboundsCmd _ _ _ _` (fn thm => ASSUME_TAC (ONCE_REWRITE_RULE [validIntervalboundsCmd_def] thm))
      \\ qexistsl_tac [`absenv`, `updEnv n vr E1`, `updEnv n vf E2`, `outVars`, `fVars`, `insert n () dVars`, `elo`, `ehi`, `P`]
      \\ fs [Once getRetExp_def]
      \\ rpt conj_tac
      >- (match_mp_tac approxUpdBound
          \\ fs[]
          \\ qpat_x_assum `absenv e = _` (fn thm => simp[GSYM thm])
          \\ conj_tac
          >- (Cases_on `absenv e`
              \\ rename1 `absenv e = (iv, err_e)`
              \\ fs []
              \\ match_mp_tac validErrorbound_sound
              \\ qexistsl_tac [`e`, `E1`, `E2`, `absenv`,`P`, `FST iv`, `SND iv`, `fVars`, `dVars`]
              \\ fs [Once freeVars_def, domain_union, SUBSET_DEF]
              \\ rpt strip_tac
              \\ first_x_assum match_mp_tac
              \\ fs []
              \\ metis_tac [])
          >- (Cases_on `lookup n (union fVars dVars)` \\ fs [domain_lookup]))
      >- (match_mp_tac ssa_equal_set
          \\ qexists_tac `insert n () (union fVars dVars)` \\ fs [ domain_union]
          \\ once_rewrite_tac [UNION_COMM]
          \\ fs [INSERT_UNION_EQ])
      >- (fs [SUBSET_DEF] \\ rpt strip_tac
          \\ first_x_assum match_mp_tac
          \\ once_rewrite_tac [freeVars_def]
          \\ fs [domain_union])
      >- (Cases_on `x = n` \\ rveq \\ fs []
          >- (rpt strip_tac \\ rveq \\ fs []
              >- (qexists_tac `vr` \\ fs [updEnv_def]
                  \\ rw_sym_asm `absenv e = _`
                  \\ match_mp_tac validIntervalbounds_sound
                  \\ qexistsl_tac [`P`, `fVars`, `dVars`, `E1`] \\ fs[SUBSET_DEF]
                  \\ rpt strip_tac \\ first_x_assum match_mp_tac
                  \\ fs [Once freeVars_def, domain_union]
                  \\ metis_tac [])
              >- (fs [updEnv_def]
                  \\ Cases_on `v = n` \\ fs[]
                  \\ rveq \\ fs [domain_union]))
          >- (rpt strip_tac \\ fs []
              >- (rveq
                  \\ qexists_tac `vr` \\ fs [updEnv_def]
                  \\ rw_sym_asm `absenv e = _`
                  \\ match_mp_tac validIntervalbounds_sound
                  \\ qexistsl_tac [`P`, `fVars`, `dVars`, `E1`] \\ fs[SUBSET_DEF]
                  \\ rpt strip_tac \\ first_x_assum match_mp_tac
                  \\ fs [Once freeVars_def, domain_union]
                  \\ metis_tac [])
              >- (fs [updEnv_def]
                  \\ Cases_on `v = n` \\ fs[]
                  \\ rveq \\ fs [domain_union])))
      >- (rpt strip_tac
          \\ fs [updEnv_def]
          \\ Cases_on `n = v` \\ rveq \\ fs []
          \\ fs [domain_union]))
  >- (inversion `bstep _ _ 0 _` bstep_cases \\ rveq
      \\ inversion `bstep _ _ machineEpsilon _` bstep_cases \\ rveq
      \\ rename1 `eval_exp 0 _ e vR`
      \\ rename1 `eval_exp machineEpsilon _ e vF`
      \\ rw_thm_asm `validErrorboundCmd _ _ _` validErrorboundCmd_def
      \\ rw_thm_asm `validIntervalboundsCmd _ _ _ _` validIntervalboundsCmd_def
      \\ fs []
      \\ drule validErrorbound_sound
      \\ disch_then (
           qspecl_then [`e`, `vR`, `vF`, `err`, `P`, `elo`, `ehi`]
           match_mp_tac)
      \\ fs[freeVars_def, getRetExp_def]));

val _ = export_theory();
