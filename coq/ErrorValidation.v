(**
   This file contains the coq implementation of the error bound validator as well
   as its soundness proof. The function validErrorbound is the Error bound
   validator from the certificate checking process. Under the assumption that a
   valid range arithmetic result has been computed, it can validate error bounds
   encoded in the analysis result. The validator is used in the file
   CertificateChecker.v to build the complete checker.
 **)
Require Import Coq.QArith.QArith Coq.QArith.Qminmax Coq.QArith.Qabs Coq.QArith.Qreals Coq.Lists.List.
Require Import Coq.micromega.Psatz Coq.Reals.Reals.
Require Import Daisy.Infra.Abbrevs Daisy.Infra.RationalSimps Daisy.Infra.RealRationalProps Daisy.Infra.RealSimps Daisy.Infra.Ltacs.
Require Import Daisy.Environments Daisy.IntervalValidation Daisy.ErrorBounds.

(** Error bound validator **)
Fixpoint validErrorbound (e:exp Q) (absenv:analysisResult) (dVars:NatSet.t):=
  let (intv, err) := (absenv e) in
  if (Qleb 0 err)
  then
    match e with
    |Var _ v =>
     if (NatSet.mem v dVars)
     then true
     else (Qleb (maxAbs intv * RationalSimps.machineEpsilon) err)
    |Const n =>
     Qleb (maxAbs intv * RationalSimps.machineEpsilon) err
    |Unop Neg e =>
     if (validErrorbound e absenv dVars)
     then Qeq_bool err (snd (absenv e))
     else false
    |Unop Inv e => false
    |Binop b e1 e2 =>
     if ((validErrorbound e1 absenv dVars) && (validErrorbound e2 absenv dVars))
     then
          let (ive1, err1) := absenv e1 in
          let (ive2, err2) := absenv e2 in
          let errIve1 := widenIntv ive1 err1 in
          let errIve2 := widenIntv ive2 err2 in
          let upperBoundE1 := maxAbs ive1 in
          let upperBoundE2 := maxAbs ive2 in
          match b with
          | Plus =>
            Qleb (err1 + err2 + (maxAbs (addIntv errIve1 errIve2)) * machineEpsilon) err
          | Sub =>
            Qleb (err1 + err2 + (maxAbs (subtractIntv errIve1 errIve2)) * machineEpsilon) err
          | Mult =>
            Qleb ((upperBoundE1 * err2 + upperBoundE2 * err1 + err1 * err2)  + (maxAbs (multIntv errIve1 errIve2)) * machineEpsilon) err
          | Div =>
            if (((Qleb (ivhi errIve2) 0) && (negb (Qeq_bool (ivhi errIve2) 0))) ||
                 ((Qleb 0 (ivlo errIve2)) && (negb (Qeq_bool (ivlo errIve2) 0))))
                then
                  let upperBoundInvE2 := maxAbs (invertIntv ive2) in
                  let minAbsIve2 := minAbs (errIve2) in
                  let errInv := (1 / (minAbsIve2 * minAbsIve2)) * err2 in
                  Qleb ((upperBoundE1 * errInv + upperBoundInvE2 * err1 + err1 * errInv) + (maxAbs (divideIntv errIve1 errIve2)) * machineEpsilon) err
                else false
          end
     else false
    end
  else false.

(** Error bound command validator **)
Fixpoint validErrorboundCmd (f:cmd Q) (env:analysisResult) (dVars:NatSet.t) {struct f} : bool :=
  match f with
  |Let x e g =>
   if ((validErrorbound e env dVars) && (Qeq_bool (snd (env e)) (snd (env (Var Q x)))))
   then validErrorboundCmd g env (NatSet.add x dVars)
   else false
  |Ret e => validErrorbound e env dVars
  end.

(**
    Since errors are intervals with 0 as center, we encode them as single values.
    This lemma enables us to deduce from each run of the validator the invariant
    that when it succeeds, the errors must be positive.
**)
Lemma err_always_positive e (absenv:analysisResult) iv err dVars:
  validErrorbound e absenv dVars = true ->
  (iv,err) = absenv e ->
  (0 <= Q2R err)%R.
Proof.
  destruct e;intros validErrorbound_e absenv_e;
    unfold validErrorbound in validErrorbound_e;
    rewrite <- absenv_e in validErrorbound_e; simpl in *;
      andb_to_prop validErrorbound_e.
  - apply Qle_bool_iff in L; apply Qle_Rle in L; rewrite Q2R0_is_0 in L; auto.
  - apply Qle_bool_iff in L; apply Qle_Rle in L; rewrite Q2R0_is_0 in L; auto.
  - destruct u; simpl in *; try congruence.
    andb_to_prop R.
    apply Qle_bool_iff in L; apply Qle_Rle in L; rewrite Q2R0_is_0 in L; auto.
  - apply Qle_bool_iff in L; apply Qle_Rle in L; rewrite Q2R0_is_0 in L; auto.
Qed.

Lemma validErrorboundCorrectVariable:
  forall E1 E2 absenv (v:nat) nR nF e nlo nhi P fVars dVars,
    approxEnv E1 absenv fVars dVars E2 ->
    eval_exp 0%R E1 (toRExp (Var Q v)) nR ->
    eval_exp (Q2R machineEpsilon) E2 (toRExp (Var Q v)) nF ->
    validIntervalbounds (Var Q v) absenv P dVars = true ->
    validErrorbound (Var Q v) absenv dVars = true ->
    (forall v,
        NatSet.mem v dVars = true ->
        exists r : R,
          E1 v = Some r /\
          (Q2R (fst (fst (absenv (Var Q v)))) <= r <= Q2R (snd (fst (absenv (Var Q v)))))%R) ->
    (forall v, NatSet.mem v fVars= true ->
          exists r, E1 v = Some r /\
                (Q2R (fst (P v)) <= r <= Q2R (snd (P v)))%R) ->
    absenv (Var Q v) = ((nlo, nhi), e) ->
    (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros E1 E2 absenv v nR nF e nlo nhi P fVars dVars approxCEnv eval_real
         eval_float bounds_valid error_valid dVars_sound P_valid absenv_var.
  inversion eval_real; inversion eval_float; subst.
  rename H0 into E1_v;
    rename H3 into E2_v.
  simpl in error_valid.
  rewrite absenv_var in error_valid; simpl in error_valid.
  rewrite <- andb_lazy_alt in error_valid.
  andb_to_prop error_valid.
  rename L into error_pos.
  rename R into error_valid.
  (* induction on the approximation relation to do a case distinction on whether
     we argue currently about a free or a let bound variable *)
  induction approxCEnv.
  (* empty environment case, contradiction *)
  - unfold emptyEnv in *; simpl in *.
    congruence.
  - unfold updEnv in *; simpl in *.
    case_eq (v =? x); intros eq_case; rewrite eq_case in *.
    + rewrite Nat.eqb_eq in eq_case; subst.
      assert (NatSet.mem x dVars = false) as x_not_bound.
      * case_eq (NatSet.mem x dVars); intros case_mem; try auto.
        rewrite NatSet.mem_spec in case_mem.
        assert (NatSet.In x (NatSet.union fVars dVars))
          as x_in_union by (rewrite NatSet.union_spec; auto).
        rewrite <- NatSet.mem_spec in x_in_union.
        rewrite x_in_union in *.
        congruence.
      * rewrite x_not_bound in error_valid.
        inversion E1_v; inversion E2_v;
          subst.
        eapply Rle_trans; try eauto.
        apply Qle_bool_iff in error_valid.
        apply Qle_Rle in error_valid.
        eapply Rle_trans; eauto.
        rewrite Q2R_mult.
        apply Rmult_le_compat_r.
        { apply mEps_geq_zero. }
        { rewrite <- maxAbs_impl_RmaxAbs.
          apply contained_leq_maxAbs.
          unfold contained; simpl.
          pose proof (validIntervalbounds_sound (Var Q x) A P (E:=fun y : nat => if y =? x then Some nR else E1 y) (vR:=nR) bounds_valid (fVars := (NatSet.add x fVars))) as valid_bounds_prf.
          rewrite absenv_var in valid_bounds_prf; simpl in valid_bounds_prf.
          apply valid_bounds_prf; try auto.
          intros v v_mem_diff.
          rewrite NatSet.diff_spec, NatSet.singleton_spec in v_mem_diff.
          destruct v_mem_diff as [v_eq v_no_dVar].
          subst.
          rewrite NatSet.add_spec; auto. }
    + apply IHapproxCEnv; try auto.
      * constructor; auto.
      * constructor; auto.
      * intros v0 mem_dVars;
          specialize (dVars_sound v0 mem_dVars).
        destruct dVars_sound as [vR0 [val_def iv_sound_val]].
        case_eq (v0 =? x); intros case_mem;
          rewrite case_mem in val_def; simpl in val_def.
        { rewrite Nat.eqb_eq in case_mem; subst.
          rewrite NatSet.mem_spec in mem_dVars.
          assert (NatSet.In x (NatSet.union fVars dVars))
            as x_in_union by (rewrite NatSet.union_spec; auto).
          rewrite <- NatSet.mem_spec in x_in_union;
            rewrite x_in_union in *; congruence. }
        { exists vR0; split; auto. }
      * intros v0 v0_fVar.
        assert (NatSet.mem v0 (NatSet.add x fVars) = true)
          as v0_in_add by (rewrite NatSet.mem_spec, NatSet.add_spec; rewrite NatSet.mem_spec in v0_fVar; auto).
        specialize (P_valid v0 v0_in_add).
        case_eq (v0 =? x); intros case_v0; rewrite case_v0 in *; try auto.
        rewrite Nat.eqb_eq in case_v0; subst.
        assert (NatSet.mem x (NatSet.union fVars dVars) = true)
          as x_in_union
            by (rewrite NatSet.mem_spec, NatSet.union_spec; rewrite NatSet.mem_spec in v0_fVar; auto).
        rewrite x_in_union in *; congruence.
  - unfold updEnv in E1_v, E2_v; simpl in *.
    case_eq (v =? x); intros eq_case; rewrite eq_case in *.
    + rewrite Nat.eqb_eq in eq_case; subst.
      inversion E1_v; inversion E2_v; subst.
      rewrite absenv_var in *; auto.
    + apply IHapproxCEnv; try auto.
      * constructor; auto.
      * constructor; auto.
      * rewrite absenv_var.
        case_eq (NatSet.mem v dVars);
        intros case_dVars; rewrite case_dVars in *; simpl in *; try auto.
        assert (NatSet.mem v (NatSet.add x dVars) = false) as not_in_add.
        { case_eq (NatSet.mem v (NatSet.add x dVars));
            intros case_add; rewrite case_add in *; simpl in *; try auto.
          - rewrite NatSet.mem_spec in case_add.
            rewrite NatSet.add_spec in case_add.
            destruct case_add as [v_eq_x | v_dVar]; subst.
            + rewrite Nat.eqb_neq in eq_case. exfalso; apply eq_case; auto.
            + rewrite <- NatSet.mem_spec in v_dVar. rewrite v_dVar in case_dVars.
              inversion case_dVars. }
        { rewrite absenv_var in bounds_valid. rewrite not_in_add in bounds_valid.
          auto. }
      * rewrite absenv_var in bounds_valid; simpl in *.
        case_eq (NatSet.mem v dVars);
        intros case_dVars; rewrite case_dVars in *; simpl in *; try auto.
        assert (NatSet.mem v (NatSet.add x dVars) = false) as not_in_add.
        { case_eq (NatSet.mem v (NatSet.add x dVars));
            intros case_add; rewrite case_add in *; simpl in *; try auto.
          - rewrite NatSet.mem_spec in case_add.
            rewrite NatSet.add_spec in case_add.
            destruct case_add as [v_eq_x | v_dVar]; subst.
            + rewrite Nat.eqb_neq in eq_case. exfalso; apply eq_case; auto.
            + rewrite <- NatSet.mem_spec in v_dVar. rewrite v_dVar in case_dVars.
              inversion case_dVars. }
        { rewrite not_in_add in error_valid; auto. }
      * intros v0 mem_dVars.
        rewrite absenv_var in *; simpl in *.
        rewrite NatSet.mem_spec in mem_dVars.
        assert (NatSet.In v0 (NatSet.add x dVars)) as v0_in_add.
        { rewrite NatSet.add_spec. right; auto. }
        { rewrite <- NatSet.mem_spec in v0_in_add.
          specialize (dVars_sound v0 v0_in_add).
          destruct dVars_sound as [vR0 [val_def iv_sound_val]].
          unfold updEnv in val_def; simpl in val_def.
          case_eq (v0 =? x); intros case_mem;
            rewrite case_mem in val_def; simpl in val_def.
          - rewrite Nat.eqb_eq in case_mem; subst.
            apply (NatSetProps.Dec.F.union_3 fVars) in mem_dVars.
            rewrite <- NatSet.mem_spec in mem_dVars.
            rewrite mem_dVars in *; congruence.
          - exists vR0; split; auto. }
      * rewrite absenv_var in bounds_valid.
        intros v0 v0_fVar.
        specialize (P_valid v0 v0_fVar).
        unfold updEnv in P_valid; simpl in *.
        case_eq (v0 =? x); intros case_v0; rewrite case_v0 in *; try auto.
        rewrite Nat.eqb_eq in case_v0; subst.
        assert (NatSet.mem x (NatSet.union fVars dVars) = true)
          as x_in_union
            by (rewrite NatSet.mem_spec, NatSet.union_spec; rewrite NatSet.mem_spec in v0_fVar; auto).
        rewrite x_in_union in *; congruence.
Qed.

Lemma validErrorboundCorrectConstant:
  forall E1 E2 absenv (n:Q) nR nF e nlo nhi dVars,
    eval_exp 0%R E1 (Const (Q2R n)) nR ->
    eval_exp (Q2R (RationalSimps.machineEpsilon)) E2 (Const (Q2R n)) nF ->
    validErrorbound (Const n) absenv dVars = true ->
    (Q2R nlo <= nR <= Q2R nhi)%R ->
    absenv (Const n) = ((nlo,nhi),e) ->
    (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros E1 E2 absenv n nR nF e nlo nhi dVars
         eval_real eval_float error_valid intv_valid absenv_const.
  eapply Rle_trans.
  eapply const_abs_err_bounded; eauto.
  unfold validErrorbound in error_valid.
  rewrite absenv_const in *; simpl in *.
  andb_to_prop error_valid.
  rename R into error_valid.
  inversion eval_real; subst.
  rewrite delta_0_deterministic in *; auto.
  apply Qle_bool_iff in error_valid.
  apply Qle_Rle in error_valid.
  destruct intv_valid.
  eapply Rle_trans.
  - eapply Rmult_le_compat_r.
    apply mEps_geq_zero.
    apply RmaxAbs; eauto.
  - rewrite Q2R_mult in error_valid.
    rewrite <- maxAbs_impl_RmaxAbs in error_valid; auto.
Qed.

Lemma validErrorboundCorrectAddition E1 E2 absenv
      (e1:exp Q) (e2:exp Q) (nR nR1 nR2 nF nF1 nF2 :R) (e err1 err2 :error)
      (alo ahi e1lo e1hi e2lo e2hi:Q) dVars:
  eval_exp 0%R E1 (toRExp e1) nR1 ->
  eval_exp 0%R E1 (toRExp e2) nR2 ->
  eval_exp 0%R E1 (toRExp (Binop Plus e1 e2)) nR ->
  eval_exp (Q2R RationalSimps.machineEpsilon) E2 (toRExp e1) nF1 ->
  eval_exp (Q2R RationalSimps.machineEpsilon) E2 (toRExp e2) nF2 ->
  eval_exp (Q2R RationalSimps.machineEpsilon) (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv)) (toRExp (Binop Plus (Var Q 1) (Var Q 2))) nF ->
  validErrorbound (Binop Plus e1 e2) absenv dVars = true ->
  (Q2R e1lo <= nR1 <= Q2R e1hi)%R ->
  (Q2R e2lo <= nR2 <= Q2R e2hi)%R ->
  absenv e1 = ((e1lo,e1hi),err1) ->
  absenv e2 = ((e2lo, e2hi),err2) ->
  absenv (Binop Plus e1 e2) = ((alo,ahi),e)->
  (Rabs (nR1 - nF1) <= (Q2R err1))%R ->
  (Rabs (nR2 - nF2) <= (Q2R err2))%R ->
  (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros e1_real e2_real eval_real e1_float e2_float eval_float
         valid_error valid_intv1 valid_intv2 absenv_e1 absenv_e2 absenv_add
         err1_bounded err2_bounded.
  eapply Rle_trans.
  eapply
    (add_abs_err_bounded e1 e2);
    try eauto.
  unfold validErrorbound in valid_error at 1.
  rewrite absenv_add, absenv_e1, absenv_e2 in valid_error.
  andb_to_prop valid_error.
  rename R0 into valid_error.
  eapply Rle_trans.
  apply Rplus_le_compat_l.
  eapply Rmult_le_compat_r.
  apply mEps_geq_zero.
  Focus 2.
  rewrite Qle_bool_iff in valid_error.
  apply Qle_Rle in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite Q2R_mult in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- Rabs_eq_Qabs in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- maxAbs_impl_RmaxAbs in valid_error.
  apply valid_error.
  clear L R.
  remember (addIntv (widenIntv (e1lo, e1hi) err1) (widenIntv (e2lo, e2hi) err2)) as iv.
  iv_assert iv iv_unf.
  destruct iv_unf as [ivl [ivh iv_unf]].
  rewrite iv_unf.
  rewrite <- maxAbs_impl_RmaxAbs.
  assert (ivlo iv = ivl) by (rewrite iv_unf; auto).
  assert (ivhi iv = ivh) by (rewrite iv_unf; auto).
  rewrite <- H, <- H0.
  assert (contained nR1 (Q2R e1lo, Q2R e1hi)) as contained_intv1 by auto.
  pose proof (distance_gives_iv (a:=nR1) _ contained_intv1 err1_bounded).
  assert (contained nR2 (Q2R e2lo, Q2R e2hi)) as contained_intv2 by auto.
  pose proof (distance_gives_iv (a:=nR2) _ contained_intv2 err2_bounded).
  pose proof (IntervalArith.interval_addition_valid H1 H2).
  unfold IntervalArith.contained in H3.
  destruct H3.
  subst; simpl in *.
  apply RmaxAbs; simpl.
  - rewrite Q2R_min4.
    repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus; auto.
  - rewrite Q2R_max4.
    repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus; auto.
Qed.

Lemma validErrorboundCorrectSubtraction E1 E2 absenv
      (e1:exp Q) (e2:exp Q) (nR nR1 nR2 nF nF1 nF2 :R) (e err1 err2 :error)
      (alo ahi e1lo e1hi e2lo e2hi:Q) dVars:
  eval_exp 0%R E1 (toRExp e1) nR1 ->
  eval_exp 0%R E1 (toRExp e2) nR2 ->
  eval_exp 0%R E1 (toRExp (Binop Sub e1 e2)) nR ->
  eval_exp (Q2R RationalSimps.machineEpsilon) E2 (toRExp e1) nF1 ->
  eval_exp (Q2R RationalSimps.machineEpsilon) E2 (toRExp e2) nF2 ->
  eval_exp (Q2R RationalSimps.machineEpsilon) (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv)) (toRExp (Binop Sub (Var Q 1) (Var Q 2))) nF ->
  validErrorbound (Binop Sub e1 e2) absenv dVars = true ->
  (Q2R e1lo <= nR1 <= Q2R e1hi)%R ->
  (Q2R e2lo <= nR2 <= Q2R e2hi)%R ->
  absenv e1 = ((e1lo,e1hi),err1) ->
  absenv e2 = ((e2lo, e2hi),err2) ->
  absenv (Binop Sub e1 e2) = ((alo,ahi),e)->
  (Rabs (nR1 - nF1) <= (Q2R err1))%R ->
  (Rabs (nR2 - nF2) <= (Q2R err2))%R ->
  (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros e1_real e2_real eval_real e1_float e2_float eval_float
         valid_error valid_intv1 valid_intv2 absenv_e1 absenv_e2 absenv_sub
         err1_bounded err2_bounded.
  eapply Rle_trans.
  eapply subtract_abs_err_bounded.
  apply e1_real.
  apply e1_float.
  apply e2_real.
  apply e2_float.
  apply eval_real.
  apply eval_float.
  apply err1_bounded.
  apply err2_bounded.
  unfold validErrorbound in valid_error at 1.
  rewrite absenv_sub, absenv_e1, absenv_e2 in valid_error.
  andb_to_prop valid_error.
  rename R0 into valid_error.
  apply Qle_bool_iff in valid_error.
  apply Qle_Rle in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite Q2R_mult in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- Rabs_eq_Qabs in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- maxAbs_impl_RmaxAbs in valid_error.
  eapply Rle_trans.
  apply Rplus_le_compat_l.
  eapply Rmult_le_compat_r.
  apply mEps_geq_zero.
  Focus 2.
  apply valid_error.
  remember (subtractIntv (widenIntv (e1lo, e1hi) err1) (widenIntv (e2lo, e2hi) err2)) as iv.
  iv_assert iv iv_unf.
  destruct iv_unf as [ivl [ivh iv_unf]].
  rewrite iv_unf.
  rewrite <- maxAbs_impl_RmaxAbs.
  assert (ivlo iv = ivl) by (rewrite iv_unf; auto).
  assert (ivhi iv = ivh) by (rewrite iv_unf; auto).
  rewrite <- H, <- H0.
    assert (contained nR1 (Q2R e1lo, Q2R e1hi)) as contained_intv1 by auto.
  pose proof (distance_gives_iv (a:=nR1) _ contained_intv1 err1_bounded).
  assert (contained nR2 (Q2R e2lo, Q2R e2hi)) as contained_intv2 by auto.
  pose proof (distance_gives_iv (a:=nR2) _ contained_intv2 err2_bounded).
  pose proof (IntervalArith.interval_subtraction_valid H1 H2).
  unfold IntervalArith.contained in H3.
  destruct H3.
  subst; simpl in *.
  apply RmaxAbs; simpl.
  - rewrite Q2R_min4.
    repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus;
      repeat rewrite Q2R_opp;
      repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus; auto.
  - rewrite Q2R_max4.
    repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus;
      repeat rewrite Q2R_opp;
      repeat rewrite Q2R_plus;
      repeat rewrite Q2R_minus; auto.
Qed.

Lemma validErrorboundCorrectMult E1 E2 absenv
      (e1:exp Q) (e2:exp Q) (nR nR1 nR2 nF nF1 nF2 :R) (e err1 err2 :error)
      (alo ahi e1lo e1hi e2lo e2hi:Q) dVars:
  eval_exp 0%R E1 (toRExp e1) nR1 ->
  eval_exp 0%R E1 (toRExp e2) nR2 ->
  eval_exp 0%R E1 (toRExp (Binop Mult e1 e2)) nR ->
  eval_exp (Q2R RationalSimps.machineEpsilon) E2 (toRExp e1) nF1 ->
  eval_exp (Q2R RationalSimps.machineEpsilon) E2 (toRExp e2) nF2 ->
  eval_exp (Q2R RationalSimps.machineEpsilon) (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv)) (toRExp (Binop Mult (Var Q 1) (Var Q 2))) nF ->
  validErrorbound (Binop Mult e1 e2) absenv dVars = true ->
  (Q2R e1lo <= nR1 <= Q2R e1hi)%R ->
  (Q2R e2lo <= nR2 <= Q2R e2hi)%R ->
  absenv e1 = ((e1lo,e1hi),err1) ->
  absenv e2 = ((e2lo, e2hi),err2) ->
  absenv (Binop Mult e1 e2) = ((alo,ahi),e)->
  (Rabs (nR1 - nF1) <= (Q2R err1))%R ->
  (Rabs (nR2 - nF2) <= (Q2R err2))%R ->
  (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros e1_real e2_real eval_real e1_float e2_float eval_float
         valid_error valid_e1 valid_e2 absenv_e1 absenv_e2 absenv_mult
         err1_bounded err2_bounded.
  eapply Rle_trans.
  eapply (mult_abs_err_bounded e1 e2); eauto.
  unfold validErrorbound in valid_error at 1.
  rewrite absenv_mult, absenv_e1, absenv_e2 in valid_error.
  andb_to_prop valid_error.
  rename R0 into valid_error.
  assert (0 <= Q2R err1)%R as err1_pos by (eapply (err_always_positive e1 absenv dVars); eauto).
  assert (0 <= Q2R err2)%R as err2_pos by (eapply (err_always_positive e2 absenv dVars); eauto).
  clear R L1.
  apply Qle_bool_iff in valid_error.
  apply Qle_Rle in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite Q2R_mult in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- Rabs_eq_Qabs in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite <- maxAbs_impl_RmaxAbs in valid_error.
  eapply Rle_trans.
  Focus 2.
  apply valid_error.
  apply Rplus_le_compat.
  - unfold Rabs in err1_bounded.
    unfold Rabs in err2_bounded.
    (* Before doing case distinction, prove bounds that will be used many times: *)
    assert (nR1 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi))%R
      as upperBound_nR1
        by (apply contained_leq_maxAbs_val; unfold contained; auto).
    assert (nR2 <= RmaxAbsFun (Q2R e2lo, Q2R e2hi))%R
      as upperBound_nR2
        by (apply contained_leq_maxAbs_val; unfold contained; auto).
    assert (-nR1 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi))%R
      as upperBound_Ropp_nR1
        by (apply contained_leq_maxAbs_neg_val; unfold contained; auto).
    assert (- nR2 <= RmaxAbsFun (Q2R e2lo, Q2R e2hi))%R
      as upperBound_Ropp_nR2
        by (apply contained_leq_maxAbs_neg_val; unfold contained; auto).
    assert (nR1 * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
      as bound_nR1 by (apply Rmult_le_compat_r; auto).
    assert (- nR1 * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
      as bound_neg_nR1 by (apply Rmult_le_compat_r; auto).
    assert (nR2 * Q2R err1 <= RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1)%R
      as bound_nR2 by (apply Rmult_le_compat_r; auto).
    assert (- nR2 * Q2R err1 <= RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1)%R
      as bound_neg_nR2 by (apply Rmult_le_compat_r; auto).
    assert (- (Q2R err1 * Q2R err2) <= Q2R err1 * Q2R err2)%R as err_neg_bound
        by  (rewrite Ropp_mult_distr_l; apply Rmult_le_compat_r; lra).
    assert (0 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
      as zero_up_nR1 by lra.
    assert (RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2 + RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1)%R
      as nR1_to_sum by lra.
    assert (RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2 + RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1 <=  RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2 + RmaxAbsFun (Q2R e2lo, Q2R e2hi) * Q2R err1 + Q2R err1 * Q2R err2)%R
      as sum_to_errsum by lra.
    clear e1_real e1_float e2_real e2_float eval_real eval_float valid_error
      absenv_e1 absenv_e2.
    (* Large case distinction for
         a) different cases of the value of Rabs (...) and
         b) wether arguments of multiplication in (nf1 * nF2) are < or >= 0 *)
    destruct Rcase_abs in err1_bounded; destruct Rcase_abs in err2_bounded.
    + rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
      rewrite Ropp_plus_distr in err1_bounded, err2_bounded.
      rewrite Ropp_involutive in err1_bounded, err2_bounded.
      assert (nF1 <= Q2R err1 + nR1)%R by lra.
      assert (nF2 <= Q2R err2 + nR2)%R by lra.
      unfold Rabs.
      destruct Rcase_abs.
      * rewrite Rsub_eq_Ropp_Rplus. rewrite Ropp_plus_distr.
        rewrite Ropp_involutive.
        destruct (Rle_lt_dec 0 nF1).
        { (* Upper Bound ... *)
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          apply H0.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l. hnf. left; auto.
            assert (nR1 <= nF1)%R by lra.
            apply H1.
            lra.
        }
        {
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_neg_l.
          hnf. left; auto.
          assert (nR2 < nF2)%R by lra.
          apply Rlt_le in H1; apply H1.
          destruct (Rle_lt_dec 0 nR2).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (nR1 < nF1)%R by lra.
            apply Rlt_le in H1; apply H1.
            lra.
        }
      * rewrite Rsub_eq_Ropp_Rplus.
        destruct (Rle_lt_dec 0 nF1).
        {
          rewrite Ropp_mult_distr_r.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          assert (- nF2 <= - nR2)%R by lra.
          apply H1.
          destruct (Rle_lt_dec 0 (- nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (nR1 < nF1)%R by lra.
            apply Rlt_le in H1; apply H1.
            lra.
        }
        {
          rewrite Ropp_mult_distr_l.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l.
          rewrite <- (Ropp_involutive 0).
          apply Ropp_ge_le_contravar.
          apply Rle_ge.
          rewrite Ropp_0.
          hnf. left; auto.
          apply H0.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            assert (- nF1 <= -nR1)%R by lra.
            apply H1.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            apply Ropp_le_ge_contravar in H.
            apply Rge_le in H.
            apply H.
            lra.
        }
    + rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
      rewrite Ropp_plus_distr in err1_bounded.
      rewrite Ropp_involutive in err1_bounded.
      assert (nF1 <= Q2R err1 + nR1)%R by lra.
      assert (nF2 <= Q2R err2 + nR2)%R by lra.
      unfold Rabs.
      destruct Rcase_abs.
      * rewrite Rsub_eq_Ropp_Rplus. rewrite Ropp_plus_distr.
        rewrite Ropp_involutive.
        destruct (Rle_lt_dec 0 nF1).
        { (* Upper Bound ... *)
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          apply H0.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l. hnf. left; auto.
            assert (nR1 <= nF1)%R by lra.
            apply H1.
            lra.
        }
        {
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_neg_l.
          hnf. left; auto.
          assert (- nF2 <= - (nR2 - Q2R err2))%R by lra.
          apply Ropp_le_ge_contravar in H1.
          repeat rewrite Ropp_involutive in H1.
          apply Rge_le in H1.
          apply H1.
          destruct (Rle_lt_dec 0 (nR2 - Q2R err2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (nR1 < nF1)%R by lra.
            apply Rlt_le in H1; apply H1.
            lra.
        }
      * rewrite Rsub_eq_Ropp_Rplus.
        destruct (Rle_lt_dec 0 nF1).
        {
          rewrite Ropp_mult_distr_r.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          assert (- nF2 <= - nR2 + Q2R err2)%R by lra.
          apply H1.
          destruct (Rle_lt_dec 0 (- nR2 + Q2R err2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (nR1 < nF1)%R by lra.
            apply Rlt_le in H1; apply H1.
            lra.
        }
        {
          rewrite Ropp_mult_distr_l.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l.
          rewrite <- (Ropp_involutive 0).
          apply Ropp_ge_le_contravar.
          apply Rle_ge.
          rewrite Ropp_0.
          hnf. left; auto.
          apply H0.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            assert (- nF1 <= -nR1)%R by lra.
            apply H1.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            apply Ropp_le_ge_contravar in H.
            apply Rge_le in H.
            apply H.
            lra.
        }
    + rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
      rewrite Ropp_plus_distr in err2_bounded.
      rewrite Ropp_involutive in err2_bounded.
      assert (nF1 <= Q2R err1 + nR1)%R by lra.
      assert (nF2 <= Q2R err2 + nR2)%R by lra.
      unfold Rabs.
      destruct Rcase_abs.
      * rewrite Rsub_eq_Ropp_Rplus. rewrite Ropp_plus_distr.
        rewrite Ropp_involutive.
        destruct (Rle_lt_dec 0 nF1).
        { (* Upper Bound ... *)
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          apply H0.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l. hnf. left; auto.
            assert (- nF1 <= - (nR1 - Q2R err1))%R by lra.
            apply Ropp_le_ge_contravar in H1.
            repeat rewrite Ropp_involutive in H1.
            apply Rge_le in H1.
            apply H1.
            lra.
        }
        {
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_neg_l.
          hnf. left; auto.
          assert (nR2 < nF2)%R by lra.
          apply Rlt_le in H1; apply H1.
          destruct (Rle_lt_dec 0 nR2).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (- nF1 <= - (nR1 - Q2R err1))%R by lra.
            apply Ropp_le_ge_contravar in H1.
            repeat rewrite Ropp_involutive in H1.
            apply Rge_le in H1.
            apply H1.
            lra.
        }
      * rewrite Rsub_eq_Ropp_Rplus.
        destruct (Rle_lt_dec 0 nF1).
        {
          rewrite Ropp_mult_distr_r.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          assert (- nF2 <= - nR2)%R by lra.
          apply H1.
          destruct (Rle_lt_dec 0 (- nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            hnf. left; auto.
            assert (- nF1 <= - (nR1 - Q2R err1))%R by lra.
            apply Ropp_le_ge_contravar in H1.
            repeat rewrite Ropp_involutive in H1.
            apply Rge_le in H1.
            apply H1.
            lra.
        }
        {
          rewrite Ropp_mult_distr_l.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l.
          lra.
          apply H0.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            assert (- nF1 <= - (nR1 - Q2R err1))%R by lra.
            apply H1.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l; try lra.
            apply Ropp_le_ge_contravar in H.
            apply Rge_le in H.
            apply H.
            lra.
        }
    (* All positive *)
    + assert (nF1 <= Q2R err1 + nR1)%R by lra.
      assert (nF2 <= Q2R err2 + nR2)%R by lra.
      unfold Rabs.
      destruct Rcase_abs.
      * rewrite Rsub_eq_Ropp_Rplus. rewrite Ropp_plus_distr.
        rewrite Ropp_involutive.
        destruct (Rle_lt_dec 0 nF1).
        { (* Upper Bound ... *)
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          apply H0.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l. hnf. left; auto.
            assert (nR1 - Q2R err1 <= nF1)%R by lra.
            apply H1.
            lra.
        }
        {
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_neg_l.
          hnf. left; auto.
          assert (nR2 - Q2R err2 <= nF2)%R by lra.
          apply H1.
          destruct (Rle_lt_dec 0 (nR2 - Q2R err2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            lra.
            assert (nR1 - Q2R err1 <= nF1)%R by lra.
            apply H1.
            lra.
        }
      * rewrite Rsub_eq_Ropp_Rplus.
        destruct (Rle_lt_dec 0 nF1).
        {
          rewrite Ropp_mult_distr_r.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l; auto.
          assert (- nF2 <= Q2R err2 - nR2)%R by lra.
          apply H1.
          destruct (Rle_lt_dec 0 (Q2R err2 - nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            apply H.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            lra.
            assert (nR1 - Q2R err1 <= nF1)%R by lra.
            apply H1.
            lra.
        }
        {
          rewrite Ropp_mult_distr_l.
          eapply Rle_trans.
          eapply Rplus_le_compat_l.
          eapply Rmult_le_compat_l.
          rewrite <- (Ropp_involutive 0).
          apply Ropp_ge_le_contravar.
          apply Rle_ge.
          rewrite Ropp_0.
          lra.
          apply H0.
          destruct (Rle_lt_dec 0 (Q2R err2 + nR2)).
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            eapply Rmult_le_compat_r; auto.
            assert (- nF1 <= Q2R err1 - nR1)%R by lra.
            apply H1.
            lra.
          - eapply Rle_trans.
            eapply Rplus_le_compat_l.
            rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l.
            lra.
            apply Ropp_le_ge_contravar in H.
            apply Rge_le in H.
            apply H.
            lra.
        }
  - remember (multIntv (widenIntv (e1lo, e1hi) err1) (widenIntv (e2lo, e2hi) err2)) as iv.
    iv_assert iv iv_unf.
    destruct iv_unf as [ivl [ivh iv_unf]].
    rewrite iv_unf.
    rewrite <- maxAbs_impl_RmaxAbs.
    assert (ivlo iv = ivl) by (rewrite iv_unf; auto).
    assert (ivhi iv = ivh) by (rewrite iv_unf; auto).
    rewrite <- H, <- H0.
    assert (contained nR1 (Q2R e1lo, Q2R e1hi)) as contained_intv1 by auto.
    pose proof (distance_gives_iv (a:=nR1) _ contained_intv1 err1_bounded).
    assert (contained nR2 (Q2R e2lo, Q2R e2hi)) as contained_intv2 by auto.
    pose proof (distance_gives_iv (a:=nR2) _ contained_intv2 err2_bounded).
    pose proof (IntervalArith.interval_multiplication_valid H1 H2).
    unfold IntervalArith.contained in H3.
    destruct H3.
    unfold RmaxAbsFun.
    apply Rmult_le_compat_r.
    apply mEps_geq_zero.
    apply RmaxAbs; subst; simpl in *.
    + rewrite Q2R_min4.
      repeat rewrite Q2R_mult;
        repeat rewrite Q2R_minus;
        repeat rewrite Q2R_plus; auto.
    + rewrite Q2R_max4.
      repeat rewrite Q2R_mult;
        repeat rewrite Q2R_minus;
        repeat rewrite Q2R_plus; auto.
Qed.

Lemma validErrorboundCorrectDiv E1 E2 absenv
      (e1:exp Q) (e2:exp Q) (nR nR1 nR2 nF nF1 nF2 :R) (e err1 err2 :error)
      (alo ahi e1lo e1hi e2lo e2hi:Q) dVars:
  eval_exp 0%R E1 (toRExp e1) nR1 ->
  eval_exp 0%R E1 (toRExp e2) nR2 ->
  eval_exp 0%R E1 (toRExp (Binop Div e1 e2)) nR ->
  eval_exp (Q2R RationalSimps.machineEpsilon) E2 (toRExp e1) nF1 ->
  eval_exp (Q2R RationalSimps.machineEpsilon) E2 (toRExp e2) nF2 ->
  eval_exp (Q2R RationalSimps.machineEpsilon) (updEnv 2 nF2 (updEnv 1 nF1 emptyEnv)) (toRExp (Binop Div (Var Q 1) (Var Q 2))) nF ->
  validErrorbound (Binop Div e1 e2) absenv dVars = true ->
  (Q2R e1lo <= nR1 <= Q2R e1hi)%R ->
  (Q2R e2lo <= nR2 <= Q2R e2hi)%R ->
  (Qleb e2hi 0 && negb (Qeq_bool e2hi 0) || Qleb 0 e2lo && negb (Qeq_bool e2lo 0) = true) ->
  absenv e1 = ((e1lo,e1hi),err1) ->
  absenv e2 = ((e2lo, e2hi),err2) ->
  absenv (Binop Div e1 e2) = ((alo,ahi),e)->
  (Rabs (nR1 - nF1) <= (Q2R err1))%R ->
  (Rabs (nR2 - nF2) <= (Q2R err2))%R ->
  (Rabs (nR - nF) <= (Q2R e))%R.
Proof.
  intros e1_real e2_real eval_real e1_float e2_float eval_float
         valid_error valid_bounds_e1 valid_bounds_e2 no_div_zero_real absenv_e1
         absenv_e2 absenv_div err1_bounded err2_bounded.
  eapply Rle_trans.
  eapply (div_abs_err_bounded e1 e2); eauto.
  unfold validErrorbound in valid_error at 1.
  rewrite absenv_div, absenv_e1, absenv_e2 in valid_error.
  andb_to_prop valid_error.
  assert (validErrorbound e1 absenv dVars = true) as valid_err_e1 by auto;
    assert (validErrorbound e2 absenv dVars = true) as valid_err_e2 by auto.
  clear L1 R.
  rename R1 into valid_error.
  rename L0 into no_div_zero_float.
  assert (contained nR1 (Q2R e1lo, Q2R e1hi)) as contained_intv1 by auto.
  pose proof (distance_gives_iv (a:=nR1) _ contained_intv1 err1_bounded).
  assert (contained nR2 (Q2R e2lo, Q2R e2hi)) as contained_intv2 by auto.
  pose proof (distance_gives_iv (a:=nR2) _ contained_intv2 err2_bounded).
  assert (0 <= Q2R err1)%R as err1_pos by (eapply (err_always_positive e1 absenv); eauto).
  assert (0 <= Q2R err2)%R as err2_pos by (eapply (err_always_positive e2 absenv); eauto).
  apply Qle_bool_iff in valid_error.
  apply Qle_Rle in valid_error.
  repeat rewrite Q2R_plus in valid_error.
  repeat rewrite Q2R_mult in valid_error.
  rewrite Q2R_div in valid_error.
  - rewrite Q2R_mult in valid_error.
    repeat rewrite <- maxAbs_impl_RmaxAbs in valid_error.
    rewrite <- maxAbs_impl_RmaxAbs_iv in valid_error.
    repeat rewrite <- minAbs_impl_RminAbs_iv in valid_error.
    apply le_neq_bool_to_lt_prop in no_div_zero_float; apply le_neq_bool_to_lt_prop in no_div_zero_real.
    assert ((IVhi (Q2R e2lo,Q2R e2hi) < 0)%R \/ (0 < IVlo (Q2R e2lo,Q2R e2hi))%R) as no_div_zero_real_R
          by (simpl; rewrite <- Q2R0_is_0; simpl in no_div_zero_real;
              destruct no_div_zero_real as [ndiv | ndiv]; apply Qlt_Rlt in ndiv; auto).
    apply Q_case_div_to_R_case_div in no_div_zero_float; apply Q_case_div_to_R_case_div in no_div_zero_real.
    assert (Q2R e2lo = 0 -> False)%R by (apply (lt_or_gt_neq_zero_lo _ (Q2R e2hi)); try auto; lra).
    assert (Q2R e2hi = 0 -> False)%R by (apply (lt_or_gt_neq_zero_hi (Q2R e2lo)); try auto; lra).
    eapply Rle_trans.
    Focus 2.
    apply valid_error.
    apply Rplus_le_compat.
    (* Error Propagation proof *)
    + clear absenv_e1 absenv_e2 valid_error eval_float eval_real e1_float
            e1_real e2_float e2_real absenv_div
            valid_err_e1 valid_err_e2 E1 E2 absenv alo ahi nR nF e1 e2 e L.
      unfold IntervalArith.contained, widenInterval in *.
      simpl in *.
      rewrite Q2R_plus, Q2R_minus in no_div_zero_float.
      assert (Q2R e2lo - Q2R err2 = 0 -> False)%R by (apply (lt_or_gt_neq_zero_lo _ (Q2R e2hi + Q2R err2)); try auto; lra).
      assert (Q2R e2hi + Q2R err2 = 0 -> False)%R by (apply (lt_or_gt_neq_zero_hi (Q2R e2lo - Q2R err2)); try auto; lra).
      pose proof (interval_inversion_valid (iv:=(Q2R e2lo,Q2R e2hi)) no_div_zero_real_R valid_bounds_e2) as valid_bounds_inv_e2.
      clear no_div_zero_real_R.
      (* Before doing case distinction, prove bounds that will be used many times: *)
      assert (nR1 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi))%R
        as upperBound_nR1
          by (apply contained_leq_maxAbs_val; unfold contained; auto).
      assert (/ nR2 <= RmaxAbsFun (invertInterval (Q2R e2lo, Q2R e2hi)))%R
        as upperBound_nR2
          by (apply contained_leq_maxAbs_val; unfold contained; auto).
      assert (-nR1 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi))%R
        as upperBound_Ropp_nR1
          by (apply contained_leq_maxAbs_neg_val; unfold contained; auto).
      assert (- / nR2 <= RmaxAbsFun (invertInterval (Q2R e2lo, Q2R e2hi)))%R
        as upperBound_Ropp_nR2
          by (apply contained_leq_maxAbs_neg_val; unfold contained; auto).
      assert (nR1 * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
        as bound_nR1
          by (apply Rmult_le_compat_r; auto).
      assert (- nR1 * Q2R err2 <= RmaxAbsFun (Q2R e1lo, Q2R e1hi) * Q2R err2)%R
        as bound_neg_nR1
          by (apply Rmult_le_compat_r; auto).
      unfold invertInterval in *; simpl in upperBound_nR2, upperBound_Ropp_nR2.
      (* Case distinction for the divisor range
	 positive or negative in float and real valued execution *)
      destruct no_div_zero_real as [ real_iv_neg | real_iv_pos];
        destruct no_div_zero_float as [float_iv_neg | float_iv_pos].
      (* The range of the divisor lies in the range from -infinity until 0 *)
      * unfold widenIntv in *; unfold IntervalArith.contained in *; simpl in *.
        rewrite <- Q2R_plus in float_iv_neg.
        rewrite <- Q2R0_is_0 in float_iv_neg.
        rewrite <- Q2R0_is_0 in real_iv_neg.
        pose proof (err_prop_inversion_neg float_iv_neg real_iv_neg err2_bounded valid_bounds_e2 H0 err2_pos) as err_prop_inv.
        rewrite Q2R_plus in float_iv_neg.
        rewrite Q2R0_is_0 in float_iv_neg.
        rewrite Q2R0_is_0 in real_iv_neg.
        rewrite Q2R_minus, Q2R_plus.
        repeat rewrite minAbs_negative_iv_is_hi; try lra.
        unfold Rdiv.
        repeat rewrite Q2R1.
        rewrite Rmult_1_l.
        (* Prove inequality to 0 in Q *)
        assert (e2lo == 0 -> False)
               by (rewrite <- Q2R0_is_0 in real_iv_neg; apply Rlt_Qlt in real_iv_neg;
                   assert (e2lo < 0) by (apply (Qle_lt_trans _ e2hi); try auto; apply Rle_Qle; lra);
                   lra).
        assert (e2hi == 0 -> False)
          by (rewrite <- Q2R0_is_0 in real_iv_neg; apply Rlt_Qlt in real_iv_neg; lra).
        rewrite Rabs_case_inverted in err1_bounded, err2_bounded.
        assert (nF1 <= Q2R err1 + nR1)%R as ub_nF1 by lra.
        assert (nR1 - Q2R err1 <= nF1)%R as lb_nF1 by lra.
        destruct err2_bounded as [[nR2_sub_pos err2_bounded] | [nR2_sub_neg err2_bounded]].
        (* Positive case for abs (nR2 - nF2) <= err2 *)
        { rewrite <- Ropp_mult_distr_l, <- Ropp_mult_distr_r, Ropp_involutive.
          apply Rgt_lt in nR2_sub_pos.
          assert (nF2 < nR2)%R as nF2_le_nR2 by lra.
          apply Ropp_lt_contravar in nF2_le_nR2.
          apply Rinv_lt_contravar in nF2_le_nR2; [ | apply Rmult_0_lt_preserving; try lra].
          repeat (rewrite <- Ropp_inv_permute in nF2_le_nR2; try lra).
          apply Ropp_lt_contravar in nF2_le_nR2.
          repeat rewrite Ropp_involutive in nF2_le_nR2.
          assert (/ nR2 - / nF2 < 0)%R as abs_inv_neg by lra.
          rewrite Rabs_left in err_prop_inv; try lra.
          rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded, err_prop_inv.
          assert (/nF2 <= /nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R as error_prop_inv_up by lra.
          assert (/nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) <= /nF2)%R as error_prop_inv_down by lra.
          (* Next do a case distinction for the absolute value*)
          unfold Rabs.
          destruct Rcase_abs.
          (* Case 1: Absolute value negative *)
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr.
            rewrite Ropp_involutive.
            (* To upper bound terms, do a case distinction for nF1 *)
            destruct (Rle_lt_dec 0 nF1).
            (* 0 <= nF1 *)
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_l; try lra.
              apply error_prop_inv_up.
              destruct (Rle_lt_dec 0 (/ nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                apply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 + err_inv) = nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                apply Rmult_le_compat_r; try lra.
                repeat (rewrite Q2R_inv; try auto).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                apply Rmult_le_compat_neg_l; try lra.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (/ nR2 + err_inv) * (nR1 - Q2R err1) =
                          nR1 * err_inv - / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                repeat rewrite Rsub_eq_Ropp_Rplus.
                apply Rplus_le_compat.
                { apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                  rewrite Ropp_mult_distr_l.
                  apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
            (* nF1 < 0 *)
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 - err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (/ nR2 * nR1) + (/ nR2 - err_inv) * (nR1 - Q2R err1) = - nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
          (* Case 2: Absolute value positive *)
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_mult_distr_l.
            destruct (Rle_lt_dec 0 (- nF1)).
            (* 0 <= - nF1 *)
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_l; try lra.
              apply error_prop_inv_up.
              destruct (Rle_lt_dec 0 (/ nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                apply Rmult_le_compat_r; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + - (nR1 - Q2R err1) * (/ nR2 + err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                apply Rmult_le_compat_r; try lra.
                repeat (rewrite Q2R_inv; try auto).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                apply Rmult_le_compat_neg_l; try lra.
                apply Ropp_le_contravar.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 + err_inv) *  - (Q2R err1 + nR1) =  - nR1 * err_inv + - / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                repeat rewrite Rsub_eq_Ropp_Rplus.
                apply Rplus_le_compat.
                { apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                  apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
            (* - nF1 <= 0 *)
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_l; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (nR1 - Q2R err1) = nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply Ropp_le_contravar.
                apply ub_nF1.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (Q2R err1 + nR1) = nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
        }
        { rewrite <- Ropp_mult_distr_l, <- Ropp_mult_distr_r, Ropp_involutive.
          assert (nR2 <= nF2)%R as nR2_le_nF2 by lra.
          apply Ropp_le_contravar in nR2_le_nF2.
          apply Rinv_le_contravar in nR2_le_nF2; [ | lra].
          repeat (rewrite <- Ropp_inv_permute in nR2_le_nF2; try lra).
          apply Ropp_le_contravar in nR2_le_nF2.
          repeat rewrite Ropp_involutive in nR2_le_nF2.
          assert (0 <= / nR2 - / nF2)%R as abs_inv_pos by lra.
          rewrite Rabs_right in err_prop_inv; try lra.
          rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded, err_prop_inv.
          assert (/nF2 <= /nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R as error_prop_inv_up by lra.
          assert (/nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) <= /nF2)%R as error_prop_inv_down by lra.
          (* Next do a case distinction for the absolute value*)
          unfold Rabs.
          destruct Rcase_abs.
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr.
            rewrite Ropp_involutive.
            (* To upper bound terms, do a case distinction for nF1 *)
            destruct (Rle_lt_dec 0 nF1).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_l; try lra.
              apply error_prop_inv_up.
              destruct (Rle_lt_dec 0 (/ nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                apply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 + err_inv) = nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                apply Rmult_le_compat_r; try lra.
                repeat (rewrite Q2R_inv; try auto).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                apply Rmult_le_compat_neg_l; try lra.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (/ nR2 + err_inv) * (nR1 - Q2R err1) = nR1 * err_inv - / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                repeat rewrite Rsub_eq_Ropp_Rplus.
                apply Rplus_le_compat.
                { apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                  rewrite Ropp_mult_distr_l.
                  apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 - err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (/ nR2 * nR1) + (/ nR2 - err_inv) * (nR1 - Q2R err1) = - nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
          - rewrite Rsub_eq_Ropp_Rplus.
            rewrite Ropp_mult_distr_l.
            destruct (Rle_lt_dec 0 (- nF1)).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_l; try lra.
              apply error_prop_inv_up.
              destruct (Rle_lt_dec 0 (/ nR2 + Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                apply Rmult_le_compat_r; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + - (nR1 - Q2R err1) * (/ nR2 + err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                apply Rmult_le_compat_r; try lra.
                repeat (rewrite Q2R_inv; try auto).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                apply Rmult_le_compat_neg_l; try lra.
                apply Ropp_le_contravar.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2) _).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 + err_inv) *  - (Q2R err1 + nR1) =  - nR1 * err_inv + - / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                repeat rewrite Rsub_eq_Ropp_Rplus.
                apply Rplus_le_compat.
                { apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
                  apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_l; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (nR1 - Q2R err1) = nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_lt_0_inverting; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply Ropp_le_contravar.
                apply ub_nF1.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2hi + Q2R err2) * (Q2R e2hi + Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (Q2R err1 + nR1) = nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
        }
      * unfold widenIntv in *; unfold IntervalArith.contained in *; simpl in *.
        exfalso.
        assert (Q2R e2lo - Q2R err2 <= Q2R e2hi)%R by lra.
        assert (Q2R e2hi < Q2R e2lo - Q2R err2)%R by (apply (Rlt_trans _ 0 _); auto).
        lra.
      * unfold widenIntv in *; unfold IntervalArith.contained in *; simpl in *.
        exfalso.
        assert (Q2R e2lo <= Q2R e2hi + Q2R err2)%R by lra.
        assert (Q2R e2hi + Q2R err2 < Q2R e2lo)%R as hierr_lt_lo by (apply (Rlt_trans _ 0 _); auto).
        apply Rlt_not_le in hierr_lt_lo.
        apply hierr_lt_lo; auto.
      * (** FIXME: Get rid of rewrites by fixing Lemma **)
        rewrite <- Q2R_minus in float_iv_pos.
        rewrite <- Q2R0_is_0 in float_iv_pos.
        rewrite <- Q2R0_is_0 in real_iv_pos.
        pose proof (err_prop_inversion_pos float_iv_pos real_iv_pos err2_bounded valid_bounds_e2 H0 err2_pos) as err_prop_inv.
        rewrite Q2R_minus in float_iv_pos.
        rewrite Q2R0_is_0 in float_iv_pos.
        rewrite Q2R0_is_0 in real_iv_pos.
        rewrite Q2R_minus, Q2R_plus.
        repeat rewrite minAbs_positive_iv_is_lo; try lra.
        unfold Rdiv.
        repeat rewrite Q2R1.
        rewrite Rmult_1_l.
        (* Prove inequality to 0 in Q *)
        assert (e2lo == 0 -> False)
          by (rewrite <- Q2R0_is_0 in real_iv_pos; apply Rlt_Qlt in real_iv_pos; lra).
        assert (e2hi == 0 -> False)
          by (rewrite <- Q2R0_is_0 in real_iv_pos; apply Rlt_Qlt in real_iv_pos;
              assert (0 < e2hi) by (apply (Qlt_le_trans _ e2lo); try auto; apply Rle_Qle; lra);
              lra).
        rewrite Rabs_case_inverted in err1_bounded, err2_bounded.
        assert (nF1 <= Q2R err1 + nR1)%R as ub_nF1 by lra.
        assert (nR1 - Q2R err1 <= nF1)%R as lb_nF1 by lra.
        destruct err2_bounded as [[nR2_sub_pos err2_bounded] | [nR2_sub_neg err2_bounded]].
        { apply Rgt_lt in nR2_sub_pos.
          assert (nF2 < nR2)%R as nF2_le_nR2 by lra.
          apply Rinv_lt_contravar in nF2_le_nR2; [ | apply Rmult_0_lt_preserving; try lra].
          assert (/ nR2 - / nF2 <= 0)%R as abs_inv_neg by lra.
          rewrite Rabs_left in err_prop_inv; try lra.
          rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
          assert (/nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) <= /nF2)%R as error_prop_inv_down by lra.
          assert (/nF2 <= /nR2 + Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R as error_prop_inv_up by lra.
          (* Next do a case distinction for the absolute value*)
          unfold Rabs.
          destruct Rcase_abs.
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr.
            rewrite Ropp_involutive.
            (* To upper bound terms, do a case distinction for nF1 *)
            destruct (Rle_lt_dec 0 nF1).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat; try lra.
              hnf. left.
              apply Rinv_0_lt_compat; try lra.
              apply ub_nF1.
              apply error_prop_inv_up.
              rewrite (Rmult_comm (Q2R err2) _).
              remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
              assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 + err_inv) = nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
              rewrite simpl_properr.
              apply Rplus_le_compat_r.
              apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
              apply Rmult_le_compat; try lra.
              * hnf; left. apply Rinv_0_lt_compat; lra.
              * repeat (rewrite Q2R_inv; try auto).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 - err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_0_lt_preserving; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (/ nR2 * nR1) + (/ nR2 - err_inv) * (nR1 - Q2R err1) = - nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
          - rewrite Rsub_eq_Ropp_Rplus.
            rewrite Ropp_mult_distr_l.
            destruct (Rle_lt_dec 0 (- nF1)).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat; try lra.
              hnf; left.
              apply Rinv_0_lt_compat.
              apply (Rlt_le_trans _ (Q2R e2lo - Q2R err2)); try lra.
              apply Ropp_le_contravar.
              apply lb_nF1.
              apply error_prop_inv_up.
              rewrite (Rmult_comm (Q2R err2) _).
              remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
              assert ((nR1 * / nR2) + - (nR1 - Q2R err1) * (/ nR2 + err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                as simpl_properr by lra.
              rewrite simpl_properr.
              apply Rplus_le_compat_r.
              apply Rplus_le_compat.
              * apply Rmult_le_compat_r; try lra.
              * apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (/ nR2 * nR1 + - (nR1 - Q2R err1) * (/ nR2 - err_inv) = nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_0_lt_preserving; try lra).
                          lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply Ropp_le_contravar.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (Q2R err1 + nR1) = nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
        }
        {
          apply Rminus_le in nR2_sub_neg.
          apply Rinv_le_contravar in nR2_sub_neg; [ | lra].
          assert (0 <= / nR2 - / nF2)%R as abs_inv_pos by lra.
          rewrite Rabs_right in err_prop_inv; try lra.
          rewrite Rsub_eq_Ropp_Rplus in err1_bounded, err2_bounded.
          rewrite Ropp_plus_distr in err2_bounded.
          rewrite Ropp_involutive in err2_bounded.
          assert (/nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) <= /nF2)%R as error_prop_inv_down by lra.
          assert (/nF2 <= /nR2 + Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R as error_prop_inv_up by lra.
          (* Next do a case distinction for the absolute value*)
          unfold Rabs.
          destruct Rcase_abs.
          - rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr.
            rewrite Ropp_involutive.
            (* To upper bound terms, do a case distinction for nF1 *)
            destruct (Rle_lt_dec 0 nF1).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat; try lra.
              hnf. left.
              apply Rinv_0_lt_compat; try lra.
              apply ub_nF1.
              apply error_prop_inv_up.
              rewrite (Rmult_comm (Q2R err2) _).
              remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
              assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 + err_inv) = nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
              rewrite simpl_properr.
              apply Rplus_le_compat_r.
              apply Rplus_le_compat; [ apply Rmult_le_compat_r; lra | ].
              apply Rmult_le_compat; try lra.
              * hnf; left. apply Rinv_0_lt_compat; lra.
              * repeat (rewrite Q2R_inv; try auto).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))).
              * eapply Rle_trans.
                apply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (nR1 * / nR2) + (Q2R err1 + nR1) * (/ nR2 - err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                    repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_0_lt_preserving; try lra).
                    lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (- (/ nR2 * nR1) + (/ nR2 - err_inv) * (nR1 - Q2R err1) = - nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
          - rewrite Rsub_eq_Ropp_Rplus.
            rewrite Ropp_mult_distr_l.
            destruct (Rle_lt_dec 0 (- nF1)).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat; try lra.
              hnf; left.
              apply Rinv_0_lt_compat.
              apply (Rlt_le_trans _ (Q2R e2lo - Q2R err2)); try lra.
              apply Ropp_le_contravar.
              apply lb_nF1.
              apply error_prop_inv_up.
              rewrite (Rmult_comm (Q2R err2) _).
              remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
              assert ((nR1 * / nR2) + - (nR1 - Q2R err1) * (/ nR2 + err_inv) = - nR1 * err_inv + / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                as simpl_properr by lra.
              rewrite simpl_properr.
              apply Rplus_le_compat_r.
              apply Rplus_le_compat.
              * apply Rmult_le_compat_r; try lra.
              * apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto).
            + eapply Rle_trans.
              eapply Rplus_le_compat_l.
              eapply Rmult_le_compat_neg_l; try lra.
              apply error_prop_inv_down.
              destruct (Rle_lt_dec 0 (/ nR2 - Q2R err2 * / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))).
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                eapply Rmult_le_compat_r; try lra.
                apply Ropp_le_contravar.
                apply lb_nF1.
                rewrite Rmult_comm.
                setoid_rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (/ nR2 * nR1 + - (nR1 - Q2R err1) * (/ nR2 - err_inv) = nR1 * err_inv + / nR2 * Q2R err1 - Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
                { assert (0 <= err_inv)%R.
                  - subst.
                    assert (0 < / ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)))%R
                      by (apply Rinv_0_lt_compat; apply Rmult_0_lt_preserving; try lra).
                          lra.
                  - assert (0 <= (Q2R err1 * err_inv))%R
                      by (rewrite <- (Rmult_0_l 0); apply Rmult_le_compat; lra).
                    apply (Rle_trans _ 0); lra. }
              * eapply Rle_trans.
                eapply Rplus_le_compat_l.
                rewrite Rmult_comm.
                eapply Rmult_le_compat_neg_l.
                hnf; left; auto.
                apply Ropp_le_contravar.
                apply ub_nF1.
                rewrite (Rmult_comm (Q2R err2)).
                remember (/ ((Q2R e2lo - Q2R err2) * (Q2R e2lo - Q2R err2)) * Q2R err2)%R as err_inv.
                assert (nR1 * / nR2 + (/ nR2 - err_inv) * - (Q2R err1 + nR1) = nR1 * err_inv + - / nR2 * Q2R err1 + Q2R err1 * err_inv)%R
                  as simpl_properr by lra.
                rewrite simpl_properr.
                apply Rplus_le_compat_r.
                apply Rplus_le_compat.
                { apply Rmult_le_compat_r; try lra. }
                { apply Rmult_le_compat_r; try lra.
                  repeat (rewrite Q2R_inv; try auto). }
        }
    + remember (divideIntv (widenIntv (e1lo, e1hi) err1) (widenIntv (e2lo, e2hi) err2)) as iv.
      iv_assert iv iv_unf.
      destruct iv_unf as [ivl [ivh iv_unf]].
      rewrite iv_unf.
      assert (ivlo iv = ivl) as ivlo_eq by (rewrite iv_unf; auto).
      assert (ivhi iv = ivh) as ivhi_eq by (rewrite iv_unf; auto).
      rewrite <- ivlo_eq, <- ivhi_eq.
      assert (IVhi (widenInterval (Q2R e2lo, Q2R e2hi) (Q2R err2)) < 0 \/ 0 < IVlo (widenInterval (Q2R e2lo, Q2R e2hi) (Q2R err2)))%R
        as no_div_zero_widen
             by (unfold widenInterval in *; simpl in *; rewrite Q2R_plus, Q2R_minus in no_div_zero_float; auto).
      pose proof (IntervalArith.interval_division_valid no_div_zero_widen H H0) as valid_div_float.
      unfold IntervalArith.contained, widenInterval in *; simpl in *.
      assert (e2lo - err2 == 0 -> False).
      * hnf; intros.
        destruct no_div_zero_float as [float_iv | float_iv]; rewrite <- Q2R0_is_0 in float_iv; apply Rlt_Qlt in float_iv; try lra.
        assert (Q2R e2lo - Q2R err2 <= Q2R e2hi + Q2R err2)%R by lra.
        rewrite<- Q2R_minus, <- Q2R_plus in H4.
        apply Rle_Qle in H4. lra.
      * assert (e2hi + err2 == 0 -> False).
        { hnf; intros.
          destruct no_div_zero_float as [float_iv | float_iv]; rewrite <- Q2R0_is_0 in float_iv; apply Rlt_Qlt in float_iv; try lra.
          assert (Q2R e2lo - Q2R err2 <= Q2R e2hi + Q2R err2)%R by lra.
          rewrite<- Q2R_minus, <- Q2R_plus in H5.
          apply Rle_Qle in H5. lra. }
        { destruct valid_div_float.
          unfold RmaxAbsFun.
          apply Rmult_le_compat_r.
          apply mEps_geq_zero.
          rewrite <- maxAbs_impl_RmaxAbs.
          unfold RmaxAbsFun.
          apply RmaxAbs; subst; simpl in *.
          - rewrite Q2R_min4.
            repeat rewrite Q2R_mult.
            + repeat (rewrite Q2R_inv; auto).
              repeat rewrite Q2R_minus, Q2R_plus; auto.
          - rewrite Q2R_max4.
            repeat rewrite Q2R_mult.
            repeat (rewrite Q2R_inv; auto).
            repeat rewrite Q2R_minus.
            repeat rewrite Q2R_plus; auto. }
  - apply le_neq_bool_to_lt_prop in no_div_zero_float.
    unfold IntervalArith.contained, widenInterval in *; simpl in *.
    assert (e2lo - err2 == 0 -> False).
    + hnf; intros.
      destruct no_div_zero_float as [float_iv | float_iv]; try lra.
      assert (Q2R e2lo - Q2R err2 <= Q2R e2hi + Q2R err2)%R by lra.
      rewrite<- Q2R_minus, <- Q2R_plus in H2.
      apply Rle_Qle in H2. lra.
    + assert (e2hi + err2 == 0 -> False).
      * hnf; intros.
        destruct no_div_zero_float as [float_iv | float_iv]; try lra.
        assert (Q2R e2lo - Q2R err2 <= Q2R e2hi + Q2R err2)%R by lra.
        rewrite<- Q2R_minus, <- Q2R_plus in H3.
        apply Rle_Qle in H3. lra.
      * unfold widenIntv; simpl.
        hnf. intros.
        assert (forall a, Qabs a == 0 -> a == 0).
        { intros. unfold Qabs in H4. destruct a.
          rewrite <- Z.abs_0 in H4. inversion H4. rewrite Zmult_1_r in *.
          rewrite Z.abs_0_iff in H6.
          rewrite H6. rewrite Z.abs_0. hnf. simpl; auto. }
        { assert (minAbs (e2lo - err2, e2hi + err2) == 0 -> False).
          - unfold minAbs. unfold fst, snd. apply Q.min_case_strong.
            + intros. apply H6; rewrite H5; auto.
            + intros. apply H1. rewrite (H4 (e2lo-err2) H6). hnf. auto.
            + intros. apply H2. rewrite H4; auto. hnf; auto.
          - rewrite <- (Qmult_0_l (minAbs (e2lo - err2, e2hi + err2))) in H3.
            rewrite (Qmult_inj_r) in H3; auto. }
Qed.

(**
   Soundness theorem for the error bound validator.
   Proof is by induction on the expression e.
   Each case requires the application of one of the soundness lemmata proven before
 **)
Theorem validErrorbound_sound (e:exp Q):
  forall E1 E2 fVars dVars absenv nR nF err P elo ehi,
    approxEnv E1 absenv fVars dVars E2 ->
    NatSet.Subset (NatSet.diff (usedVars e) dVars) fVars ->
    eval_exp 0%R E1 (toRExp e) nR ->
    eval_exp (Q2R RationalSimps.machineEpsilon) E2 (toRExp e) nF ->
    validErrorbound e absenv dVars = true ->
    validIntervalbounds e absenv P dVars = true ->
    (forall v : NatSet.elt, NatSet.mem v dVars = true ->
                       exists vr, E1 v = Some vr /\
                             (Q2R (fst (fst (absenv (Var Q v)))) <= vr <= Q2R (snd (fst (absenv (Var Q v)))))%R) ->
    (forall v, NatSet.mem v fVars= true ->
          exists vR, E1 v = Some vR /\
                (Q2R (fst (P v)) <= vR <= Q2R (snd (P v)))%R) ->
    absenv e = ((elo,ehi),err) ->
    (Rabs (nR - nF) <= (Q2R err))%R.
Proof.
  induction e;
    try (intros E1 E2 fVars dVars absenv nR nF err P elo ehi;
    intros approxCEnv fVars_subset eval_real eval_float valid_error valid_intv valid_dVars P_valid absenv_eq).
  - eapply validErrorboundCorrectVariable; eauto.
  - pose proof (validIntervalbounds_sound (Const v) absenv P (vR:=nR) valid_intv valid_dVars (fVars:=fVars))
      as bounds_valid.
    eapply validErrorboundCorrectConstant; eauto.
    rewrite absenv_eq in *; simpl in *; auto.
  - unfold validErrorbound in valid_error.
    rewrite absenv_eq in valid_error.
    andb_to_prop valid_error.
    destruct u; try congruence.
    env_assert absenv e absenv_e.
    destruct absenv_e as  [iv [err_e absenv_e]].
    rename R into valid_error.
    andb_to_prop valid_error.
    apply Qeq_bool_iff in R.
    apply Qeq_eqR in R.
    rewrite R.
    destruct iv as [e_lo e_hi].
    inversion eval_real; subst.
    inversion eval_float; subst.
    unfold evalUnop.
    apply bound_flip_sub.
    eapply IHe; eauto.
    simpl in valid_intv.
    rewrite absenv_eq in valid_intv; andb_to_prop valid_intv.
    auto.
    instantiate (1 := e_hi).
    instantiate (1 := e_lo).
    rewrite absenv_e; auto.
  - pose proof (ivbounds_approximatesPrecond_sound (Binop b e1 e2) absenv P valid_intv) as env_approx_p.
    simpl in valid_error.
    env_assert absenv e1 absenv_e1.
    env_assert absenv e2 absenv_e2.
    destruct absenv_e1 as [iv1 [err1 absenv_e1]].
    destruct absenv_e2 as [iv2 [err2 absenv_e2]].
    iv_assert iv1 iv_e1.
    destruct iv_e1 as [ivlo1 [ivhi1 iv_e1]].
    rewrite iv_e1 in absenv_e1.
    iv_assert iv2 iv_e2.
    destruct iv_e2 as [ivlo2 [ivhi2 iv_e2]].
    rewrite iv_e2 in absenv_e2.
    rewrite absenv_eq, absenv_e1, absenv_e2 in valid_error.
    andb_to_prop valid_error.
    simpl in valid_intv.
    rewrite absenv_eq, absenv_e1, absenv_e2 in valid_intv.
    andb_to_prop valid_intv.
    inversion eval_real; subst.
    apply binary_unfolding in eval_float.
    destruct eval_float as [vF1 [vF2 [ eval_vF1 [eval_vF2 eval_float]]]].
    simpl in *.
    pose proof (validIntervalbounds_sound e1 absenv P (vR:=v1) L2 valid_dVars (fVars:=fVars))
      as valid_bounds_e1.
    rewrite absenv_e1 in valid_bounds_e1.
    pose proof (validIntervalbounds_sound e2 absenv P (vR:=v2) R2 valid_dVars (fVars:=fVars))
      as valid_bounds_e2.
    rewrite absenv_e2 in valid_bounds_e2;
      simpl in *.
    assert (Rabs (v1 - vF1) <= Q2R err1 /\ Rabs (v2 - vF2) <= Q2R err2)%R.
    + split.
      * eapply IHe1; eauto.
        hnf. intros a in_diff.
        rewrite NatSet.diff_spec in in_diff.
        apply fVars_subset.
        destruct in_diff.
        rewrite NatSet.diff_spec, NatSet.union_spec.
        split; auto.
      * eapply IHe2; eauto.
        hnf. intros a in_diff.
        rewrite NatSet.diff_spec in in_diff.
        apply fVars_subset.
        destruct in_diff.
        rewrite NatSet.diff_spec, NatSet.union_spec.
        split; auto.
    + destruct H.
      assert ((Q2R ivlo1 <= v1 <= Q2R ivhi1) /\ (Q2R ivlo2 <= v2 <= Q2R ivhi2))%R.
      * split.
        { apply valid_bounds_e1; try auto.
          hnf. intros a in_diff.
          rewrite NatSet.diff_spec in in_diff.
          destruct in_diff as [in_freeVars no_dVar].
          apply fVars_subset.
          rewrite NatSet.diff_spec, NatSet.union_spec.
          split; try auto. }
        { apply valid_bounds_e2; try auto.
          hnf. intros a in_diff.
          rewrite NatSet.diff_spec in in_diff.
          destruct in_diff as [in_freeVars no_dVar].
          apply fVars_subset.
          rewrite NatSet.diff_spec, NatSet.union_spec.
          split; try auto. }
      * destruct H1. destruct b.
        { eapply (validErrorboundCorrectAddition (e1:=e1) absenv); eauto.
          unfold validErrorbound.
          rewrite absenv_eq, absenv_e1, absenv_e2; simpl.
          apply Is_true_eq_true.
          apply andb_prop_intro; split.
          - apply Is_true_eq_left in L. auto.
          - apply andb_prop_intro.
            split; try auto.
            apply andb_prop_intro.
            split; apply Is_true_eq_left; try auto.
            apply L1.
            apply R.
            apply Is_true_eq_left; apply R0. }
        { eapply (validErrorboundCorrectSubtraction (e1:=e1) absenv); eauto.
          unfold validErrorbound.
          rewrite absenv_eq, absenv_e1, absenv_e2; simpl.
          apply Is_true_eq_true.
          apply andb_prop_intro; split.
          - apply Is_true_eq_left in L. auto.
          - apply andb_prop_intro.
            split; try auto.
            apply andb_prop_intro.
            split; apply Is_true_eq_left; try auto.
            apply L1.
            apply R.
            apply Is_true_eq_left; apply R0. }
        { eapply (validErrorboundCorrectMult (e1 := e1) absenv); eauto.
          unfold validErrorbound.
          rewrite absenv_eq, absenv_e1, absenv_e2; simpl.
          apply Is_true_eq_true.
          apply andb_prop_intro; split.
          - apply Is_true_eq_left in L. auto.
          - apply andb_prop_intro.
            split; try auto.
            apply andb_prop_intro.
            split; apply Is_true_eq_left; try auto.
            apply L1.
            apply R.
            apply Is_true_eq_left; apply R0. }
        { eapply (validErrorboundCorrectDiv (e1 := e1) absenv); eauto.
          unfold validErrorbound.
          rewrite absenv_eq, absenv_e1, absenv_e2; simpl.
          apply Is_true_eq_true.
          apply andb_prop_intro; split.
          - apply Is_true_eq_left in L. auto.
          - apply andb_prop_intro.
            split; try auto.
            apply andb_prop_intro.
            split; apply Is_true_eq_left; try auto.
            apply L1.
            apply R.
            apply Is_true_eq_left; apply R0.
          - andb_to_prop R1; auto. }
Qed.

Theorem validErrorboundCmd_sound (f:cmd Q) (absenv:analysisResult):
  forall E1 E2 outVars fVars dVars vR vF elo ehi err P,
    approxEnv E1 absenv fVars dVars E2 ->
    ssa f (NatSet.union fVars dVars) outVars ->
    NatSet.Subset (NatSet.diff (Commands.freeVars f) dVars) fVars ->
    bstep (toRCmd f) E1 0%R vR  ->
    bstep (toRCmd f) E2 (Q2R RationalSimps.machineEpsilon) vF ->
    validErrorboundCmd f absenv dVars = true ->
    validIntervalboundsCmd f absenv P dVars = true ->
    (forall v : NatSet.elt, NatSet.mem v dVars = true ->
                       exists vR, E1 v = Some vR /\
                             (Q2R (fst (fst (absenv (Var Q v)))) <= vR <= Q2R (snd (fst (absenv (Var Q v)))))%R) ->
    (forall v, NatSet.mem v fVars= true ->
          exists vR, E1 v = Some vR /\
                (Q2R (fst (P v)) <= vR <= Q2R (snd (P v)))%R) ->
    absenv (getRetExp f) = ((elo,ehi),err) ->
    (Rabs (vR - vF) <= (Q2R err))%R.
Proof.
  induction f;
    intros E1 E2 outVars fVars dVars vR vF elo ehi err P;
    intros approxc1c2 ssa_f freeVars_subset eval_real eval_float valid_bounds valid_intv fVars_sound P_valid absenv_res.
  - simpl in eval_real, eval_float.
    inversion eval_real; inversion eval_float; subst.
    inversion ssa_f; subst.
    assert (approxEnv (updEnv n v E1) absenv fVars (NatSet.add n dVars) (updEnv n v0 E2)).
    + eapply approxUpdBound; try auto.
      * unfold validErrorboundCmd in valid_bounds.
        andb_to_prop valid_bounds.
        rename L0 into validErrorbound_e.
        rewrite Qeq_bool_iff in R0.
        assert (snd (absenv (Var Q n)) == snd (absenv (Var Q n))) by apply Qeq_refl.
        pose proof (Qleb_comp _ _ R0 _ _ H).
        assert (Qle (snd (absenv e)) (snd (absenv (Var Q n)))).
        { rewrite <- Qle_bool_iff.
          rewrite H0.
          rewrite Qle_bool_iff.
          apply Qle_lteq.
          right. auto. }
        { apply Qle_Rle in H1.
          eapply Rle_trans.
          Focus 2.
          apply H1.
          env_assert absenv e absenv_e.
          destruct absenv_e as [iv [err_e absenv_e]].
          destruct iv.
          eapply validErrorbound_sound; eauto.
          - hnf. intros a a_in_diff.
            apply freeVars_subset.
            rewrite NatSet.diff_spec in *.
            simpl.
            destruct a_in_diff.
            split; try auto.
            rewrite NatSet.remove_spec, NatSet.union_spec.
            split; try auto.
            hnf; intros; subst.
            specialize (H2 n H3).
            rewrite <- NatSet.mem_spec in H2.
            rewrite H2 in *; congruence.
          - simpl in valid_intv.
            andb_to_prop valid_intv.
            assumption.
          - instantiate (1 := q0). instantiate (1 := q).
            rewrite absenv_e; auto. }
      (* * inversion ssa_f; subst.
        case_eq (NatSet.mem n fVars); intros case_mem.
        { rewrite NatSet.mem_spec in case_mem.
          assert (NatSet.In n fVars \/ NatSet.In n dVars) as in_disj by (left; auto).
          rewrite <- NatSet.union_spec, <- NatSet.mem_spec in in_disj.
          hnf in inVars_union.
          rewrite NatSet.mem_spec in in_disj.
          rewrite inVars_union in in_disj.
          rewrite <- NatSet.mem_spec in in_disj.
          rewrite in_disj in H7. inversion H7. }
        { case_eq (NatSet.mem n (NatSet.union fVars dVars)); intros case_union; try auto.
          rewrite NatSet.mem_spec in case_union.
          rewrite inVars_union in case_union.
          rewrite <- NatSet.mem_spec in case_union.
          rewrite case_union in H7; inversion H7. } *)
    + simpl in valid_bounds.
      andb_to_prop valid_bounds.
      rename L0 into validbound_e;
        rename R0 into var_bound_eq_e;
        rename R into valid_rec.
      simpl in valid_intv;
        andb_to_prop valid_intv.
      eapply (IHf (updEnv n v E1) (updEnv n v0 E2) _ _ _ vR vF elo ehi err P); eauto.
      * instantiate (1 := outVars).
        eapply ssa_equal_set; eauto.
        hnf. intros a; split; intros in_set.
        { rewrite NatSet.add_spec, NatSet.union_spec;
            rewrite NatSet.union_spec, NatSet.add_spec in in_set.
          destruct in_set as [P1 | [ P2 | P3]]; auto. }
        { rewrite NatSet.add_spec, NatSet.union_spec in in_set;
            rewrite NatSet.union_spec, NatSet.add_spec.
          destruct in_set as [P1 | [ P2 | P3]]; auto. }
      * hnf. intros a in_diff.
        rewrite NatSet.diff_spec, NatSet.add_spec in in_diff.
        destruct in_diff as [ in_freeVars no_dVar].
        apply freeVars_subset.
        simpl.
        rewrite NatSet.diff_spec, NatSet.remove_spec, NatSet.union_spec.
        split; try auto.
      * intros v1 v1_mem.
        unfold updEnv.
        case_eq (v1 =? n); intros v1_eq.
        { rename R1 into eq_lo;
            rename R0 into eq_hi.
          apply Qeq_bool_iff in eq_lo.
          apply Qeq_eqR in eq_lo.
          apply Qeq_bool_iff in eq_hi.
          apply Qeq_eqR in eq_hi.
          apply Nat.eqb_eq in v1_eq; subst.
          rewrite <- eq_lo, <- eq_hi.
          exists v; split; try auto.
          eapply validIntervalbounds_sound; eauto.
          hnf. intros a a_mem_diff.
          rewrite NatSet.diff_spec in a_mem_diff.
          destruct a_mem_diff as [a_mem_freeVars a_no_dVar].
          apply freeVars_subset.
          simpl. rewrite NatSet.diff_spec, NatSet.remove_spec, NatSet.union_spec.
          split; try auto.
          split; try auto.
          hnf; intros; subst.
          specialize (H2 n a_mem_freeVars).
          rewrite <- NatSet.mem_spec in H2.
          rewrite H2 in *; congruence. }
        { rewrite NatSet.mem_spec in v1_mem.
          rewrite NatSet.add_spec in v1_mem.
          rewrite Nat.eqb_neq in v1_eq.
          destruct v1_mem.
          - exfalso; apply v1_eq; auto.
          - apply fVars_sound. rewrite NatSet.mem_spec; auto. }
      * intros v1 mem_fVars.
        specialize (P_valid v1 mem_fVars).
        unfold updEnv.
        case_eq (v1 =? n); intros case_v1; try rewrite case_v1 in *; try auto.
        rewrite Nat.eqb_eq in case_v1; subst.
        assert (NatSet.In n (NatSet.union fVars dVars))
          as in_union
            by (rewrite NatSet.mem_spec in *; rewrite NatSet.union_spec; auto).
        rewrite <- NatSet.mem_spec in in_union.
        rewrite in_union in *; congruence.
  - inversion eval_real; inversion eval_float; subst.
    unfold updEnv; simpl.
    unfold validErrorboundCmd in valid_bounds.
    simpl in valid_intv.
    env_assert absenv e absenv_e.
    destruct absenv_e as [iv [err_e absenv_e]].
    destruct iv.
    eapply validErrorbound_sound; eauto.
Qed.