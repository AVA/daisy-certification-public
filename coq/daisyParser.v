Require Import Coq.Strings.Ascii Coq.Strings.String Coq.Lists.List Coq.Reals.Reals.

Require Import CertificateChecker.

Import Coq.Lists.List.ListNotations.

Inductive Token:Type :=
| DLET
| DRET
| DPRE
| DABS
| DCOND
| DVAR
| DCONST: N -> Token
| DNEG
| DPLUS
| DSUB
| DMUL
| DDIV
| DFRAC
| DFAIL: ascii -> Token.

Open Scope string_scope.

Definition getChar (input:string):=
  match input with
  |String c s => c
  | _ => ascii_of_nat 0
  end.

Definition getConst (c:ascii) :=
  ((N_of_ascii c) - 48)%N.

Definition suffix (s:string) :=
match s with
|String c s' => s'
| EmptyString => EmptyString
end.

Definition isDigit (c:ascii) :=
  (48 <=? (nat_of_ascii c)) && (nat_of_ascii c <=? 57).

Definition isAlpha (c:ascii) :bool :=
  (65 <=? nat_of_ascii c) && (nat_of_ascii c <=? 90) ||
    (97 <=? nat_of_ascii c) && (nat_of_ascii c <=? 122).

Definition isAlphaNum (c :ascii) :bool :=
  isDigit c || isAlpha c.

Fixpoint lexConst (input:string) (akk:N) :=
  match input with
  |String c input' =>
   if (isDigit c)
   then lexConst input' (akk * 10 + getConst c)
   else (akk, input)
  |EmptyString => (akk, input)
  end.

Fixpoint lexName (input:string) :=
    match input with
     | String char input' =>
       if (isAlphaNum char)
       then
         let (name, input') := lexName input' in
         (String char name, input')
       else ("", input)
     | "" => ("",input)
     end.

Fixpoint strSize str :nat :=
    match str with
    | String _ str' => 1 + strSize str'
    | "" => 0
    end.

Fixpoint lex input fuel :=
match fuel with
  |S fuel' =>
   match input with
       | String char input' =>
         if isDigit char
         then
           let (num, input'') := lexConst input 0 in
           DCONST num :: lex input'' fuel'
         else
           if isAlpha char
           then
             let (name, input'') := lexName input in
             match name with
                 | "Ret" => DRET :: lex input'' fuel'
                 | "Let" => DLET :: lex input'' fuel'
                 | "PRE" => DPRE :: lex input'' fuel'
                 | "ABS" => DABS :: lex input'' fuel'
                 | "Var" => DVAR :: lex input'' fuel'
                 | _ => []
             end
           else
             match char with
                  | "+"%char => DPLUS :: lex input' fuel'
                  | "-"%char => DSUB  :: lex input' fuel'
                  | "*"%char => DMUL :: lex input' fuel'
                  | "/"%char => DDIV :: lex input' fuel'
                  | "035"%char => DFRAC :: lex input' fuel'
                  | "?"%char => DCOND :: lex input' fuel'
                  | "~"%char => DNEG :: lex input' fuel'
                  | " "%char => lex input' fuel'
                  | "010"%char => lex input' fuel'
                  | _ => DFAIL char :: []
              end
       |  _  => []
   end
  |_ => []
end.

Fixpoint str_of_num (n:nat) (m:nat):=
  match m with
    |S m' =>
     if n <? 10 then String (ascii_of_nat (n + 48)) ""
     else String (ascii_of_nat ((n mod 10) + 48)) (str_of_num (n / 10) m')
    |_ => ""
  end .

(*
Definition pp_token (token:Token) :=
  match token with
  | DLET => "Let"
  | DRET => "Ret"
  | DPRE => "PRE"
  | DABS => "ABS"
  | DCOND => "?"
  | DVAR => "Var"
  | DCONST num => str_of_num num num
  | DNEG => "~"
  | DPLUS => "+"
  | DSUB => "-"
  | DMUL => "*"
  | DDIV => "/"
  | DFRAC => "#"
  end .

Fixpoint str_join s1 s2 :=
  match s1 with
  | String c s1' => String c (str_join s1' s2)
  | "" => s2
  end.

(* Pretty Printer for Tokens *)
Fixpoint pp (tokList:list Token) :=
  match tokList with
  | token :: tokRest => str_join (pp_token token) (pp tokRest)
  | [] => ""
  end.
*)
(** Prefix form parser for expressions **)
Fixpoint parseExp (tokList:list Token) (fuel:nat):option (exp Q * list Token) :=
  match fuel with
    |S fuel' =>
     match tokList with
     | DCONST n :: DFRAC :: DCONST m :: tokRest =>
       match m with
       |N0 => None
       |Npos p => Some (Const (Z.of_N n # p), tokRest)
       end
     | DVAR :: DCONST n :: tokRest => Some (Var Q (N.to_nat n), tokRest)
     | DNEG :: tokRest =>
       match (parseExp tokRest fuel') with
       | Some (Const c, tokRest) => Some (Const (- c), tokRest)
       | Some (e1,tokRest') => Some (Unop Neg e1, tokRest')
       | None => None
       end
     | DPLUS :: tokRest =>
       match parseExp tokRest fuel' with
       | Some (e1,tokRest') =>
         match (parseExp tokRest' fuel') with
         | Some (e2, tokRest'') => Some (Binop Plus e1 e2, tokRest'')
         | None => None
         end
       | None => None
       end
     | DSUB :: tokRest =>
       match parseExp tokRest fuel' with
       | Some (e1,tokRest') =>
         match (parseExp tokRest' fuel') with
         | Some (e2, tokRest'') => Some (Binop Sub e1 e2, tokRest'')
         | None => None
         end
       | None => None
       end
     | DMUL :: tokRest =>
       match parseExp tokRest fuel' with
       | Some (e1,tokRest') =>
         match (parseExp tokRest' fuel') with
         | Some (e2, tokRest'') => Some (Binop Mult e1 e2, tokRest'')
         | None => None
         end
       | None => None
       end
     | DDIV :: tokRest =>
       match parseExp tokRest fuel' with
       | Some (e1,tokRest') =>
         match (parseExp tokRest' fuel') with
         | Some (e2, tokRest'') => Some (Binop Div e1 e2, tokRest'')
         | None => None
         end
       | None => None
       end
     | _ => None
     end
    |_ => None
  end.

Definition parseRet input fuel :option (cmd Q * list Token):=
    match parseExp input fuel with
     | Some (e, tokRest) => Some (Ret e, tokRest)
     | None => None
    end .

Fixpoint parseLet input fuel:option (cmd Q * list Token) :=
  match fuel with
    |S fuel' =>
     match input with
     (* We already have a valid let binding *)
     | DVAR :: DCONST n :: expLetRest =>
       (* so we parse an expression *)
       match parseExp expLetRest fuel with
       | Some (e, letRest) =>
         match letRest with
             (* If we continue with a let *)
             | DLET :: letBodyRest =>
               (* Parse it *)
               match (parseLet letBodyRest fuel') with
               (* And construct a result from it *)
               | Some (letCmd, arbRest) => Some (Let (N.to_nat n) e letCmd, arbRest)
               | _ => None
               end
             (* But if we have a return *)
             | DRET :: retBodyRest =>
               (* parse only this *)
               match (parseRet retBodyRest fuel) with
               (* and construct the result *)
               | Some (retCmd, arbRest) => Some (Let (N.to_nat n) e retCmd, arbRest)
               | _ => None
               end
             | _ => None (* fail if there is no continuation for the let *)
       end
     | None => None (* fail if we do not have an expression to bind *)
     end
    | _ => None (* fail if we cannot find a variable *)
  end
| _ => None
end.

Definition parseFrac (input:list Token) :option (Q * list Token) :=
  match input with
  | DNEG :: DCONST n :: DFRAC :: DCONST m :: tokRest =>
    match m with
    |N0 => None
    |Npos p => Some ((- Z.of_N n # p),tokRest)
    end
  | DCONST n :: DFRAC :: DCONST m :: tokRest =>
    match m with
    |N0 => None
    |Npos p => Some ((Z.of_N n # p),tokRest)
    end
  | _ => None
  end.

Definition parseIV (input:list Token) :option (intv * list Token) :=
  match (parseFrac input) with
  |Some (iv_lo, tokRest) =>
   match (parseFrac tokRest) with
   | Some (iv_hi, tokList) => Some ((iv_lo, iv_hi), tokList)
   | _ => None
   end
  | _ => None
  end.

Definition defaultPre :(nat -> intv):= fun x => mkIntv 0 0.

Definition updPre (n:nat) (iv:intv) (P:precond) :=
  fun m => if (n =? m) then iv else P m.

(** Precondition parser:
  The precondition should be encoded in the following format:
  PRECOND ::= DCOND DVAR DCONST FRACTION FRACTION PRECOND | EPSILON
  The beginning of the precondition is marked by the DPRE token
**)
Fixpoint parsePrecondRec (input:list Token) (fuel:nat) :option (precond * list Token) :=
  match fuel with
    |S fuel' =>
     match input with
     | DCOND :: DVAR :: DCONST n :: fracRest =>
       match parseIV fracRest with
       | Some (iv, precondRest) =>
         match parsePrecondRec precondRest fuel' with
         | Some (P, tokRest) => Some (updPre (N.to_nat n) iv P, tokRest)
         | None => Some (updPre (N.to_nat n) iv defaultPre, precondRest)
         end
       | _ => None
       end
     | _ => None
     end
    |_ => None
  end.

Definition parsePrecond (input :list Token) fuel :=
  match input with
  | DPRE :: tokRest => parsePrecondRec tokRest fuel
  | _ => None
  end.

Definition defaultAbsenv:analysisResult := fun e => (mkIntv 0 0 ,0).

Definition updAbsenv (e:exp Q) (iv:intv) (err:Q) (A:analysisResult):=
  fun e' => if expEqBool e e' then (iv, err) else A e'.

(** Abstract environment parser:
  The abstract environment should be encoded in the following format:
  ABSENV ::= ? EXPRESSION FRACTION FRACTION FRACTION ABSENV | EPSILON
  The beginning of the abstract environment is marked by the DABS token
**)
Fixpoint parseAbsEnvRec (input:list Token) fuel :option (analysisResult * list Token):=
  match fuel with
    |S fuel' =>
     match input with
     | DCOND :: expRest =>
       match parseExp expRest fuel with
       | Some (e,fracRest) =>
         match parseIV fracRest with
         | Some (iv, errRest) =>
           match parseFrac errRest with
           | Some (err, absenvRest) =>
             match parseAbsEnvRec absenvRest fuel' with
             | Some (A, tokRest) => Some (updAbsenv e iv err A, tokRest)
             | None => Some (updAbsenv e iv err defaultAbsenv, absenvRest)
             end
           | None => None
           end
         | _ => None
         end
       | None => None
       end
     | _ => Some (defaultAbsenv, input)
     end
    |_ => None
  end.

Definition parseAbsEnv (input:list Token) fuel :=
  match input with
  | DABS :: tokRest => parseAbsEnvRec tokRest fuel
  | _ => None
  end.

Definition dParse  (input:list Token) fuel :=
  let cmdParse :=
      match input with
      | DLET :: tokRest => parseLet tokRest fuel
      | DRET :: tokRest => parseRet tokRest fuel
      | _ => None
      end
  in
  match cmdParse with
  | Some (dCmd, tokRest) =>
    match tokRest with
    | DPRE :: preRest =>
      match parsePrecond tokRest fuel with
      | Some (P, absenvRest) =>
        match parseAbsEnv absenvRest fuel with
        | Some (A, residual) => Some ((dCmd, P, A), residual)
        | _ => None
        end
      | None => None
      end
    | DABS :: absRest =>
      match parseAbsEnv tokRest fuel with
      | Some (A, precondRest) =>
        match parsePrecond precondRest fuel with
        | Some (P, residual) => Some ((dCmd, P, A),residual)
        | _ => None
        end
      | None => None
      end
    | _ => None
    end
  | _ => None
  end.

Fixpoint check_rec (input:list Token) (num_fun:nat) fuel:=
  match num_fun with
  | S n' =>
    match dParse input fuel with
    | Some ((dCmd, P, A), []) =>
      if CertificateCheckerCmd dCmd A P
      then "True\n"
      else "False\n"
    | Some ((dCmd, P, A), residual) =>
      if CertificateCheckerCmd dCmd A P
      then check_rec residual n' fuel
      else "False\n"
    | None => "parse failure\n"
    end
  | _ => "failure num of functions given"
  end.

Fixpoint str_length s :=
  match s with
  |String c s' => S (str_length s')
  |"" => O
  end.

Fixpoint check (f:cmd Q) (P:precond) (A:analysisResult) (n:nat) :=
  match n with
  |S n' => CertificateCheckerCmd f A P && (check f P A n')
  |_ => true
  end.

Fixpoint check_all (num_fun:nat) (iters:nat) (input:list Token) fuel:=
  match num_fun with
  |S nf =>
   match (dParse input fuel) with
   |Some ((f,P,A), residual) =>
    if (check f P A iters)
    then
      match residual with
      |a :: b => check_all nf iters residual fuel
      |[] => "True"
      end
    else
      "False"
   |None => "Failure: Parse"
   end
  |_ => "Failure: Number of Functions in certificate\n"
  end.

Definition runChecker (input:string) :=
  let tokList := lex input (str_length input) in
  match tokList with
  | DCONST n :: DCONST m :: tokRest => check_all (N.to_nat m) (N.to_nat n)  tokRest (List.length tokList)
  | _ => "failure no num of functions"
  end.

(* Definition res := Eval compute in runChecker "1 1 Ret + 1657#5 Var 1 PRE ? Var 1 ~100#1 100#1 ABS ? + 1657#5 Var 1 1157#5 2157#5 7771411516990528329#81129638414606681695789005144064 ? 1657#5 1657#5 1657#5 1657#45035996273704960 ? Var 1 ~100#1 100#1 25#2251799813685248". *)