(**
Environment library.
Defines the environment type for the Daisy framework and a simulation relation between environments.
 **)
Require Import Coq.Reals.Reals Coq.micromega.Psatz Coq.QArith.Qreals.
Require Import Daisy.Infra.ExpressionAbbrevs Daisy.Infra.RationalSimps Daisy.Commands.

(**
Define an approximation relation between two environments.
We use this relation for the soundness proofs.
It is necessary to have this relation, since two evaluations of the very same
expression may yield different values for different machine epsilons
(or environments that already only approximate each other)
 **)
Inductive approxEnv : env -> analysisResult -> NatSet.t -> NatSet.t -> env -> Prop :=
|approxRefl A:
   approxEnv emptyEnv A NatSet.empty NatSet.empty emptyEnv
|approxUpdFree E1 E2 A v1 v2 x fVars dVars:
   approxEnv E1 A fVars dVars E2 ->
   (Rabs (v1 - v2) <= Rabs v1 * Q2R machineEpsilon)%R ->
   NatSet.mem x (NatSet.union fVars dVars) = false ->
   approxEnv (updEnv x v1 E1) A (NatSet.add x fVars) dVars (updEnv x v2 E2)
|approxUpdBound E1 E2 A v1 v2 x fVars dVars:
   approxEnv E1 A fVars dVars E2 ->
   (Rabs (v1 - v2) <= Q2R (snd (A (Var Q x))))%R ->
   NatSet.mem x (NatSet.union fVars dVars) = false ->
   approxEnv (updEnv x v1 E1) A fVars (NatSet.add x dVars) (updEnv x v2 E2).

Inductive approxParams :env -> R -> env -> Prop :=
|approxParamRefl eps:
   approxParams emptyEnv eps emptyEnv
|approxParamUpd E1 E2 eps x v1 v2 :
   approxParams E1 eps E2 ->
   (Rabs (v1 - v2) <= eps)%R ->
   approxParams (updEnv x v1 E1) eps (updEnv x v2 E2).
