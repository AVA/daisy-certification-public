(**
 Formalization of the Abstract Syntax Tree of a subset used in the Daisy framework
 **)
Require Import Coq.Reals.Reals Coq.QArith.QArith.
Require Export Daisy.Infra.ExpressionAbbrevs Daisy.Infra.NatSet.

(**
  Next define what a program is.
  Currently no loops, or conditionals.
  Only assignments and return statement
**)
Inductive cmd (V:Type) :Type :=
Let: nat -> exp V -> cmd V -> cmd V
| Ret: exp V -> cmd V.

(**
  Define big step semantics for the Daisy language, terminating on a "returned"
  result value
 **)
Inductive bstep : cmd R -> env -> R -> R -> Prop :=
  let_b x e s E v eps res:
    eval_exp eps E e v ->
    bstep s (updEnv x v E) eps res ->
    bstep (Let x e s) E eps res
 |ret_b e E v eps:
    eval_exp eps E e v ->
    bstep (Ret e) E eps v.

(**
  The free variables of a command are all used variables of expressions
  without the let bound variables
**)
Fixpoint freeVars V (f:cmd V) :NatSet.t :=
  match f with
  | Let x e g => NatSet.remove x (NatSet.union (usedVars e) (freeVars g))
  | Ret e => usedVars e
  end.

(**
  The defined variables of a command are all let bound variables
**)
Fixpoint definedVars V (f:cmd V) :NatSet.t :=
  match f with
  | Let x _ g => NatSet.add x (definedVars g)
  | Ret _ => NatSet.empty
  end.

(**
  The live variables of a command are all variables which occur on the right
  hand side of an assignment or at a return statement
 **)
Fixpoint liveVars V (f:cmd V) :NatSet.t :=
  match f with
  | Let _ e g => NatSet.union (usedVars e) (liveVars g)
  | Ret e => usedVars e
  end.