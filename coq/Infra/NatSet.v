Require Import Coq.MSets.MSets Coq.Arith.PeanoNat.

(**
 Module for an ordered type with leibniz, based on code from coq-club mailing list
http://coq-club.inria.narkive.com/zptqoou2/how-to-use-msets
**)
Module OWL.
  Definition t := nat.
  Definition eq := @eq t.
  Definition eq_equiv :Equivalence eq := eq_equivalence.
  Definition lt := lt.
  Definition lt_strorder : StrictOrder lt := Nat.lt_strorder.
  Instance lt_compat : Proper (eq ==> eq ==> iff) lt.
  Proof.
    now unfold eq; split; subst.
  Qed.
  Definition compare := Compare_dec.nat_compare.

Lemma compare_spec : forall x y, CompSpec eq lt x y (compare x y).
Proof.
  intros; case_eq (compare x y); constructor.
  now apply Compare_dec.nat_compare_eq.
  now apply Compare_dec.nat_compare_Lt_lt.
  now apply Compare_dec.nat_compare_Gt_gt.
Qed.

Definition eq_dec := Peano_dec.eq_nat_dec.
Definition eq_leibniz a b (H:eq a b) := H.
End OWL.

Module NatSet := MakeWithLeibniz OWL.

Module NatSetProps := WPropertiesOn OWL NatSet.