(**
Proofs of general bounds on the error of arithmetic expressions.
This shortens soundness proofs later.
Bounds are explained in section 5, Deriving Computable Error Bounds
**)
Require Import Coq.Reals.Reals Coq.micromega.Psatz Coq.QArith.QArith Coq.QArith.Qreals.
Require Import Daisy.Infra.Abbrevs Daisy.Infra.RationalSimps Daisy.Infra.RealSimps Daisy.Infra.RealRationalProps.
Require Import Daisy.Environments Daisy.Infra.ExpressionAbbrevs.

Lemma const_abs_err_bounded (n:R) (nR:R) (nF:R) (E1 E2:env) (absenv:analysisResult):
  eval_exp 0%R E1 (Const n) nR ->
  eval_exp (Q2R machineEpsilon) E2 (Const n) nF ->
  (Rabs (nR - nF) <= Rabs n * (Q2R machineEpsilon))%R.
Proof.
  intros eval_real eval_float.
  inversion eval_real; subst.
  rewrite delta_0_deterministic; auto.
  inversion eval_float; subst.
  unfold perturb; simpl.
  rewrite Rabs_err_simpl, Rabs_mult.
  apply Rmult_le_compat_l; [apply Rabs_pos | auto].
Qed.

Lemma add_abs_err_bounded (e1:exp Q) (e1R:R) (e1F:R) (e2:exp Q) (e2R:R) (e2F:R)
      (vR:R) (vF:R) (E1 E2:env) (err1 err2 :Q):
  eval_exp 0%R E1 (toRExp e1) e1R ->
  eval_exp (Q2R machineEpsilon) E2 (toRExp e1) e1F ->
  eval_exp 0%R E1 (toRExp e2) e2R ->
  eval_exp (Q2R machineEpsilon) E2 (toRExp e2) e2F ->
  eval_exp 0%R E1 (Binop Plus (toRExp e1) (toRExp e2)) vR ->
  eval_exp (Q2R machineEpsilon) (updEnv 2 e2F (updEnv 1 e1F emptyEnv)) (Binop Plus (Var R 1) (Var R 2)) vF ->
  (Rabs (e1R - e1F) <= Q2R err1)%R ->
  (Rabs (e2R - e2F) <= Q2R err2)%R ->
  (Rabs (vR - vF) <= Q2R err1 + Q2R err2 + (Rabs (e1F + e2F) * (Q2R machineEpsilon)))%R.
Proof.
  intros e1_real e1_float e2_real e2_float plus_real plus_float bound_e1 bound_e2.
  (* Prove that e1R and e2R are the correct values and that vR is e1R + e2R *)
  inversion plus_real; subst.
  rewrite delta_0_deterministic in plus_real; auto.
  rewrite (delta_0_deterministic (evalBinop Plus v1 v2) delta); auto.
  unfold evalBinop in *; simpl in *.
  clear delta H2.
  rewrite (meps_0_deterministic H3 e1_real);
    rewrite (meps_0_deterministic H5 e2_real).
  rewrite (meps_0_deterministic H3 e1_real) in plus_real.
  rewrite (meps_0_deterministic H5 e2_real) in plus_real.
  clear H3 H5 H6 v1 v2.
  (* Now unfold the float valued evaluation to get the deltas we need for the inequality *)
  inversion plus_float; subst.
  unfold perturb; simpl.
  inversion H3; subst; inversion H5; subst.
  unfold updEnv; simpl.
  unfold updEnv in H0,H1; simpl in *.
  symmetry in H0, H1.
  inversion H0; inversion H1; subst.
  (* We have now obtained all necessary values from the evaluations --> remove them for readability *)
  clear plus_float H3 H5 plus_real e1_real e1_float e2_real e2_float H0 H1.
  repeat rewrite Rmult_plus_distr_l.
  rewrite Rmult_1_r.
  rewrite Rsub_eq_Ropp_Rplus.
  repeat rewrite Ropp_plus_distr.
  rewrite plus_bounds_simplify.
  pose proof (Rabs_triang (e1R + - e1F) ((e2R + - e2F) + - ((e1F + e2F) * delta))).
  rewrite Rplus_assoc.
  eapply Rle_trans.
  apply H.
  pose proof (Rabs_triang (e2R + - e2F) (- ((e1F + e2F) * delta))).
  pose proof (Rplus_le_compat_l (Rabs (e1R + - e1F)) _ _ H0).
  eapply Rle_trans.
  apply H1.
  rewrite <- Rplus_assoc.
  repeat rewrite <- Rsub_eq_Ropp_Rplus.
  rewrite Rabs_Ropp.
  eapply Rplus_le_compat.
  - eapply Rplus_le_compat; auto.
  - rewrite Rabs_mult.
    eapply Rle_trans.
    eapply Rmult_le_compat_l.
    apply Rabs_pos.
    apply H2.
    apply Req_le; auto.
Qed.

(**
  Copy-Paste proof with minor differences, was easier then manipulating the evaluations and then applying the lemma
**)
Lemma subtract_abs_err_bounded (e1:exp Q) (e1R:R) (e1F:R) (e2:exp Q) (e2R:R)
      (e2F:R) (vR:R) (vF:R) (E1 E2:env) err1 err2:
  eval_exp 0%R E1 (toRExp e1) e1R ->
  eval_exp (Q2R machineEpsilon) E2   (toRExp e1) e1F ->
  eval_exp 0%R E1 (toRExp e2) e2R ->
  eval_exp (Q2R machineEpsilon) E2 (toRExp e2) e2F ->
  eval_exp 0%R E1 (Binop Sub (toRExp e1) (toRExp e2)) vR ->
  eval_exp (Q2R machineEpsilon) (updEnv 2 e2F (updEnv 1 e1F emptyEnv)) (Binop Sub (Var R 1) (Var R 2)) vF ->
  (Rabs (e1R - e1F) <= Q2R err1)%R ->
  (Rabs (e2R - e2F) <= Q2R err2)%R ->
  (Rabs (vR - vF) <= Q2R err1 + Q2R err2 + ((Rabs (e1F - e2F)) * (Q2R machineEpsilon)))%R.
Proof.
  intros e1_real e1_float e2_real e2_float sub_real sub_float bound_e1 bound_e2.
  (* Prove that e1R and e2R are the correct values and that vR is e1R + e2R *)
  inversion sub_real; subst.
  rewrite delta_0_deterministic in sub_real; auto.
  rewrite delta_0_deterministic; auto.
  unfold evalBinop in *; simpl in *.
  clear delta H2.
  rewrite (meps_0_deterministic H3 e1_real);
    rewrite (meps_0_deterministic H5 e2_real).
  rewrite (meps_0_deterministic H3 e1_real) in sub_real.
  rewrite (meps_0_deterministic H5 e2_real) in sub_real.
  clear H3 H5 H6 v1 v2.
  (* Now unfold the float valued evaluation to get the deltas we need for the inequality *)
  inversion sub_float; subst.
  unfold perturb; simpl.
  inversion H3; subst; inversion H5; subst.
  unfold updEnv; simpl.
  symmetry in H0, H1.
  unfold updEnv in H0, H1; simpl in H0, H1.
  inversion H0; inversion H1; subst.
  (* We have now obtained all necessary values from the evaluations --> remove them for readability *)
  clear sub_float H3 H5 sub_real e1_real e1_float e2_real e2_float H0 H1.
  repeat rewrite Rmult_plus_distr_l.
  rewrite Rmult_1_r.
  repeat rewrite Rsub_eq_Ropp_Rplus.
  repeat rewrite Ropp_plus_distr.
  rewrite plus_bounds_simplify.
  rewrite Ropp_involutive.
  rewrite Rplus_assoc.
  eapply Rle_trans.
  apply Rabs_triang.
  eapply Rle_trans.
  eapply Rplus_le_compat_l.
  apply Rabs_triang.
  rewrite <- Rplus_assoc.
  setoid_rewrite Rplus_comm at 4.
  repeat rewrite <- Rsub_eq_Ropp_Rplus.
  rewrite Rabs_Ropp.
  rewrite Rabs_minus_sym in bound_e2.
  apply Rplus_le_compat; [apply Rplus_le_compat; auto | ].
  rewrite Rabs_mult.
  eapply Rmult_le_compat_l; [apply Rabs_pos | auto].
Qed.

Lemma mult_abs_err_bounded (e1:exp Q) (e1R:R) (e1F:R) (e2:exp Q) (e2R:R) (e2F:R)
      (vR:R) (vF:R) (E1 E2:env):
  eval_exp 0%R E1 (toRExp e1) e1R ->
  eval_exp (Q2R machineEpsilon) E2 (toRExp e1) e1F ->
  eval_exp 0%R E1 (toRExp e2) e2R ->
  eval_exp (Q2R machineEpsilon) E2  (toRExp e2) e2F ->
  eval_exp 0%R E1  (Binop Mult (toRExp e1) (toRExp e2)) vR ->
  eval_exp (Q2R machineEpsilon) (updEnv 2 e2F (updEnv 1 e1F emptyEnv)) (Binop Mult (Var R 1) (Var R 2)) vF ->
  (Rabs (vR - vF) <= Rabs (e1R * e2R - e1F * e2F) + Rabs (e1F * e2F) * (Q2R machineEpsilon))%R.
Proof.
  intros e1_real e1_float e2_real e2_float mult_real mult_float.
  (* Prove that e1R and e2R are the correct values and that vR is e1R * e2R *)
  inversion mult_real; subst.
  rewrite delta_0_deterministic in mult_real; auto.
  rewrite delta_0_deterministic; auto.
  unfold evalBinop in *; simpl in *.
  clear delta H2.
  rewrite (meps_0_deterministic H3 e1_real);
    rewrite (meps_0_deterministic H5 e2_real).
  rewrite (meps_0_deterministic H3 e1_real) in mult_real.
  rewrite (meps_0_deterministic H5 e2_real) in mult_real.
  clear H3 H5 H6 v1 v2.
  (* Now unfold the float valued evaluation to get the deltas we need for the inequality *)
  inversion mult_float; subst.
  unfold perturb; simpl.
  inversion H3; subst; inversion H5; subst.
  symmetry in H0, H1;
    unfold updEnv in *; simpl in *.
  inversion H0; inversion H1; subst.
  (* We have now obtained all necessary values from the evaluations --> remove them for readability *)
  clear mult_float H3 H5 mult_real e1_real e1_float e2_real e2_float H0 H1.
  repeat rewrite Rmult_plus_distr_l.
  rewrite Rmult_1_r.
  rewrite Rsub_eq_Ropp_Rplus.
  rewrite Ropp_plus_distr.
  rewrite <- Rplus_assoc.
  setoid_rewrite <- Rsub_eq_Ropp_Rplus at 2.
  eapply Rle_trans.
  eapply Rabs_triang.
  eapply Rplus_le_compat_l.
  rewrite Rabs_Ropp.
  repeat rewrite Rabs_mult.
  eapply Rmult_le_compat_l; auto.
  rewrite <- Rabs_mult.
  apply Rabs_pos.
Qed.

Lemma div_abs_err_bounded (e1:exp Q) (e1R:R) (e1F:R) (e2:exp Q) (e2R:R) (e2F:R)
      (vR:R) (vF:R) (E1 E2:env):
  eval_exp 0%R E1 (toRExp e1) e1R ->
  eval_exp (Q2R machineEpsilon) E2 (toRExp e1) e1F ->
  eval_exp 0%R E1 (toRExp e2) e2R ->
  eval_exp (Q2R machineEpsilon) E2  (toRExp e2) e2F ->
  eval_exp 0%R E1 (Binop Div (toRExp e1) (toRExp e2)) vR ->
  eval_exp (Q2R machineEpsilon) (updEnv 2 e2F (updEnv 1 e1F emptyEnv)) (Binop Div (Var R 1) (Var R 2)) vF ->
  (Rabs (vR - vF) <= Rabs (e1R / e2R - e1F / e2F) + Rabs (e1F / e2F) * (Q2R machineEpsilon))%R.
Proof.
  intros e1_real e1_float e2_real e2_float div_real div_float.
  (* Prove that e1R and e2R are the correct values and that vR is e1R * e2R *)
  inversion div_real; subst.
  rewrite delta_0_deterministic in div_real; auto.
  rewrite delta_0_deterministic; auto.
  unfold evalBinop in *; simpl in *.
  clear delta H2.
  rewrite (meps_0_deterministic H3 e1_real);
    rewrite (meps_0_deterministic H5 e2_real).
  rewrite (meps_0_deterministic H3 e1_real) in div_real.
  rewrite (meps_0_deterministic H5 e2_real) in div_real.
  clear H3 H5 H6 v1 v2.
  (* Now unfold the float valued evaluation to get the deltas we need for the inequality *)
  inversion div_float; subst.
  unfold perturb; simpl.
  inversion H3; subst; inversion H5; subst.
  symmetry in H0, H1;
    unfold updEnv in *; simpl in *.
  inversion H0; inversion H1; subst.
  (* We have now obtained all necessary values from the evaluations --> remove them for readability *)
  clear div_float H3 H5 div_real e1_real e1_float e2_real e2_float H0 H1.
  repeat rewrite Rmult_plus_distr_l.
  rewrite Rmult_1_r.
  rewrite Rsub_eq_Ropp_Rplus.
  rewrite Ropp_plus_distr.
  rewrite <- Rplus_assoc.
  setoid_rewrite <- Rsub_eq_Ropp_Rplus at 2.
  eapply Rle_trans.
  eapply Rabs_triang.
  eapply Rplus_le_compat_l.
  rewrite Rabs_Ropp.
  repeat rewrite Rabs_mult.
  eapply Rmult_le_compat_l; auto.
  apply Rabs_pos.
Qed.

Lemma err_prop_inversion_pos_real nF nR err elo ehi
      (float_iv_pos : (0 < elo - err)%R)
      (real_iv_pos : (0 < elo)%R)
      (err_bounded : (Rabs (nR - nF) <= err)%R)
      (valid_bounds_e2 : (elo <= nR <= ehi)%R)
      (valid_bounds_e2_err : (elo - err <= nF <= ehi + err)%R)
      (err_pos : (0 <= err)%R):
  (Rabs (/nR - / nF) <= err * / ((elo - err) * (elo- err)))%R.
Proof.
  rewrite Rabs_case_inverted in err_bounded.
  assert (0 < nF)%R as nF_pos by lra.
  destruct err_bounded as [ [diff_pos err_bounded] | [diff_neg err_bounded]].
  - cut (0 < /nF - / nR)%R.
    + intros abs_neg.
      rewrite Rabs_left; try lra.
      rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr, Ropp_involutive.
      rewrite Ropp_inv_permute; try lra.
      apply (Rle_trans _ (/ - nR + / (nR - err))).
      * apply Rplus_le_compat_l.
        apply Rinv_le_contravar; lra.
      * rewrite equal_naming_inv; try lra.
        assert (- nR + (nR - err) = - err)%R as simplify_up by lra.
        rewrite simplify_up.
        unfold Rdiv.
        repeat(rewrite <- Ropp_mult_distr_l); rewrite <- Ropp_inv_permute.
        {
          rewrite <- Ropp_mult_distr_r, Ropp_involutive.
          apply Rmult_le_compat_l; try lra.
          apply Rinv_le_contravar.
          - apply Rmult_0_lt_preserving; lra.
          - apply Rmult_le_compat; lra.
        }
        { assert (0 < nR * (nR - err))%R by (apply Rmult_0_lt_preserving; lra); lra. }
    + cut (/ nR < /nF)%R.
      * intros; lra.
      * apply Rinv_lt_contravar; try lra.
        apply Rmult_0_lt_preserving; lra.
  - cut (0 <= /nR - /nF)%R.
    + intros abs_pos.
      rewrite Rabs_right; try lra.
      rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr, Ropp_involutive in err_bounded.
      rewrite Rsub_eq_Ropp_Rplus.
      apply (Rle_trans _ (/nR - / (nR + err))).
      * apply Rplus_le_compat_l.
        apply Ropp_le_contravar.
        apply Rinv_le_contravar; lra.
      * rewrite Rsub_eq_Ropp_Rplus, Ropp_inv_permute; try lra.
        rewrite equal_naming_inv; try lra.
        assert (nR + - (nR + err) = - err)%R as simpl_up by lra.
        rewrite simpl_up.
        unfold Rdiv.
        rewrite <- Ropp_mult_distr_l, <- Ropp_mult_distr_r, <- Ropp_inv_permute.
        { rewrite <- Ropp_mult_distr_r. rewrite Ropp_involutive.
          apply Rmult_le_compat_l; try auto.
          apply Rinv_le_contravar.
          - apply Rmult_0_lt_preserving; lra.
          - apply Rmult_le_compat; lra.
        }
        { assert (0 < nR * (nR + err))%R by (apply Rmult_0_lt_preserving; lra); lra. }
    + cut (/nF <= /nR)%R.
      * intros; lra.
      * apply Rinv_le_contravar; try lra.
Qed.

Lemma err_prop_inversion_pos nF nR err (elo ehi:Q)
      (float_iv_pos : (Q2R 0 < Q2R (elo - err))%R)
      (real_iv_pos : (Q2R 0 < Q2R elo)%R)
      (err_bounded : (Rabs (nR - nF) <= Q2R err)%R)
      (valid_bounds_e2 : (Q2R elo <= nR <= Q2R ehi)%R)
      (valid_bounds_e2_err : (Q2R elo - Q2R err <= nF <= Q2R ehi + Q2R err)%R)
      (err_pos : (0 <= Q2R err)%R):
  (Rabs (/nR - / nF) <= Q2R err * / ((Q2R elo- Q2R err) * (Q2R elo- Q2R err)))%R.
Proof.
  eapply err_prop_inversion_pos_real; try rewrite <- Q2R0_is_0; eauto.
  rewrite <- Q2R_minus; auto.
  rewrite Q2R0_is_0; auto.
Qed.

Lemma err_prop_inversion_neg_real nF nR err elo ehi
      (float_iv_neg : (ehi + err < 0)%R)
      (real_iv_neg : (ehi < 0)%R)
      (err_bounded : (Rabs (nR - nF) <= err)%R)
      (valid_bounds_e : (elo <= nR <= ehi)%R)
      (valid_bounds_e_err : (elo - err <= nF <= ehi + err)%R)
      (err_pos : (0 <= err)%R):
  (Rabs (/nR - / nF) <= err * / ((ehi + err) * (ehi + err)))%R.
Proof.
  rewrite Rabs_case_inverted in err_bounded.
  assert (nF < 0)%R as nF_neg by lra.
  destruct err_bounded as [ [diff_pos err_bounded] | [diff_neg err_bounded]].
  - cut (0 < /nF - / nR)%R.
    + intros abs_neg.
      rewrite Rabs_left; try lra.
      rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr, Ropp_involutive.
      rewrite Ropp_inv_permute; try lra.
      apply (Rle_trans _ (/ - nR + / (nR - err))).
      * apply Rplus_le_compat_l.
        assert (0 < - nF)%R by lra.
        assert (0 < - (nR - err))%R by lra.
        assert (nR - err <= nF)%R as nR_lower by lra.
        apply Ropp_le_contravar in nR_lower.
        apply Rinv_le_contravar in nR_lower; try lra.
        repeat (rewrite <- Ropp_inv_permute in nR_lower; try lra).
      * rewrite equal_naming_inv; try lra.
        assert (- nR + (nR - err) = - err)%R as simplify_up by lra.
        rewrite simplify_up.
        unfold Rdiv.
        repeat(rewrite <- Ropp_mult_distr_l); rewrite <- Ropp_inv_permute.
        {
          rewrite <- Ropp_mult_distr_r, Ropp_involutive.
          apply Rmult_le_compat_l; try lra.
          apply Rinv_le_contravar.
          - apply Rmult_lt_0_inverting; lra.
          - eapply Rle_trans.
            eapply Rmult_le_compat_neg_l; try lra.
            instantiate (1 := (nR - err)%R); try lra.
            setoid_rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l; lra.
        }
        { assert (0 < nR * (nR - err))%R by (apply Rmult_lt_0_inverting; lra); lra. }
    + cut (/ nR < /nF)%R.
      * intros; lra.
      * apply Rinv_lt_contravar; try lra.
        apply Rmult_lt_0_inverting; lra.
  - cut (0 <= /nR - /nF)%R.
    + intros abs_pos.
      rewrite Rabs_right; try lra.
      rewrite Rsub_eq_Ropp_Rplus, Ropp_plus_distr, Ropp_involutive in err_bounded.
      rewrite Rsub_eq_Ropp_Rplus.
      apply (Rle_trans _ (/nR - / (nR + err))).
      * apply Rplus_le_compat_l.
        apply Ropp_le_contravar.
        assert (0 < - nF)%R by lra.
        assert (0 < - (nR + err))%R by lra.
        assert (nF <= nR + err)%R as nR_upper by lra.
        apply Ropp_le_contravar in nR_upper.
        apply Rinv_le_contravar in nR_upper; try lra.
        repeat (rewrite <- Ropp_inv_permute in nR_upper; try lra).
      * rewrite Rsub_eq_Ropp_Rplus, Ropp_inv_permute; try lra.
        rewrite equal_naming_inv; try lra.
        assert (nR + - (nR + err) = - err)%R as simpl_up by lra.
        rewrite simpl_up.
        unfold Rdiv.
        rewrite <- Ropp_mult_distr_l, <- Ropp_mult_distr_r, <- Ropp_inv_permute.
        { rewrite <- Ropp_mult_distr_r. rewrite Ropp_involutive.
          apply Rmult_le_compat_l; try auto.
          apply Rinv_le_contravar.
          - apply Rmult_lt_0_inverting; lra.
          - eapply Rle_trans.
            eapply Rmult_le_compat_neg_l; try lra.
            instantiate (1:= (nR + err)%R); try lra.
            setoid_rewrite Rmult_comm.
            eapply Rmult_le_compat_neg_l; lra.
        }
        { assert (0 < nR * (nR + err))%R by (apply Rmult_lt_0_inverting; lra); lra. }
    + cut (/nF <= /nR)%R.
      * intros; lra.
      * assert (nR <= nF)%R by lra.
        assert (- nF <= - nR)%R as le_inv by lra.
        apply Rinv_le_contravar in le_inv; try lra.
        repeat (rewrite <- Ropp_inv_permute in le_inv; try lra).
Qed.

Lemma err_prop_inversion_neg nF nR err (elo ehi:Q)
      (float_iv_neg : (Q2R (ehi + err) < Q2R 0)%R)
      (real_iv_neg : (Q2R ehi < Q2R 0)%R)
      (err_bounded : (Rabs (nR - nF) <= Q2R err)%R)
      (valid_bounds_e : (Q2R elo <= nR <= Q2R ehi)%R)
      (valid_bounds_e_err : (Q2R elo - Q2R err <= nF <= Q2R ehi + Q2R err)%R)
      (err_pos : (0 <= Q2R err)%R):
  (Rabs (/nR - / nF) <= Q2R err * / ((Q2R ehi + Q2R err) * (Q2R ehi + Q2R err)))%R.
Proof.
  eapply err_prop_inversion_neg_real; try rewrite <- Q2R0_is_0; try lra.
  rewrite <- Q2R_plus ; auto.
  apply valid_bounds_e.
  auto.
  rewrite Q2R0_is_0; auto.
Qed.
