(**
  We define a pseudo SSA predicate.
  The formalization is similar to the renamedApart property in the LVC framework by Schneider, Smolka and Hack
  http://dblp.org/rec/conf/itp/SchneiderSH15
  Our predicate is not as fully fledged as theirs, but we especially borrow the idea of annotating
  the program with the predicate with the set of free and defined variables
**)
Require Import Coq.QArith.QArith Coq.QArith.Qreals Coq.Reals.Reals.
Require Import Coq.micromega.Psatz.
Require Import Daisy.Infra.RealRationalProps Daisy.Infra.Ltacs.
Require Export Daisy.Commands.

Lemma validVars_add V (e:exp V) Vs n:
  NatSet.Subset (usedVars e) Vs ->
  NatSet.Subset (usedVars e) (NatSet.add n Vs).
Proof.
  induction e; try auto.
  - intros valid_subset.
    hnf. intros a in_singleton.
    specialize (valid_subset a in_singleton).
    rewrite NatSet.add_spec; right; auto.
  - intros vars_binop.
    simpl in *.
    intros a in_empty.
    inversion in_empty.
  - simpl; intros in_vars.
    intros a in_union.
    rewrite NatSet.union_spec in in_union.
    destruct in_union.
    + apply IHe1; try auto.
      hnf; intros.
      apply in_vars.
      rewrite NatSet.union_spec; auto.
    + apply IHe2; try auto.
      hnf; intros.
      apply in_vars.
      rewrite NatSet.union_spec; auto.
Qed.

Inductive ssa (V:Type): (cmd V) -> (NatSet.t) -> (NatSet.t) -> Prop :=
 ssaLet x e s inVars Vterm:
   NatSet.Subset (usedVars e) inVars->
   NatSet.mem x inVars = false ->
   ssa s (NatSet.add x inVars) Vterm ->
   ssa (Let x e s) inVars Vterm
|ssaRet e inVars Vterm:
   NatSet.Subset (usedVars e) inVars ->
   NatSet.Equal inVars Vterm ->
   ssa (Ret e) inVars Vterm.

Lemma ssa_subset_freeVars V (f:cmd V) inVars outVars:
  ssa f inVars outVars ->
  NatSet.Subset (freeVars f) inVars.
Proof.
  intros ssa_f; induction ssa_f.
  - simpl in *. hnf; intros a in_fVars.
    rewrite NatSet.remove_spec, NatSet.union_spec in in_fVars.
    destruct in_fVars as [in_union not_eq].
    destruct in_union; try auto.
    specialize (IHssa_f a H1).
    rewrite NatSet.add_spec in IHssa_f.
    destruct IHssa_f; subst; try auto.
    exfalso; apply not_eq; auto.
  - hnf; intros.
    simpl in H1.
    apply H; auto.
Qed.

Lemma ssa_equal_set V (f:cmd V) inVars inVars' outVars:
  NatSet.Equal inVars' inVars ->
  ssa f inVars outVars ->
  ssa f inVars' outVars.
Proof.
  intros set_eq ssa_f.
  revert set_eq; revert inVars'.
  induction ssa_f.
  - constructor.
    + rewrite set_eq; auto.
    + case_eq (NatSet.mem x inVars'); intros case_mem; try auto.
      rewrite NatSet.mem_spec in case_mem.
      rewrite set_eq in case_mem.
      rewrite <- NatSet.mem_spec in case_mem.
      rewrite case_mem in *; congruence.
    + apply IHssa_f; auto.
      apply NatSetProps.Dec.F.add_m; auto.
  - constructor.
    + rewrite set_eq; auto.
    + rewrite set_eq; auto.
Qed.

Fixpoint validSSA (f:cmd Q) (inVars:NatSet.t) :=
  match f with
  |Let x e g =>
    andb (andb (negb (NatSet.mem x inVars)) (NatSet.subset (usedVars e) inVars)) (validSSA g (NatSet.add x inVars))
  |Ret e => NatSet.subset (usedVars e) inVars
  end.

Lemma validSSA_sound f inVars:
  validSSA f inVars = true ->
  exists outVars, ssa f inVars outVars.
Proof.
  revert inVars; induction f.
  - intros inVars validSSA_let.
    simpl in *.
    andb_to_prop validSSA_let.
    specialize (IHf (NatSet.add n inVars) R).
    destruct IHf as [outVars IHf].
    exists outVars.
    constructor; eauto.
    + rewrite <- NatSet.subset_spec; auto.
    + rewrite negb_true_iff in L0. auto.
  - intros inVars validSSA_ret.
    simpl in *.
    exists inVars.
    constructor; auto.
    + rewrite <- NatSet.subset_spec; auto.
    + hnf; intros; split; auto.
Qed.