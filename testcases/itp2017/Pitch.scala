import daisy.lang._
import Real._


object Pitch {

  // s1, s2, s3, y1: <1, 32, 30>, [-1, 1]
  def out1(s1: Real, s2: Real, s3: Real): Real = {
    require(-1 <= s1 && s1 <= 1 && -1 <= s2 && s2 <= 1 && -1 <= s3 && s3 <= 1)
    (0.120200) * s1 + (-42.565500) * s2 + (-1.000100) * s3
  }

  def state1(s1: Real, s2: Real, s3: Real, y1: Real): Real = {
    require(-1 <= s1 && s1 <= 1 && -1 <= s2 && s2 <= 1 && -1 <= s3 && s3 <= 1 && -1 <= y1 && y1 <= 1)
    (0.999715) * s1 + (0.046781) * s2 + (-0.000333) * s3 + (0.000100) * y1
  }

  def state2(s1: Real, s2: Real, s3: Real, y1: Real): Real = {
    require(-1 <= s1 && s1 <= 1 && -1 <= s2 && s2 <= 1 && -1 <= s3 && s3 <= 1 && -1 <= y1 && y1 <= 1)
    (-0.000011) * s1 + (0.998710) * s2 + (-0.000020) * s3 + (0.000000) * y1
  }

  def state3(s1: Real, s2: Real, s3: Real, y1: Real): Real = {
    require(-1 <= s1 && s1 <= 1 && -1 <= s2 && s2 <= 1 && -1 <= s3 && s3 <= 1 && -1 <= y1 && y1 <= 1)
    (-0.000000) * s1 + (0.056663) * s2 + (0.998299) * s3 + (0.001700) * y1
  }

}
